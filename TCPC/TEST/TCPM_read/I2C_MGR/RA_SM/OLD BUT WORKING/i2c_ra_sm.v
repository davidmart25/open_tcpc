module ra_sm (
	input clk,
	input enable,

	input sda,
	input scl,
	
	output nack_received,
	output ack_received
);

/* State REGS */
reg [3:0] state;
reg [3:0] next_state;

/* One Hot encoding */
parameter IDLE = 4'b0001;
parameter WAIT_POSEDGE = 4'b0010;
parameter WAIT_NEGEDGE = 4'b0100;
parameter WAIT_NEGEDGE_SEND_NACK = 4'b1000;

/* Delayed Values */
reg scl_d;

/* Next Values */ 
reg next_nack_received;
reg next_ack_received;

/* Current values */ 
reg current_nack_received;
reg current_ack_received;

/*Assigns*/
assign posedge_scl = ~scl_d&scl;
assign negedge_scl = scl_d&~scl;

/*Output assigns */
assign nack_received = current_nack_received;
assign ack_received = current_ack_received;

/* Next State Logic*/
always@(*) begin
	case(state)
	IDLE: begin
		if(enable)
			next_state = WAIT_POSEDGE;
		else
			next_state = IDLE;
	end

	WAIT_POSEDGE: begin
		if(posedge_scl) begin
			if(sda == 0)
				next_state = WAIT_NEGEDGE;
			else
				next_state = WAIT_NEGEDGE_SEND_NACK;
		end
		else
			next_state = WAIT_POSEDGE;		
	end
	WAIT_NEGEDGE: begin
		if(sda == 1)
			next_state = WAIT_NEGEDGE_SEND_NACK;
		else if (negedge_scl)
			next_state = IDLE;
		else
			next_state = WAIT_NEGEDGE;
	end
	WAIT_NEGEDGE_SEND_NACK: begin
		if(negedge_scl)
			next_state = IDLE;
		else
			next_state = WAIT_NEGEDGE_SEND_NACK;
	end
	endcase
	
end

/* Next Output Logic */
always@(*) begin
	next_nack_received = 0;
	next_ack_received = 0;
	case(state)
		WAIT_NEGEDGE: begin
			if(negedge_scl && sda == 0) begin
				next_ack_received = 1;
				next_nack_received = 0;			
			end
		end
		WAIT_NEGEDGE_SEND_NACK: begin
			if(negedge_scl) begin
				next_ack_received = 0;
				next_nack_received = 1;	
			end	
		end 
	endcase
end

/* Sincronic Logic */
always@(posedge clk) begin
	scl_d <= scl;
	if(~enable)
		state <= IDLE;
	else
		state <= next_state;
	
	current_ack_received  <= next_ack_received;
	current_nack_received <= next_nack_received;
end

/* Simulation Code */
/*
initial begin
	$dumpfile("ra_sim.vcd"); //non synth code, simulation only
	$dumpvars;
end
*/
endmodule


