`include "r8b_sm.v"
module testbench;
reg clk;
reg enable;

reg scl;
reg sda;

wire done;
wire error;

wire [7:0] data_out;

r8b_sm master(
	.clk(clk),
	.enable(enable),
	
	.scl(scl),
	.sda(sda),
	
	.done(done),
	.error(error),

	.data_out(data_out)
);

always@(negedge scl) begin
	sda = ~sda;
end

initial begin
	sda = 1; 
	clk = 0;
	scl = 1;
	enable = 0;
	#50 enable =1;
end

initial forever #1 clk  = ~clk;
initial forever #200 scl = ~scl;

endmodule


