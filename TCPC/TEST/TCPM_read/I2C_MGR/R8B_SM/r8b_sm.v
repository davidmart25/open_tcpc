module r8b_sm (
	input clk,
	input enable,

	input scl,
	input sda,

	output done,
	output error,
	
	output [7:0] data_out
);
/* State REGs */ 
reg [4:0] state;
reg [4:0] next_state;

/* One Hot encoding */ 

parameter IDLE =           5'b00001;
parameter RECEIVE_BIT =    5'b00010;
parameter DONE_RECEIVING = 5'b00100;
parameter STOP =           5'b01000;
parameter RECEIVE_ERROR =  5'b10000;

/* Next values */ 
reg next_done;
reg next_error;
reg [7:0] next_data_out;

reg [3:0] next_bit_count;

/* Current values */
reg current_done;
reg current_error;
reg [7:0] current_data_out;

reg [3:0] current_bit_count;
/* Delayed Values */ 
reg sda_d;
reg scl_d;
/* Assigns*/
assign posedge_scl = ~scl_d&scl;
/* Output Assigns */
assign done = current_done;
assign error = current_error;
assign data_out = current_data_out;

/* Next State Logic */
always @(*) begin
	case(state)
		IDLE: begin
			if(enable)
				next_state = RECEIVE_BIT;
			else
				next_state = IDLE;
		end
		RECEIVE_BIT: begin
			if(current_error)
				next_state = RECEIVE_ERROR;
			else if (current_bit_count == 0 && ~scl)
				next_state = DONE_RECEIVING;
			else
				next_state = RECEIVE_BIT;
		end
		DONE_RECEIVING: next_state = STOP;

		STOP: next_state = STOP;

		RECEIVE_ERROR: next_state =  STOP;
	endcase
end

/* Next Output Logic */
always @(*) begin
	next_done = current_done;
	next_error = current_error;	
	next_data_out = current_data_out;

	next_bit_count = current_bit_count;
	case(state)
	IDLE: begin
		if(enable)
			next_bit_count = 8;
		else begin //clear all variables
			next_done = 0;
			next_error = 0;
			next_bit_count = 8;
		end
	end
	RECEIVE_BIT: begin
		if(posedge_scl) begin
			next_data_out = current_data_out << 1;
			next_data_out[0] = sda;
			next_bit_count = current_bit_count -1;
		end 
		else if (scl) begin
			if(sda != sda_d)
				next_error = 1;
		end
	end
	
	DONE_RECEIVING: begin
		next_done = 1;
	end
	
	RECEIVE_ERROR: begin
		next_error = 1;
		next_done  = 0;
	end
	
	endcase
end

/* Sincronic Logic */
always @(posedge clk) begin
	scl_d <= scl;
	sda_d <= sda;
	if(~enable)
		state <= IDLE;
	else
		state <= next_state;
	
	current_done = next_done;
	current_error = next_error;	
	current_data_out = next_data_out;

	current_bit_count = next_bit_count;

end
/*
initial begin
	$dumpfile("r8b_sim.vcd"); //non synth code, simulation only
	$dumpvars;
end
*/
endmodule
