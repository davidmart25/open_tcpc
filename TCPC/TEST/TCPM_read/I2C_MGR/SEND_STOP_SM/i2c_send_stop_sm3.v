module send_stop_sm (
	input clk,
	
	input enable,
	output done,
	
	output sda,
	input scl
);

/* State REGS */

reg [1:0] state;
reg [1:0] next_state;

/* One Hot encoding */

parameter IDLE = 2'b01;
parameter WAIT_SCL_1 = 2'b10;

/* Delayed Values*/ 

/* Next values */ 
reg next_done;
reg next_sda;

/* Current values */
reg current_done;
reg current_sda;
/*Assigns */

/* Output Assigns */ 
assign done = current_done;
assign sda = current_sda;

/* Next State Logic */ 
always @(*) begin
	case(state)
	IDLE: begin
		if(enable)
			next_state = WAIT_SCL_1;
		else
			next_state = IDLE;
	end
	WAIT_SCL_1: begin
		if(scl) // This scl maybe should be delayed by a certain amount
			next_state = IDLE;
		else
			next_state = WAIT_SCL_1;			
	end

	endcase
end

/* Next Output Logic */ 
always @(*) begin
	next_sda = 0;
	next_done = 0;
	if(state == WAIT_SCL_1) begin
		if(scl) begin// this scl maybe should be delayed by a certain amount
			next_sda = 1;
			next_done = 1;
		end	
	end
end

/* Sincronic Logic */ 
always @(posedge clk) begin
/* Delay FFs */

/* State FFs */ 
	if(~enable)
		state <= IDLE;	
	else
		state <= next_state;

	current_done <= next_done;
	current_sda <= next_sda;


end
/* Simulation Code */ 
/*
initial begin
	$dumpfile("send_stop_sim.vcd"); //non synth code, simulation only
	$dumpvars;
end
*/
endmodule

