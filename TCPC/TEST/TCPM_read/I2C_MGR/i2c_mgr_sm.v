`include "i2c_mgr_opmodes.vh"
`include "R8B_SM/r8b_sm.v"
`include "RA_SM/i2c_ra_sm.v"
`include "RMBR_SM/rmbr_sm.v"
`include "RRB_SM/i2c_rrb_sm.v"
`include "RSBR_SM/rsbr_sm.v"
`include "S8B_SM/i2c_s8b_sm.v"
`include "SCL_SM/scl_sm.v"
`include "SEND_ACK_SM/i2c_send_ack_sm.v"
`include "SEND_NACK_SM/i2c_send_nack_sm.v"
`include "SEND_START_SM/i2c_send_start_sm.v"
`include "SEND_STOP_SM/i2c_send_stop_sm.v"
`include "WMBR_SM/i2c_wmbr_sm.v"
`include "WSBR_SM/i2c_wsbr_sm.v"
`include "WTB_SM/i2c_wtb_sm.v"
module i2c_mgr_sm(
	input clk,
	input enable,

	input [2:0] op_mode,
	input [6:0] slave_address,
	input [7:0] reg_address,
	input [7:0] write_data_low, // LSB
	input [7:0] write_data_high, //MSB

	input scl_clk, //Clck frequency
	input scl_rec, //Clock received on the line
	
	output [7:0] data_out_low,
	output [7:0] data_out_high,
	output data_ready,
	output done,
	output error,
	
	output scl_out, //Clock sent on the line
	output reg sda_out,
	input sda_in,
	
	input new_data_available,

	output request_new_data
);
/* State Regs */
reg [5:0] state;
reg [5:0] next_state;

/* One Hot Encoding */ 

parameter IDLE = 6'b000001;
parameter DECODE_OP_MODE = 6'b000010;
parameter WAIT_FOR_FINISHED_TRANSACTION = 6'b000100;
parameter TRANSACTION_FINISHED = 6'b001000;
parameter STOP = 6'b010000;
parameter TRANSACTION_ERROR = 6'b100000;
 
/* Next Values */ 
reg [2:0] next_op_mode;
reg [6:0] next_slave_address;
reg [7:0] next_reg_address;
reg [7:0] next_write_data_low;
reg [7:0] next_write_data_high;
reg [7:0] next_data_out_low;
reg [7:0] next_data_out_high;

reg next_data_ready;
reg next_error;
reg next_done;


reg next_wsbr_enable;
reg next_rsbr_enable;
reg next_wmbr_enable;
reg next_rmbr_enable;
reg next_wtb_enable;
reg next_rrb_enable;

reg next_r8b_enable;
reg next_ra_enable;
reg next_s8b_enable;
reg next_scl_enable;
reg next_send_ack_enable;
reg next_send_nack_enable;
reg next_send_start_enable;
reg next_send_stop_enable;

reg [7:0] next_transmit_data;

/* Current values */ 
reg [2:0] current_op_mode;
reg [6:0] current_slave_address;
reg [7:0] current_reg_address;
reg [7:0] current_write_data_low;
reg [7:0] current_write_data_high;
reg [7:0] current_data_out_low;
reg [7:0] current_data_out_high;
reg [7:0] current_received_byte_count;

reg current_data_ready;
reg current_error;
reg current_done;

reg current_wsbr_enable; 
reg current_rsbr_enable;
reg current_wmbr_enable;
reg current_rmbr_enable;
reg current_wtb_enable;
reg current_rrb_enable;

reg current_r8b_enable;
reg current_ra_enable;
reg current_s8b_enable;
reg current_scl_enable;
reg current_send_ack_enable;
reg current_send_nack_enable;
reg current_send_start_enable;
reg current_send_stop_enable;

reg [7:0] current_transmit_data;

/* Assigns */

/* Output Assigns */

/* Wires for interconecting modules */ 
wire [7:0] wsbr_s8b_data_in;
wire [7:0] rsbr_s8b_data_in;
wire [7:0] wmbr_s8b_data_in;
wire [7:0] rmbr_s8b_data_in;
wire [7:0] wtb_s8b_data_in;
wire [7:0] rrb_s8b_data_in;
wire [7:0] rsbr_data_out;
wire [7:0] rmbr_read_data;
wire [7:0] rmbr_data_out_low;
wire [7:0] rmbr_data_out_high;
wire [7:0] wtb_reg_address;
wire [7:0] wtb_transmit_data;
wire [7:0] rrb_data_out;
wire [7:0] r8b_data_out;
wire [7:0] s8b_data_in;
wire [7:0] rsbr_read_data;
/*Module Instantiation */
wsbr_sm wsbr (
	.clk(clk),
	.enable(wsbr_enable),
	.error(wsbr_error),
	.done(wsbr_done),
	
	.slave_address(current_slave_address),
	.reg_address(current_reg_address),
	.write_data(current_write_data_low),

	.s8b_enable(wsbr_s8b_enable),	
	.s8b_data_in(wsbr_s8b_data_in),
	.s8b_done(s8b_done),
	.s8b_error(s8b_error),
	
	.send_start_enable(wsbr_send_start_enable),
	.send_start_done(send_start_done),
	
	.ra_enable(wsbr_ra_enable),
	.ra_nack_received(ra_nack_received),
	.ra_ack_received(ra_ack_received),
	
	.send_stop_enable(wsbr_send_stop_enable),
	.send_stop_done(send_stop_done)
);
rsbr_sm rsbr (
	.clk(clk),
	.enable(rsbr_enable),
	.error(rsbr_error),
	.done(rsbr_done),

	.slave_address(current_slave_address),
	.reg_address(current_reg_address),
	.read_data(rsbr_read_data),

	.s8b_enable(rsbr_s8b_enable),	
	.s8b_data_in(rsbr_s8b_data_in),
	.s8b_done(s8b_done),
	.s8b_error(s8b_error),	

	.r8b_enable(rsbr_r8b_enable),
	.r8b_done(r8b_done),
	.r8b_error(r8b_enable),
	.r8b_data_out(r8b_data_out),	
	
	.send_start_enable(rsbr_send_start_enable),
	.send_start_done(send_start_done),

	.ra_enable(rsbr_ra_enable),
	.ra_nack_received(ra_nack_received),
	.ra_ack_received(ra_ack_received),

	.send_stop_enable(rsbr_send_stop_enable),
	.send_stop_done(send_stop_done),

	.send_nack_enable(rsbr_send_nack_enable),
	.send_nack_done(send_nack_done),
	.data_out(rsbr_data_out)
);
wmbr_sm wmbr_(
	.clk(clk),
	.enable(wmbr_enable),
	.error(wmbr_error),
	.done(wmbr_done),
	
	.slave_address(current_slave_address),
	.reg_address(current_reg_address),
	.write_data_low(current_write_data_low),
	.write_data_high(current_write_data_high),

	.s8b_enable(wmbr_s8b_enable),	
	.s8b_data_in(wmbr_s8b_data_in),
	.s8b_done(s8b_done),
	.s8b_error(s8b_error),
	
	.send_start_enable(wmbr_send_start_enable),
	.send_start_done(send_start_done),
	
	.ra_enable(wmbr_ra_enable),
	.ra_nack_received(ra_nack_received),
	.ra_ack_received(ra_ack_received),
	
	.send_stop_enable(wmbr_send_stop_enable),
	.send_stop_done(send_stop_done)
);
rmbr_sm rmbr (
	.clk(clk),
	.enable(rmbr_enable),
	.error(rmbr_error),
	.done(rmbr_done),

	.slave_address(current_slave_address),
	.reg_address(current_reg_address),
	.read_data(rmbr_read_data),

	.s8b_enable(rmbr_s8b_enable),	
	.s8b_data_in(rmbr_s8b_data_in),
	.s8b_done(s8b_done),
	.s8b_error(s8b_error),	

	.r8b_enable(rmbr_r8b_enable),
	.r8b_done(r8b_done),
	.r8b_error(r8b_error),
	.r8b_data_out(r8b_data_out),	
	
	.send_start_enable(rmbr_send_start_enable),
	.send_start_done(send_start_done),

	.ra_enable(rmbr_ra_enable),
	.ra_nack_received(ra_nack_received),
	.ra_ack_received(ra_ack_received),

	.send_stop_enable(rmbr_send_stop_enable),
	.send_stop_done(send_stop_done),

	.send_nack_enable(rmbr_send_nack_enable),
	.send_nack_done(send_nack_done),
	
	.send_ack_enable(rmbr_send_ack_enable),
	.send_ack_done(send_ack_done),
	
	.data_out_low(rmbr_data_out_low),
	.data_out_high(rmbr_data_out_high)
); 
wtb_sm wtb(
	.clk(clk),
	.enable(wtb_enable),
	.error(wtb_error),
	.done(wtb_done),
	
	.slave_address(current_slave_address),
	
	.reg_address(wtb_reg_address),
	.request_new_data(request_new_data),
	.new_data_available(new_data_available),
	.transmit_data(wtb_transmit_data),
	
	.s8b_enable(wtb_s8b_enable),	
	.s8b_data_in(wtb_s8b_data_in),
	.s8b_done(s8b_done),
	.s8b_error(s8b_error),
	
	.send_start_enable(wtb_send_start_enable),
	.send_start_done(send_start_done),
	
	.ra_enable(wtb_ra_enable),
	.ra_nack_received(ra_nack_received),
	.ra_ack_received(ra_ack_received),
	
	.send_stop_enable(wtb_send_stop_enable),
	.send_stop_done(send_stop_done)
);
rrb_sm rrb  (
	.clk(clk),
	.enable(rrb_enable),
	
	.error(rrb_error),
	.done(rrb_done),
	
	.slave_address(current_slave_address),
	
	.data_out (rrb_data_out),
	.data_ready(rrb_data_ready),
	
	.s8b_enable(rrb_s8b_enable),
	.s8b_data_in(rrb_s8b_data_in),
	.s8b_done(s8b_done),
	.s8b_error(s8b_error),
	
	.send_start_enable(rrb_send_start_enable),
	.send_start_done(send_start_done),
	
	.ra_enable(rrb_ra_enable),
	.ra_nack_received(ra_nack_received),
	.ra_ack_received(ra_ack_received),
	
	.send_ack_enable(rrb_send_ack_enable),
	.send_ack_done(send_ack_done),
	
	.send_nack_enable(rrb_send_nack_enable),
	.send_nack_done(send_nack_done),
	
	.send_stop_enable(rrb_send_stop_enable),
	.send_stop_done(send_stop_done),
	
	.r8b_enable(rrb_r8b_enable),
	.r8b_done(r8b_done),
	.r8b_error(r8b_error),
	.r8b_data_out(r8b_data_out)	
);

r8b_sm r8b (
	.clk(clk),
	.enable(r8b_enable),

	.scl(scl_rec),
	.sda(sda_in),

	.done(r8b_done),
	.error(r8b_error),
	
	.data_out(r8b_data_out)
);
ra_sm ra(
	.clk(clk),
	.enable(ra_enable),

	.sda(sda_in),
	.scl(scl_rec),
	
	.nack_received(ra_nack_received),
	.ack_received(ra_ack_received)
);
s8b_sm s8b(
	.clk(clk),

	.enable(s8b_enable),
	.error(s8b_error),
	.done(s8b_done),

	.scl(scl_rec),
	.sda(s8b_sda),

	.data_in(s8b_data_in)
);
scl_sm scl_mgr(
	.clk(clk),
	.enable(scl_enable),
	.send_start_enable(send_start_enable),
	.send_stop_done(send_stop_done),
	.sda(sda_in), // Chequear esto, inconsistencia entre tb y sm
	.scl_clk(scl_clk),
	.scl(scl_out)
);
send_ack_sm send_ack(
	.clk(clk),
	.enable(send_ack_enable),
	
	.sda(send_ack_sda),
	.scl(scl_rec),
	
	.done(send_ack_done)
);
send_nack_sm send_nack(
	.clk(clk),
	.enable(send_nack_enable),
	
	.sda(send_nack_sda),
	.scl(scl_rec),
	
	.done(send_nack_done)

);
send_start_sm send_start(
	.clk(clk),
	.enable(send_start_enable),
	.done(send_start_done),
	.sda(send_start_sda),
	.scl(scl_rec)
);
send_stop_sm send_stop(
	.clk(clk),
	
	.enable(send_stop_enable),
	.done(send_stop_done),

	.sda(send_stop_sda),
	.scl(scl_rec)
);

/*Next State Logic */
always @(*) begin
	case (state)
	IDLE: begin
		if(enable)
			next_state = DECODE_OP_MODE;
		else
			next_state = IDLE;
	end		
	DECODE_OP_MODE: begin
		case(current_op_mode)
		`WRITE_SINGLE_BYTE_REG: next_state = WAIT_FOR_FINISHED_TRANSACTION;
		`READ_SINGLE_BYTE_REG: next_state = WAIT_FOR_FINISHED_TRANSACTION;
		`WRITE_MULTIPLE_BYTE_REG: next_state = WAIT_FOR_FINISHED_TRANSACTION;
		`READ_MULTIPLE_BYTE_REG: next_state = WAIT_FOR_FINISHED_TRANSACTION;
		`WRITE_TRANSMIT_BUFFER: next_state = WAIT_FOR_FINISHED_TRANSACTION;
		`READ_RECEIVE_BUFFER: next_state = WAIT_FOR_FINISHED_TRANSACTION;
		default: next_state = TRANSACTION_ERROR;
		endcase
	end
	WAIT_FOR_FINISHED_TRANSACTION: begin
		case(current_op_mode)
		`WRITE_SINGLE_BYTE_REG: begin
			if(wsbr_error)
				next_state = TRANSACTION_ERROR;
			else if(wsbr_done)
				next_state = TRANSACTION_FINISHED;
			else
				next_state = WAIT_FOR_FINISHED_TRANSACTION;
		end
		`READ_SINGLE_BYTE_REG: begin 
			if(rsbr_error)
				next_state = TRANSACTION_ERROR;
			else if(rsbr_done)
				next_state = TRANSACTION_FINISHED;
			else
				next_state = WAIT_FOR_FINISHED_TRANSACTION;		
		end 
		`WRITE_MULTIPLE_BYTE_REG: begin
			if(wmbr_error)
				next_state = TRANSACTION_ERROR;
			else if(wmbr_done)
				next_state = TRANSACTION_FINISHED;
			else
				next_state = WAIT_FOR_FINISHED_TRANSACTION;		
		end 
		`READ_MULTIPLE_BYTE_REG: begin
			if(rmbr_error)
				next_state = TRANSACTION_ERROR;
			else if(rmbr_done)
				next_state = TRANSACTION_FINISHED;
			else
				next_state = WAIT_FOR_FINISHED_TRANSACTION;		
		end 
		`WRITE_TRANSMIT_BUFFER:begin
			if(wtb_error)
				next_state = TRANSACTION_ERROR;
			else if(wtb_done)
				next_state = TRANSACTION_FINISHED;
			else
				next_state = WAIT_FOR_FINISHED_TRANSACTION;		
		end 
		`READ_RECEIVE_BUFFER:begin
			if(rrb_error)
				next_state = TRANSACTION_ERROR;
			else if(rrb_done)
				next_state = TRANSACTION_FINISHED;
			else
				next_state = WAIT_FOR_FINISHED_TRANSACTION;		
		end
		default: next_state = TRANSACTION_ERROR;
		endcase
	end
	TRANSACTION_FINISHED: next_state = STOP;
	STOP: next_state = STOP;
	TRANSACTION_ERROR: next_state = TRANSACTION_ERROR;
	default: next_state = TRANSACTION_ERROR;
	endcase
	
end

/* Next Output Logic */
always @(*) begin

	next_wsbr_enable = current_wsbr_enable;
	next_rsbr_enable = current_rsbr_enable;
	next_wmbr_enable = current_wmbr_enable;
	next_rmbr_enable = current_rmbr_enable;
	next_wtb_enable = current_wtb_enable;
	next_rrb_enable = current_rrb_enable;
	
	next_op_mode = current_op_mode;
	next_slave_address = current_slave_address;
	next_reg_address = current_reg_address;
	next_write_data_low = current_write_data_low; 
	next_write_data_high = current_write_data_high;
	
	next_transmit_data = current_transmit_data;
	
	next_data_ready =  current_data_ready;
	next_data_out_low = current_data_out_low;
	next_data_out_high = current_data_out_high;
	next_done = current_done;
	next_error = current_error;
	
	case(state)
	IDLE: begin
		if(enable) begin
			next_op_mode = op_mode;
			next_slave_address = slave_address;
			next_reg_address = reg_address;
			next_write_data_low = write_data_low; 
			next_write_data_high = write_data_high;
			
			next_scl_enable = 1;
		end
		else begin
			next_wsbr_enable = 0;
			next_rsbr_enable = 0;
			next_wmbr_enable = 0;
			next_rmbr_enable = 0;
			next_wtb_enable = 0;
			next_rrb_enable = 0;

			next_r8b_enable = 0;
			next_ra_enable = 0;
			next_s8b_enable = 0;
			next_scl_enable = 0;
			next_send_ack_enable = 0;
			next_send_nack_enable = 0;
			next_send_start_enable = 0;
			next_send_stop_enable = 0;
			
			next_data_ready =  0;
			next_done = 0;
			next_error = 0;
			
		end	

	end
	DECODE_OP_MODE: begin
		case(current_op_mode)
		`WRITE_SINGLE_BYTE_REG: begin
			next_wsbr_enable = 1;
			next_rsbr_enable = 0;
			next_wmbr_enable = 0;
			next_rmbr_enable = 0;
			next_wtb_enable = 0;
			next_rrb_enable = 0;
			
			next_r8b_enable = 0;
			next_ra_enable = wsbr_ra_enable;
			next_s8b_enable = wsbr_s8b_enable;
			//next_scl_enable = 0;
			next_send_ack_enable = 0;
			next_send_nack_enable = 0;
			next_send_start_enable = wsbr_send_start_enable;
			next_send_stop_enable = wsbr_send_stop_enable;
		end
		`READ_SINGLE_BYTE_REG: begin
			next_wsbr_enable = 0;
			next_rsbr_enable = 1;
			next_wmbr_enable = 0;
			next_rmbr_enable = 0;
			next_wtb_enable = 0;
			next_rrb_enable = 0;

			next_r8b_enable = rsbr_r8b_enable;
			next_ra_enable = rsbr_ra_enable;
			next_s8b_enable = rsbr_s8b_enable;
			//next_scl_enable = 0;
			next_send_ack_enable = 0;
			next_send_nack_enable = rsbr_send_nack_enable;
			next_send_start_enable = rsbr_send_start_enable;
			next_send_stop_enable = rsbr_send_stop_enable;			
		end
		`WRITE_MULTIPLE_BYTE_REG: begin
			next_wsbr_enable = 0;
			next_rsbr_enable = 0;
			next_wmbr_enable = 1;
			next_rmbr_enable = 0;
			next_wtb_enable = 0;
			next_rrb_enable = 0;
			
			next_r8b_enable = 0;
			next_ra_enable = wmbr_ra_enable;
			next_s8b_enable = wmbr_s8b_enable;
			//next_scl_enable = 0;
			next_send_ack_enable = 0;
			next_send_nack_enable = 0;
			next_send_start_enable = wmbr_send_start_enable;
			next_send_stop_enable = wmbr_send_stop_enable;
		end
		`READ_MULTIPLE_BYTE_REG: begin
			next_wsbr_enable = 0;
			next_rsbr_enable = 0;
			next_wmbr_enable = 0;
			next_rmbr_enable = 1;
			next_wtb_enable = 0;
			next_rrb_enable = 0;
			
			next_r8b_enable = rmbr_r8b_enable;
			next_ra_enable = rmbr_ra_enable;
			next_s8b_enable = rmbr_s8b_enable;
			//next_scl_enable = 0;
			next_send_ack_enable = rmbr_send_ack_enable;
			next_send_nack_enable = rmbr_send_nack_enable;
			next_send_start_enable = rmbr_send_start_enable;
			next_send_stop_enable = rmbr_send_stop_enable;
		end
		`WRITE_TRANSMIT_BUFFER: begin
			next_transmit_data = current_write_data_low; 
				
			next_wsbr_enable = 0;
			next_rsbr_enable = 0;
			next_wmbr_enable = 0;
			next_rmbr_enable = 0;
			next_wtb_enable = 1;
			next_rrb_enable = 0;

			next_r8b_enable = 0;
			next_ra_enable = wtb_ra_enable;
			next_s8b_enable = wtb_s8b_enable;
			//next_scl_enable = 0;
			next_send_ack_enable = 0;
			next_send_nack_enable = 0;
			next_send_start_enable = wtb_send_start_enable;
			next_send_stop_enable = wtb_send_stop_enable;			
		end
		`READ_RECEIVE_BUFFER: begin
			next_wsbr_enable = 0;
			next_rsbr_enable = 0;
			next_wmbr_enable = 0;
			next_rmbr_enable = 0;
			next_wtb_enable = 0;
			next_rrb_enable = 1;
			
			next_r8b_enable = rrb_r8b_enable;
			next_ra_enable = rrb_ra_enable;
			next_s8b_enable = rrb_s8b_enable;
			//next_scl_enable = 0;
			next_send_ack_enable = rrb_send_ack_enable;
			next_send_nack_enable = rrb_send_nack_enable;
			next_send_start_enable = rrb_send_start_enable;
			next_send_stop_enable = rrb_send_stop_enable;
		end
		default: begin
			next_wsbr_enable = 0;
			next_rsbr_enable = 0;
			next_wmbr_enable = 0;
			next_rmbr_enable = 0;
			next_wtb_enable = 0;
			next_rrb_enable = 0;
			
			next_r8b_enable = 0;
			next_ra_enable = 0;
			next_s8b_enable = 0;
			next_scl_enable = 0;
			next_send_ack_enable = 0;
			next_send_nack_enable = 0;
			next_send_start_enable = 0;
			next_send_stop_enable = 0;
		end
		endcase
	end
	WAIT_FOR_FINISHED_TRANSACTION: begin
		case(current_op_mode)
		`WRITE_TRANSMIT_BUFFER: begin
			if(request_new_data)
				next_transmit_data = write_data_low;
		end
		`READ_RECEIVE_BUFFER: begin
			if(rrb_data_ready) begin
				next_data_out_low = rrb_data_out;
				next_data_ready = 1;
			end
			else
				next_data_ready = 0;
		end
		endcase
	end
	TRANSACTION_FINISHED: begin
		case(current_op_mode)
		`READ_SINGLE_BYTE_REG: begin
			next_data_out_low = rsbr_data_out;
			next_data_ready = 1;
		end
		`READ_MULTIPLE_BYTE_REG: begin
			next_data_out_low = rmbr_data_out_low;
			next_data_out_high = rmbr_data_out_high;
			next_data_ready = 1;
		end
		`READ_RECEIVE_BUFFER: begin
			next_data_out_low = rrb_data_out;
			next_data_ready = 1;
		end
		endcase
		next_done = 1;
			
		next_wsbr_enable = 0;
		next_rsbr_enable = 0;
		next_wmbr_enable = 0;
		next_rmbr_enable = 0;
		next_wtb_enable = 0;
		next_rrb_enable = 0;
		
		next_r8b_enable = 0;
		next_ra_enable = 0;
		next_s8b_enable = 0;
		next_scl_enable = 0;
		next_send_ack_enable = 0;
		next_send_nack_enable = 0;
		next_send_start_enable = 0;
		next_send_stop_enable = 0;	
	end
	STOP: next_done =  1;
	TRANSACTION_ERROR: next_error = 1;
	endcase
end

/* CLK logic */
always @(posedge clk) begin
	if(~enable)
		state <= IDLE;
	else
		state = next_state;
		
	current_wsbr_enable <= next_wsbr_enable;
	current_rsbr_enable <= next_rsbr_enable;
	current_wmbr_enable <= next_wmbr_enable;
	current_rmbr_enable <= next_rmbr_enable;
	current_wtb_enable <= next_wtb_enable;
	current_rrb_enable <= next_rrb_enable;
	
	current_op_mode <= next_op_mode;
	current_slave_address <= next_slave_address;
	current_reg_address <= next_reg_address;
	current_write_data_low <= next_write_data_low; 
	current_write_data_high <= next_write_data_high;
	
	current_transmit_data <= next_transmit_data;
	
	current_data_ready <=  next_data_ready;
	current_data_out_low <= next_data_out_low;
	current_data_out_high <= next_data_out_high;
	current_done <= next_done;
	current_error <= next_error;
end
/* SDA LOGIC */
always @(posedge clk) begin //Maybe at posedge clk
	if(r8b_enable)
		sda_out = 1;
	else if (ra_enable)
		sda_out = 1;
	else if (s8b_enable)
		sda_out = s8b_sda; 
	else if (send_ack_enable)
		sda_out = s8b_sda;
	else if (send_nack_enable)
		sda_out = send_nack_sda;
	else if (send_start_enable)
		sda_out = send_start_sda;
	else if (send_stop_enable)
		sda_out = send_stop_sda;
	else
		sda_out = 1;
end


endmodule 



