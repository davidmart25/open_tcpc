`include "i2c_rrb_sm.v"
`include "../RA_SM/i2c_ra_sm.v"
`include "../S8B_SM/i2c_s8b_sm.v"
`include "../R8B_SM/r8b_sm.v"
`include "../SEND_ACK_SM/i2c_send_ack_sm.v"
`include "../SEND_NACK_SM/i2c_send_nack_sm.v"
`include "../SEND_STOP_SM/i2c_send_stop_sm.v"
`include "../SEND_START_SM/i2c_send_start_sm.v"
module testbench;
	reg clk;
	reg rrb_enable;
	
	reg [6:0] slave_address;
	wire [7:0] data_out;
	wire [7:0] r8b_data_out;
	wire [7:0] s8b_data_in;
	reg start_scl;
	reg sda;
	reg scl;
	reg r8b_sda;
assign ra_sda = 0;
rrb_sm rrb (
	.clk(clk),
	.enable(rrb_enable),
	
	.error(error),
	.done(done),
	
	.slave_address(slave_address),
	
	.data_out(data_out),
	.data_ready(data_ready),
	
	.s8b_enable(s8b_enable),
	.s8b_data_in(s8b_data_in),
	.s8b_done(s8b_done),
	.s8b_error(s8b_error),
	
	.send_start_enable(send_start_enable),
	.send_start_done(send_start_done),
	
	.ra_enable(ra_enable),
	.ra_nack_received(ra_nack_received),
	.ra_ack_received(ra_ack_received),
	
	.send_ack_enable(send_ack_enable),
	.send_ack_done(send_ack_done),
	
	.send_nack_enable(send_nack_enable),
	.send_nack_done(send_nack_done),
	
	.send_stop_enable(send_stop_enable),
	.send_stop_done(send_stop_done),
	
	.r8b_enable(r8b_enable),
	.r8b_done(r8b_done),
	.r8b_error(r8b_error),
	.r8b_data_out(r8b_data_out)		
);


ra_sm ra(
	.clk(clk),
	.enable(ra_enable),

	.sda(ra_sda),
	.scl(scl),

	.nack_received(ra_nack_received),
	.ack_received(ra_ack_received)
);

s8b_sm s8b(
	.clk(clk),

	.enable(s8b_enable),
	.error(s8b_error),
	.done(s8b_done),

	.scl(scl),
	.sda(s8b_sda),

	.data_in(s8b_data_in)
	
);
r8b_sm master(
	.clk(clk),
	.enable(r8b_enable),
	
	.scl(scl),
	.sda(r8b_sda),
	
	.done(r8b_done),
	.error(r8b_error),

	.data_out(r8b_data_out)
);
send_stop_sm send_stop(
	.clk(clk),
	
	.enable(send_stop_enable),
	.done(send_stop_done),
	
	.sda(send_stop_sda),
	.scl(scl)
);

send_start_sm send_start(
	.clk(clk),

	.enable(send_start_enable),
	.done(send_start_done),

	.sda(send_start_sda),
	.scl(scl)
	
);
send_nack_sm send_nack (
	.clk(clk),
	.enable(send_nack_enable),

	.sda(send_nack_sda),
	.scl(scl),
	
	.done(send_nack_done)
);
send_ack_sm send_ack (
	.clk(clk),
	.enable(send_ack_enable),
	
	.sda(send_ack_sda),
	.scl(scl),

	.done(send_ack_done)
);

always @(*) begin
	if(s8b_enable)
		sda =s8b_sda;
	else if (send_stop_enable)
		sda = send_stop_sda;
	else if (send_start_enable)
		sda = send_start_sda;
	else if (r8b_enable)
		sda = r8b_sda;
	else if (send_nack_enable)
		sda = send_nack_sda;
	else if (send_ack_enable)
		sda = send_ack_sda;
	else
		sda = 1;
end
initial begin
	r8b_sda = 1;
	clk = 0;
	scl = 1;
	rrb_enable = 0;
	start_scl = 0;
	slave_address = 8'hCA;
	#20 rrb_enable = 1;

end
initial begin
	forever #1 clk = ~clk;
end

initial begin
	forever begin
		if(start_scl)
			#200 scl = ~scl;
		else
			#200 scl = scl;
	end
end

always @(*) begin
	if(send_start_enable)
		#20 start_scl = 1;
end
always @(negedge scl) begin
	if(r8b_enable)
		r8b_sda <= ~r8b_sda;
end

endmodule

