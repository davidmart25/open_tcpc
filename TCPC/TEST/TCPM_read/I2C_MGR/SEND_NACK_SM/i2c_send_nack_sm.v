module send_nack_sm (
	input clk,
	input enable,

	output sda,
	input scl,
	
	output done
);
/* State REGS */ 
reg [2:0] state;
reg [2:0] next_state;

/*One Hot encoding */ 
parameter IDLE =         3'b001;
parameter WAIT_NEGEDGE = 3'b010;
parameter STOP=          3'b100;

/* Delayed Values */
reg scl_d;

/* Next Values */ 
reg next_sda;
reg next_done;

/* Current Values */
reg current_sda;
reg current_done;

/*Assigns */
assign negedge_scl = scl_d&~scl;

/*Output Assigns */ 
assign sda = current_sda;
assign done = current_done;

/*Next State Logic */

always @(*) begin
	case(state)
	IDLE: begin
		if(enable) 
			next_state = WAIT_NEGEDGE;
		else
			next_state = IDLE;
	end

	WAIT_NEGEDGE: begin
		if(negedge_scl)
			next_state = STOP;
		else
			next_state = WAIT_NEGEDGE;
	end
	
	STOP: next_state = STOP;
	endcase
end

/*Next Output Logic */
always@(*) begin
	next_sda = 1;
	next_done = 0;
	case(state)

	WAIT_NEGEDGE: begin
		if(negedge_scl)
			next_done = 1;
	end
	STOP: next_done = 1;
	endcase
end
/* Sincronic Logic */ 
always @(posedge clk) begin
	scl_d <= scl;
	if(~enable)
		state <= IDLE;
	else
		state <= next_state;

	current_sda <= next_sda;
	current_done <= next_done;
end

endmodule	
