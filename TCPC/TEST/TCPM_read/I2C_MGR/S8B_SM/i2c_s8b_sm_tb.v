`include "i2c_s8b_sm.v"
module testbench;
	reg clk;
	
	reg enable;
	wire error;
	wire done;
	
	reg scl;
	wire sda;
	
	reg [7:0] data_in;

s8b_sm master(
	.clk(clk),

	.enable(enable),
	.error(error),
	.done(done),

	.scl(scl),
	.sda(sda),

	.data_in(data_in)
);

initial begin
	clk = 0;
	scl = 0;
	
	enable = 0;
	data_in = 8'b0110010;
	#3000 enable = 1;
end

initial begin
	forever #1 clk = ~clk;
end

initial begin
	forever #200 scl = ~scl;
end

endmodule
