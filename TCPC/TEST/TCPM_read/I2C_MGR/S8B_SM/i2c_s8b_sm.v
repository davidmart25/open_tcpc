//Si se entra en 7, pasar el dato de una vez.
module s8b_sm (
	input clk,
	
	input enable,
	output error,
	output done,
	
	input scl,
	output sda,
	
	input [7:0] data_in
);

/* State REGS */
reg [3:0] state;
reg [3:0] next_state;

/* One Hot encoding */
parameter IDLE = 4'b0001;
parameter SEND_BIT =4'b0010;
parameter DONE_SENDING =4'b0100;
parameter STOP = 4'b1000;

/* Delayed values */
reg scl_d;

/* Next Values*/
reg [7:0] next_data;
reg [3:0] next_count;
reg next_sda;
reg next_done;
reg next_error;

/* Current Values*/ 
reg [7:0] current_data;
reg [3:0] current_count;
reg current_sda;
reg current_done;
reg current_error;

/* Assigns */
assign negedge_scl = ~scl&scl_d;

/*Output assigns */ 
assign sda = current_sda;
assign done = current_done;
assign error = current_error;

/* Next State Logic */
always @(*) begin
	case(state)

	IDLE: begin 
		if(enable)
			next_state = SEND_BIT; 
		else
			next_state = IDLE;	
	end

	SEND_BIT: begin
		if(negedge_scl) begin
			if(current_count == 0)
				next_state = DONE_SENDING;
			else
				next_state = SEND_BIT;
		end
	end
/*
		if(current_count == 0)
			next_state = DONE_SENDING;
		else
			next_state = SEND_BIT;
	end
	*/
	DONE_SENDING: begin
		if(negedge_scl)
			next_state = STOP;
	end
	
	STOP: begin 
		next_state = STOP;
	end
	default: next_state = IDLE;
	endcase
end
/* Next output Logic */
always @(*) begin
	next_data = current_data;
	next_count = current_count;
	next_sda = current_sda;
	next_done = current_done;
	next_error = current_error;

	case(state)

	IDLE: begin
		if(enable) begin
			next_data = data_in;
			next_count = 7;
			next_done = 0;
		end
		else begin //reset all variables
			next_sda = 1; //sda reset at 1
			next_done = 0;
			next_error = 0;
		end
	end

	SEND_BIT: begin
		if(negedge_scl || current_count == 7) begin
			next_sda = current_data[7];
			next_data = current_data << 1;
			next_count = current_count - 1;
		end

	end

	DONE_SENDING: begin
		if(negedge_scl)
			next_done = 1;
	end

	STOP: begin 
		
	end
	default: begin
		next_error = 1;
	end
	endcase
end
/* Sincronic Logic */

always @(posedge clk) begin
	scl_d <= scl;
	if(~enable)
		state <= IDLE;
	else
		state <= next_state;

/* Next Variables become current variables */
	current_data <= next_data;
	current_count <= next_count;
	current_sda <= next_sda;
	current_done <= next_done;
	current_error <= next_error;

end
/*
initial begin
	$dumpfile("s8b_sim.vcd"); //non synth code, simulation only
	$dumpvars;
end
*/
endmodule

