`include "../../TCPC.v"
`include "../../../include/registers.vh"
`include "../../../include/K_code_defines.vh"
`include "../../../include/Vendor_Defines.vh"
`include "../../../include/SOP_defines.vh"
`include "../../register_file/register_file.v"
`include "../../HR/hr_sm.v"
`include "../../RX/rx_sm.v"
`include "../../TX/tx_sm.v"
`include "../../Physical_Layer/Receiver/FINAL_RECEIVER/PHY_receiver.v"
`include "../../Physical_Layer/Receiver/4b5b_decoder/4b5b_decoder.v"
`include "../../Physical_Layer/Receiver/BMC_Decoder/PLL/BMC_to_bin.v"
`include "../../Physical_Layer/Receiver/SOP_Detect/sop_detect.v"
`include "../../Physical_Layer/Receiver/CRC/CRC_32_outputlogic_4b.v"
`include "../../Physical_Layer/Transmiter/PHY_transmiter.v"
`include "../../Physical_Layer/Transmiter/4b5b_Encoder/4b5b_encoder.v"
`include "../../Physical_Layer/Transmiter/BMC/bmc_encoder.v"
`include "../../Physical_Layer/Transmiter/CRC/CRC_32_8b_outputlogic.v"
`include "../../I2C_MGR/DETECT_START_SM/detect_start_sm.v"
`include "../../I2C_MGR/DETECT_STOP_SM/detect_stop_sm.v"
`include "../../I2C_MGR/R8B_SM/r8b_sm.v"
`include "../../I2C_MGR/S8B_SM/i2c_s8b_sm.v"
`include "../../I2C_MGR/SEND_ACK_SM/i2c_send_ack_sm.v"
`include "../../I2C_MGR/SEND_NACK_SM/i2c_send_nack_sm.v"
`include "../../I2C_MGR/RA_SM/i2c_ra_sm.v"
`include "../../I2C_MGR/I2C_SLAVE_MANAGER/i2c_slave_manager.v"

module testbench;

reg pll_bmc_clk; //from PLL
reg bmc_clk; //Internal Independed from pll
reg clk;	

reg reset;

reg cc_in;
reg sda_in;
reg scl_in;
wire [6:0] slave_address;

assign slave_address = 127;
TCPC TCPC_TCPM_Writes(
	.pll_bmc_clk(pll_bmc_clk), //from PLL
	.bmc_clk(bmc_clk), //Internal Independed from pll
	.clk(clk),	
	
	.reset(reset),

	.cc_in(cc_in),
	.sda_in(sda_in),
	.scl_in(scl_in),
	.sda_out(sda_out),
	.slave_address(slave_address)
);
reg send_start_enable;
send_start_sm TCPM_Start(
	.clk(clk),
	
	.enable(send_start_enable),
	//output error,
	.done(send_start_done),
	
	.sda(send_start_sda),
	.scl(scl_in)
);
reg s8b_enable;
reg [7:0] s8b_data_in;
s8b_sm TCPM_Send8b(
	.clk(clk),
	
	.enable(s8b_enable),
	.done(s8b_done),
	
	.scl(scl_in),
	.sda(s8b_sda),
	
	.data_in(s8b_data_in)

);
reg send_stop_enable;
send_stop_sm send_stop(
	.clk(clk),
	
	.enable(send_stop_enable),
	.done(send_stop_done),
	
	.sda(send_stop_sda),
	.scl(scl_in)
);
initial begin
	bmc_clk = 0;
	clk = 0;
	scl_in = 0;
	reset = 1;
	s8b_enable =0;
	send_start_enable = 0;
	send_stop_enable = 0;
	s8b_data_in[7:1] = slave_address;
	s8b_data_in[0] = 0;
	wait (clk == 1) #1 reset = 0;
	send_start_enable = 1;
	wait (send_start_done == 1) #1 send_start_enable = 0;
	s8b_enable =1;
	wait (s8b_done == 1) #1 s8b_enable = 0;
	repeat (1) 
		@(negedge scl_in);
	s8b_data_in = `TRANSMIT_BYTE_COUNT;
	s8b_enable = 1;
	wait (s8b_done ==1) #1 s8b_enable = 0;
	repeat (1)
		@(negedge scl_in);
	s8b_data_in = 8;
	s8b_enable = 1;
	repeat(8) begin
	wait (s8b_done ==1) #1 s8b_enable = 0;
	repeat (1)
		@(negedge scl_in);
	s8b_data_in = s8b_data_in +1;
	s8b_enable = 1;
	end
	wait (s8b_done ==1) #1 s8b_enable = 0;
	send_stop_enable = 1;
	wait (send_stop_done) #1 send_stop_enable = 0;
	send_start_enable = 1;
	wait (send_start_done == 1) #1 send_start_enable = 0;
	s8b_data_in[7:1] = slave_address;
	s8b_data_in[0] = 0;
	s8b_enable = 1;
	wait(s8b_done ==1 ) #1 s8b_enable = 0;
	repeat (1)
		@(negedge scl_in);
	s8b_data_in = `TRANSMIT;
	s8b_enable =1;
	wait(s8b_done == 1) #1 s8b_enable = 0;
	repeat (1)
		@(negedge scl_in);
	s8b_data_in = 0;
	s8b_enable = 1;
	wait(s8b_done == 1) #1 s8b_enable = 0;
	repeat (1)
		@(negedge scl_in);
	send_stop_enable = 1;

end
initial begin
forever #10 clk = ~clk;
end
initial begin
wait(send_start_enable) #1  forever #1000 if(~send_stop_done) scl_in = ~scl_in; else scl_in = 1;
end
initial begin
	forever #10000 bmc_clk = ~bmc_clk;
end
always@(*) begin
	if(send_start_enable)
		sda_in = send_start_sda;
	else if (s8b_enable)
		sda_in = s8b_sda;
	else if (send_stop_enable)
		sda_in = send_stop_sda;
	else	
		sda_in = 1;
end
always@(*) begin
	if(send_stop_done)
		scl_in = 1;
end

initial begin
	$dumpvars;
	$dumpfile("TCPM_write.vcd"); //non synth code, simulation only

end
endmodule

