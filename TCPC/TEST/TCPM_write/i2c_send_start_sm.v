module send_start_sm (
	input clk,
	
	input enable,
	//output error,
	output done,
	
	output sda,
	input scl
);

/* State REGS */
reg [3:0] state;
reg [3:0] next_state;

/* One Hot encoding */
parameter IDLE = 4'b0001;
parameter SEND_START = 4'b0010;
parameter WAIT_SCL_0 = 4'b0100;
parameter STOP = 4'b1000;

/* Next values */
reg next_done;
reg next_sda;

/* Current Values */
reg current_done;
reg current_sda;

/* Assigns*/

/* Output Assigns */ 
assign done = current_done;
assign sda = current_sda;

/* Next State Logic */ 
always@(*) begin
	case(state)
	IDLE: begin
		if(enable)
			next_state = SEND_START;
		else
			next_state = IDLE;
	end	
	SEND_START: begin
		if(scl)
			next_state = WAIT_SCL_0;
		else
			next_state = SEND_START;
	end
	WAIT_SCL_0: begin
		if(~scl)
			next_state = STOP;
		else
			next_state = WAIT_SCL_0;
	end
	STOP: begin
		next_state = STOP;
	end
	endcase
end

/* Next Output logic */
always @(*) begin
	next_sda = current_sda;
	next_done  = current_done;
	case(state)
	IDLE:
		if(enable) begin
			if(scl)
				next_sda = 0;
			else
				next_sda = 1;
			next_done = 0;
		end		
		else begin //resets all variables
			next_sda = 1;
			next_done = 0;			
		end
	SEND_START: begin
		next_done = 0;
		if(scl)
			next_sda = 0;
		else
			next_sda = 1;
	end
	WAIT_SCL_0: begin
		if(~scl) begin
			next_done = 1;
			next_sda = 0;
		end
	end
	
	endcase
			
end

/*Sincronic Logic */
always @(posedge clk) begin
	if(~enable)
		state <= IDLE;
	else
		state <= next_state;

	current_sda = next_sda;
	current_done = next_done;
end
/*
initial begin
	$dumpfile("send_start_sim.vcd"); //non synth code, simulation only
	$dumpvars;
end
*/
endmodule
/*Simulation Code */ 




