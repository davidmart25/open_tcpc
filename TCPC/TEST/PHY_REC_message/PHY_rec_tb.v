`include "../../TCPC.v"
`include "../../../include/registers.vh"
`include "../../../include/K_code_defines.vh"
`include "../../../include/Vendor_Defines.vh"
`include "../../../include/SOP_defines.vh"
`include "../../register_file/register_file.v"
`include "../../HR/hr_sm.v"
`include "../../RX/rx_sm.v"
`include "../../TX/tx_sm.v"
`include "../../Physical_Layer/Receiver/FINAL_RECEIVER/PHY_receiver.v"
`include "../../Physical_Layer/Receiver/4b5b_decoder/4b5b_decoder.v"
`include "../../Physical_Layer/Receiver/BMC_Decoder/PLL/BMC_to_bin.v"
`include "../../Physical_Layer/Receiver/SOP_Detect/sop_detect.v"
`include "../../Physical_Layer/Receiver/CRC/CRC_32_outputlogic_4b.v"
`include "../../Physical_Layer/Transmiter/PHY_transmiter.v"
`include "../../Physical_Layer/Transmiter/4b5b_Encoder/4b5b_encoder.v"
`include "../../Physical_Layer/Transmiter/BMC/bmc_encoder.v"
`include "../../Physical_Layer/Transmiter/CRC/CRC_32_8b_outputlogic.v"
`include "../../I2C_MGR/DETECT_START_SM/detect_start_sm.v"
`include "../../I2C_MGR/DETECT_STOP_SM/detect_stop_sm.v"
`include "../../I2C_MGR/R8B_SM/r8b_sm.v"
`include "../../I2C_MGR/S8B_SM/i2c_s8b_sm.v"
`include "../../I2C_MGR/SEND_ACK_SM/i2c_send_ack_sm.v"
`include "../../I2C_MGR/SEND_NACK_SM/i2c_send_nack_sm.v"
`include "../../I2C_MGR/RA_SM/i2c_ra_sm.v"
`include "../../I2C_MGR/I2C_SLAVE_MANAGER/i2c_slave_manager.v"
module testbench;
reg reset;
reg clk;
reg bmc_clk;
reg [7:0] data_in_from_reg;
wire load_done;
reg phy_transmiter_enable;
wire [19:0] SOP;
assign SOP = {`SYNC_2,`SYNC_1,`SYNC_1,`SYNC_1};
wire [7:0] reg_address;
reg preamble_done;
//assign Extended = message_header[15];
//assign Number_of_Data_Objects = message_header[14:12];
reg pll_bmc_clk;
reg phy_receiver_enable;
wire [15:0] RX_HEADER;
wire [6:0] slave_address;
assign slave_address = 127;
assign load_done = 1;
always@(posedge bmc_clk) begin
	pll_bmc_clk <= ~pll_bmc_clk;
end
phy_transmiter trans_test(
	.TRANSMIT(0),
	.clk(clk),
	.bmc_clk(bmc_clk),
	.data_in_from_reg(data_in_from_reg),
	.load_done(load_done),
	.enable(phy_transmiter_enable),
	.reg_address(reg_address),
	.load(load),
	.cc(cc),
	.done(trans_done)
);
phy_receiver test_receiver(
	.clk(clk),
	.pll_bmc_clk(~pll_bmc_clk),
	.enable(phy_receiver_enable),
	.preamble_done(preamble_done),
	.RECEIVE_DETECT(8'hFF),
	.rx_buffer_full(0),
	.RX_BUF_HEADER(16'b0001000000000000),
	.cc(cc)
);
TCPC TCPC_reset(
	.pll_bmc_clk(~pll_bmc_clk), //from PLL
	.bmc_clk(bmc_clk), //Internal Independed from pll
	.clk(clk),	
	
	.reset(reset),

	.cc_in(cc),
	.preamble_done(preamble_done),
	//.sda_in(sda_in),
	//.scl_in(scl_in),
	
	.slave_address(slave_address)
);
initial begin
	reset= 0;
	preamble_done = 0;
	clk =0;
	bmc_clk = 0;
	pll_bmc_clk = 0;
	data_in_from_reg = 6;
	phy_transmiter_enable =0;
	phy_receiver_enable = 0;
	#10 reset = 1;
	#10 reset = 0;
	#1000 phy_transmiter_enable =1;
	phy_receiver_enable = 1;
end

initial begin
	forever #1 clk = ~clk;
end
initial begin
	forever #100 bmc_clk = ~bmc_clk;
end
initial begin
	$dumpfile("PHY_rec.vcd"); //non synth code, simulation only
	$dumpvars;
end
initial #27270 preamble_done = 1;




endmodule
