`include "../TCPC.v"
`include "../../include/registers.vh"
`include "../../include/K_code_defines.vh"
`include "../../include/Vendor_Defines.vh"
`include "../../include/SOP_defines.vh"
`include "../register_file/register_file.v"
`include "../HR/hr_sm.v"
`include "../RX/rx_sm.v"
`include "../TX/tx_sm.v"
`include "../Physical_Layer/Receiver/New/PHY_receiver.v"
`include "../Physical_Layer/Receiver/4b5b_decoder/4b5b_decoder.v"
`include "../Physical_Layer/Receiver/BMC_Decoder/PLL/BMC_to_bin.v"
`include "../Physical_Layer/Receiver/SOP_Detect/sop_detect.v"
`include "../Physical_Layer/Receiver/CRC/CRC_32_outputlogic_4b.v"
`include "../Physical_Layer/Transmiter/PHY_transmiter.v"
`include "../Physical_Layer/Transmiter/4b5b_Encoder/4b5b_encoder.v"
`include "../Physical_Layer/Transmiter/BMC/bmc_encoder.v"
`include "../Physical_Layer/Transmiter/CRC/CRC_32_8b_outputlogic.v"
`include "../I2C_MGR/DETECT_START_SM/detect_start_sm.v"
`include "../I2C_MGR/DETECT_STOP_SM/detect_stop_sm.v"
`include "../I2C_MGR/R8B_SM/r8b_sm.v"
`include "../I2C_MGR/S8B_SM/i2c_s8b_sm.v"
`include "../I2C_MGR/SEND_ACK_SM/i2c_send_ack_sm.v"
`include "../I2C_MGR/SEND_NACK_SM/i2c_send_nack_sm.v"
`include "../I2C_MGR/RA_SM/i2c_ra_sm.v"
`include "../I2C_MGR/I2C_SLAVE_MANAGER/i2c_slave_manager.v"
module testbench;

reg pll_bmc_clk; //from PLL
reg bmc_clk; //Internal Independed from pll
reg clk;	

reg reset;

reg cc_in;
reg sda_in;
reg scl_in;
wire [6:0] slave_address;

assign slave_address = 127;

TCPC TCPC_reset(
	.pll_bmc_clk(pll_bmc_clk), //from PLL
	.bmc_clk(bmc_clk), //Internal Independed from pll
	.clk(clk),	
	
	.reset(reset),

	.cc_in(cc_in),
	.sda_in(sda_in),
	.scl_in(scl_in),
	
	.slave_address(slave_address)
);
initial begin
	pll_bmc_clk = 0;
	bmc_clk = 0;
	clk = 0;
	reset = 0;
	cc_in = 0;
	sda_in = 0;
	scl_in =0;
	#10000 reset = 1;
	#20000 reset = 0;
end
initial begin
	forever #1 clk = ~clk;
end
initial begin
	forever #1000 bmc_clk = ~bmc_clk;
end

always@(posedge bmc_clk)
	pll_bmc_clk = ~pll_bmc_clk;
	
initial begin
	$dumpfile("power_on_reset.vcd"); //non synth code, simulation only
	$dumpvars;
end
endmodule