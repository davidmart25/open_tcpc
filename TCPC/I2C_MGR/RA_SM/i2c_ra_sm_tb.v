`include "i2c_ra_sm.v"
module testbench;

	reg clk;
	reg enable;

	reg sda;
	reg scl;
	
	wire nack_received;
	wire ack_received;

ra_sm master(
	.clk(clk),
	.enable(enable),

	.sda(sda),
	.scl(scl),
	
	.nack_received(nack_received),
	.ack_received(ack_received)
);

initial begin
	clk = 0;
	scl = 0;
	enable = 0;
	sda =1;
	#10 enable = 1;

/* Receive Ack */
	#200 sda = 0;
	scl= 1;
	#200 scl = 0; 

	#20 enable = 1;
/* Receive Nack */
	sda = 1;
	#200 scl = 1;
	#200 scl = 0;
	  
end

initial begin
	forever #1 clk = ~clk;
end

always @(posedge clk) begin
	if(nack_received || ack_received)
		enable <= 0;
end

endmodule
