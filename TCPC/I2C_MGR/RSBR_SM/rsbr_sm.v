module rsbr_sm (
	input clk,
	input enable,
	output error,
	output done,

	input [6:0] slave_address,
	input [7:0] reg_address,
	output [7:0] read_data,

	output s8b_enable,	
	output [7:0] s8b_data_in,
	input s8b_done,
	input s8b_error,	

	output r8b_enable,
	input r8b_done,
	input r8b_error,
	input [7:0] r8b_data_out,	
	
	output send_start_enable,
	input send_start_done,

	output ra_enable,
	input ra_nack_received,
	input ra_ack_received,

	output send_stop_enable,
	input send_stop_done,

	output send_nack_enable,
	input send_nack_done,
	output [7:0] data_out
);

/* State REGS */ 

reg [13:0] state;
reg [13:0] next_state;

/* One Hot Encoding */ 

parameter IDLE =              14'b00000000000001;
parameter SEND_START_1 =      14'b00000000000010;
parameter SEND_ADDRESS_1 =    14'b00000000000100;
parameter RECEIVE_ACK_1 =     14'b00000000001000;
parameter SEND_REG_ADDRESS =  14'b00000000010000;
parameter RECEIVE_ACK_2 =     14'b00000000100000;
parameter SEND_START_2 =      14'b00000001000000;
parameter SEND_ADDRESS_2 =    14'b00000010000000;
parameter RECEIVE_ACK_3 =     14'b00000100000000;
parameter RECEIVE_READ_DATA = 14'b00001000000000;
parameter SEND_NACK =         14'b00010000000000;
parameter SEND_STOP = 				14'b00100000000000;
parameter TRANSACTION_ERROR = 14'b01000000000000;
parameter STOP =							14'b10000000000000;

/* Next values */
reg [6:0] next_slave_address;
reg [7:0] next_reg_address;
reg [7:0] next_read_data;

reg next_error;
reg next_done;

reg [7:0] next_s8b_data_in;

reg next_s8b_enable;
reg next_r8b_enable;
reg next_send_start_enable;
reg next_ra_enable;
reg next_send_stop_enable;
reg next_send_nack_enable;

/* Current Values */
reg [6:0] current_slave_address;
reg [7:0] current_reg_address;
reg [7:0] current_read_data;

reg current_error;
reg current_done;

reg [7:0] current_s8b_data_in;

reg current_s8b_enable;
reg current_r8b_enable;
reg current_send_start_enable;
reg current_ra_enable;
reg current_send_stop_enable;
reg current_send_nack_enable;

/* Assigns */ 
 
/* Output Assigns */ 
assign error = current_error;
assign done = current_done;
assign read_data = current_read_data;
assign data_out = r8b_data_out;

assign r8b_enable = current_r8b_enable;
assign s8b_enable = current_s8b_enable;
assign s8b_data_in = current_s8b_data_in;

assign send_start_enable = current_send_start_enable;

assign ra_enable = current_ra_enable;

assign send_stop_enable = current_send_stop_enable;

assign send_nack_enable = current_send_nack_enable;

/*Next State Logic */ 

always @(*) begin
	case(state)
	IDLE: begin
		if(enable)
			next_state = SEND_START_1;
		else
			next_state = IDLE;
	end

	SEND_START_1: begin
		if(send_start_done) 
			next_state = SEND_ADDRESS_1;
		else
			next_state = SEND_START_1;
	end
	
	SEND_ADDRESS_1: begin
		if(s8b_error) 
			next_state = TRANSACTION_ERROR;
		else if(s8b_done)
			next_state = RECEIVE_ACK_1;
		else
			next_state = SEND_ADDRESS_1;
	end

	RECEIVE_ACK_1: begin
		if(ra_nack_received)
			next_state = TRANSACTION_ERROR;
		else if (ra_ack_received)
			next_state = SEND_REG_ADDRESS;
		else
			next_state = RECEIVE_ACK_1;
	end

	SEND_REG_ADDRESS: begin
		if(s8b_error)
			next_state = TRANSACTION_ERROR;
		else if(s8b_done)
			next_state = RECEIVE_ACK_2;
		else
			next_state = SEND_REG_ADDRESS;
	end
		
	RECEIVE_ACK_2: begin
		if(ra_nack_received)
			next_state = TRANSACTION_ERROR;
		else if(ra_ack_received)
			next_state = SEND_START_2;
		else
			next_state = RECEIVE_ACK_2;
	end


	SEND_START_2: begin
		if(send_start_done)
			next_state = SEND_ADDRESS_2;
		else
			next_state = SEND_START_2;
	end

	SEND_ADDRESS_2: begin
		if(s8b_error)
			next_state = TRANSACTION_ERROR;
		else if(s8b_done)
			next_state = RECEIVE_ACK_3;
		else
			next_state = SEND_ADDRESS_2;
	end	

	RECEIVE_ACK_3: begin
		if(ra_nack_received)
			next_state = TRANSACTION_ERROR;
		else if (ra_ack_received)
			next_state = RECEIVE_READ_DATA;
		else 
			next_state = RECEIVE_ACK_3;
	end

	RECEIVE_READ_DATA: begin
		if(r8b_error)
			next_state = TRANSACTION_ERROR;
		else if(r8b_done)
			next_state = SEND_NACK;
		else
			next_state = RECEIVE_READ_DATA;
	end

	SEND_NACK: begin
		if(send_nack_done)
			next_state = SEND_STOP;
		else
			next_state = SEND_NACK;
	end

	SEND_STOP: begin
		if(send_stop_done)
			next_state = STOP;
		else
			next_state = SEND_STOP;
	end
	
	STOP: next_state = STOP;

	TRANSACTION_ERROR: next_state = STOP;

	default: next_state = TRANSACTION_ERROR;		
	
	endcase	
end

/* Next Output Logic */ 
always @(*) begin
	next_slave_address = current_slave_address;
	next_reg_address = current_reg_address; 
	next_read_data = current_read_data;

	next_error = current_error;
	next_done = current_done;

	next_s8b_data_in = current_s8b_data_in;

	next_s8b_enable = current_s8b_enable;
	next_r8b_enable = current_r8b_enable;
	next_send_start_enable = current_send_start_enable;
	next_ra_enable = current_ra_enable;
	next_send_stop_enable = current_send_stop_enable;
	next_send_nack_enable = current_send_nack_enable;

	case(state)
	IDLE: begin
		if(enable) begin
			next_slave_address = slave_address;
			next_reg_address = reg_address;
		end
		else begin
			next_s8b_enable = 0;
			next_r8b_enable = 0;
			next_send_start_enable = 0;
			next_ra_enable = 0;
			next_send_stop_enable = 0;
			next_send_nack_enable = 0;			
		end
	end
	SEND_START_1: begin
		next_s8b_enable = 0;
		next_r8b_enable = 0;
		next_send_start_enable = 1;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_send_nack_enable = 0;	
	end
	SEND_ADDRESS_1: begin
		next_error = s8b_error;
		next_s8b_data_in [7:1] = current_slave_address;
		next_s8b_data_in [0] = 0; //WRITE		

		next_s8b_enable = 1;
		next_r8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_send_nack_enable = 0;	
	end
	RECEIVE_ACK_1: begin
		next_error = ra_nack_received;

		next_s8b_enable = 0;
		next_r8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 1;
		next_send_stop_enable = 0;
		next_send_nack_enable = 0;		
	end
	SEND_REG_ADDRESS: begin
		next_error = s8b_error;
		next_s8b_data_in = current_reg_address;

		next_s8b_enable = 1;
		next_r8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 00;
		next_send_stop_enable = 0;
		next_send_nack_enable = 0;	
	end
	RECEIVE_ACK_2: begin
		next_error = ra_nack_received;

		next_s8b_enable = 0;
		next_r8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 1;
		next_send_stop_enable = 0;
		next_send_nack_enable = 0;
	end
	SEND_START_2: begin
		next_s8b_enable = 0;
		next_r8b_enable = 0;
		next_send_start_enable = 1;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_send_nack_enable = 0;			
	end

	SEND_ADDRESS_2: begin
		next_error = s8b_error;
		next_s8b_data_in [7:1] = current_slave_address;
		next_s8b_data_in [0] = 1; // READ

		next_s8b_enable = 1;
		next_r8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 00;
		next_send_stop_enable = 0;
		next_send_nack_enable = 0;
	end

	RECEIVE_ACK_3: begin
		next_error = ra_nack_received;

		next_s8b_enable = 0;
		next_r8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 1;
		next_send_stop_enable = 0;
		next_send_nack_enable = 0;
	end

	RECEIVE_READ_DATA: begin
		next_error = r8b_error;
		
		next_s8b_enable = 0;
		next_r8b_enable = 1;
		next_send_start_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_send_nack_enable = 0;
	end
	
	SEND_NACK: begin
		next_s8b_enable = 0;
		next_r8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_send_nack_enable = 1;
	end

	SEND_STOP: begin
		if(send_stop_done)
			next_done = 1;
		else
			next_done = 0;

		next_s8b_enable = 0;
		next_r8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 1;
		next_send_nack_enable = 0;
	end
	
	TRANSACTION_ERROR:begin
		next_error = 1;
		next_done = 0;

		next_s8b_enable = 0;
		next_r8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_send_nack_enable = 0;
	end

	STOP: begin

	end
	
	default: begin
		next_error = 1;
		next_done = 0;

		next_s8b_enable = 0;
		next_r8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_send_nack_enable = 0;		
	end
			
	endcase

end

/* Sincorinic Logic */ 

always @(posedge clk) begin
	if(~enable)
		state <= IDLE;
	else
		state <= next_state;

	current_slave_address <= next_slave_address;
	current_reg_address <= next_reg_address; 
	current_read_data <= next_read_data;

	current_error <= next_error;
	current_done <= next_done;

	current_s8b_data_in <= next_s8b_data_in;

	current_s8b_enable <= next_s8b_enable;
	current_r8b_enable <= next_r8b_enable;
	current_send_start_enable <= next_send_start_enable;
	current_ra_enable <= next_ra_enable;
	current_send_stop_enable <= next_send_stop_enable;
	current_send_nack_enable <= next_send_nack_enable;
	
end

initial begin
	$dumpfile("rsbr_sim.vcd"); //non synth code, simulation only
	$dumpvars;
end

endmodule




