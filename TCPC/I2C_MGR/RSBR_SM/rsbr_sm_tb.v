`include "rsbr_sm.v"
`include "../RA_SM/i2c_ra_sm.v"
`include "../S8B_SM/i2c_s8b_sm.v"
`include "../SEND_STOP_SM/i2c_send_stop_sm.v"
`include "../SEND_START_SM/i2c_send_start_sm.v"
`include "../R8B_SM/r8b_sm.v"
`include "../SEND_NACK_SM/i2c_send_nack_sm.v"


module testbench;

	reg clk;
	reg rsbr_enable;
	wire error;
	wire done;

	reg [6:0] slave_address;
	reg [7:0] reg_address;
	wire [7:0] read_data;

	wire s8b_enable;	
	wire [7:0] s8b_data_in;
	wire s8b_done;
	wire s8b_error;	

	wire r8b_enable;
	wire r8b_done;
	wire r8b_error;
	wire [7:0] r8b_data_out;
	
	reg r8b_sda;
	reg sda;	
	
	wire send_start_enable;
	wire send_start_done;

	wire ra_enable;
	wire ra_nack_received;
	wire ra_ack_received;

	wire send_stop_enable;
	wire send_stop_done;

	wire send_nack_enable;
	wire send_nack_done;


assign ra_sda = 0;
reg scl;
reg start_scl;
rsbr_sm rsbr(
	.clk(clk),
	.enable(rsbr_enable),
	.error(rsbr_error),
	.done(rsbr_done),

	.slave_address(slave_address),
	.reg_address(reg_address),
	.read_data(read_data),

	.s8b_enable(s8b_enable),	
	.s8b_data_in(s8b_data_in),
	.s8b_done(s8b_done),
	.s8b_error(s8b_error),	

	.r8b_enable(r8b_enable),
	.r8b_done(r8b_done),
	.r8b_error(r8b_error),
	.r8b_data_out(r8b_data_out),	

	.send_start_enable(send_start_enable),
	.send_start_done(send_start_done),

	.ra_enable(ra_enable),
	.ra_nack_received(ra_nack_received),
	.ra_ack_received(ra_ack_received),

	.send_stop_enable(send_stop_enable),
	.send_stop_done(send_stop_done),

	.send_nack_enable(send_nack_enable),
	.send_nack_done(send_nack_done)
	);
ra_sm ra(
	.clk(clk),
	.enable(ra_enable),

	.sda(ra_sda),
	.scl(scl),

	.nack_received(ra_nack_received),
	.ack_received(ra_ack_received)
);

r8b_sm r8b(
	.clk(clk),
	.enable(r8b_enable),
	
	.scl(scl),
	.sda(r8b_sda),
	
	.done(r8b_done),
	.error(r8b_error),
	.data_out(r8b_data_out)
);

s8b_sm s8b(
	.clk(clk),

	.enable(s8b_enable),
	.error(s8b_error),
	.done(s8b_done),

	.scl(scl),
	.sda(s8b_sda),

	.data_in(s8b_data_in)
	
);
r8b_sm r8b(
	.clk(clk),
	.enable(r8b_enable),

	.scl(scl),
	.sda(r8b_sda),
	
	.done(r8b_done),
	.error(r8b_error),

	.data_out(r8b_data_out)	
);

send_stop_sm send_stop(
	.clk(clk),
	
	.enable(send_stop_enable),
	.done(send_stop_done),
	
	.sda(send_stop_sda),
	.scl(scl)
);

send_start_sm send_start(
	.clk(clk),

	.enable(send_start_enable),
	.done(send_start_done),

	.sda(send_start_sda),
	.scl(scl)
	
);

send_nack_sm send_nack(
	.clk(clk),
	.enable(send_nack_enable),
	
	.sda(send_nack_sda),
	.scl(scl),
	.done(send_nack_done)
);


always @(*) begin
	if(s8b_enable)
		sda =s8b_sda;
	else if (send_stop_enable)
		sda = send_stop_sda;
	else if (send_start_enable)
		sda = send_start_sda;
	else if (r8b_enable)
		sda = r8b_sda;
	else if (send_nack_enable)
		sda = send_nack_sda;
	else
		sda = 1;
end

initial begin
	r8b_sda = 1;
	clk = 0;
	scl = 1;
	rsbr_enable = 0;
	start_scl = 0;
	slave_address = 8'hCA;
	reg_address = 8'hFE;
	#20 rsbr_enable = 1;

end

//Logica para el r8b_sda 
always @(*) begin
	if(s8b_enable)
		sda = s8b_sda;
	else if(send_stop_enable)
		sda =  send_stop_sda;
	else if(send_start_enable)
		sda = send_start_sda;
	else if(r8b_enable)
		sda = r8b_sda;
	else
		sda = 1
end
initial begin
	forever #1 clk = ~clk;
end

initial begin
	forever begin
		if(start_scl)
			#200 scl = ~scl;
		else
			#200 scl = scl;
	end
end

always @(*) begin
	if(send_start_enable)
		#20 start_scl = 1;
end

always @(negedge scl) begin
	if(r8b_enable)
		r8b_sda <= ~r8b_sda;
end

endmodule
