`include "../../../include/registers.vh"

module rrb_sm (
	input clk,
	input enable,
	
	output error,
	output done,
	
	input [6:0] slave_address,
	
	output [7:0] data_out,
	output data_ready,
	
	output s8b_enable,
	output [7:0] s8b_data_in,
	input s8b_done,
	input s8b_error,
	
	output send_start_enable,
	input send_start_done,
	
	output ra_enable,
	input ra_nack_received,
	input ra_ack_received,
	
	output send_ack_enable,
	input send_ack_done,
	
	output send_nack_enable,
	input send_nack_done,
	
	output send_stop_enable,
	input send_stop_done,
	
	output r8b_enable,
	input r8b_done,
	input r8b_error,
	input [7:0] r8b_data_out	
);

/* State REGs */ 
reg [15:0] state;
reg [15:0]  next_state;

/* One Hot encoding */
parameter IDLE =              16'b0000000000000001;
parameter SEND_START_1 =      16'b0000000000000010;
parameter SEND_ADDRESS_1 =    16'b0000000000000100;
parameter RECEIVE_ACK_1 =     16'b0000000000001000;
parameter SEND_REG_ADDRESS =  16'b0000000000010000;
parameter RECEIVE_ACK_2 =     16'b0000000000100000;
parameter SEND_START_2 =      16'b0000000001000000;
parameter SEND_ADDRESS_2 =    16'b0000000010000000;
parameter RECEIVE_ACK_3 =     16'b0000000100000000;
parameter RECEIVE_B_COUNT =   16'b0000001000000000;
parameter SEND_ACK_N =        16'b0000010000000000;
parameter RECEIVE_BYTE =      16'b0000100000000000;
parameter SEND_NACK =         16'b0001000000000000;
parameter SEND_STOP =         16'b0010000000000000;
parameter TRANSACTION_ERROR = 16'b0100000000000000;
parameter STOP =              16'b1000000000000000;


/* Next Values */ 
reg [7:0] next_M;
reg [7:0] next_byte_count;

reg next_error;
reg next_done;

reg [7:0] next_data_out;
reg next_data_ready;
reg next_s8b_enable;
reg [7:0] next_s8b_data_in;
reg next_send_start_enable;
reg next_ra_enable;
reg next_send_stop_enable;
reg next_r8b_enable;
reg next_send_nack_enable;
reg next_send_ack_enable;

reg [6:0] next_slave_address;
/* Current Values */ 
reg [7:0] current_M;
reg [7:0] current_byte_count;

reg current_error;
reg current_done;

reg [7:0] current_data_out;
reg current_data_ready;
reg current_s8b_enable;
reg [7:0] current_s8b_data_in;
reg current_send_start_enable;
reg current_ra_enable;
reg current_send_stop_enable;
reg current_r8b_enable;
reg current_send_nack_enable;
reg current_send_ack_enable;

reg [6:0] current_slave_address;
/*Delayed Values */

/*Assigns */

/* Output Assigns */
assign error = current_error;
assign done = current_done;

assign data_out = current_data_out;
assign data_ready = current_data_ready;
assign s8b_enable = current_s8b_enable;
assign s8b_data_in = current_s8b_data_in;
assign send_start_enable = current_send_start_enable;
assign ra_enable = current_ra_enable;
assign send_stop_enable = current_send_stop_enable; 
assign r8b_enable = current_r8b_enable;
assign send_ack_enable = current_send_ack_enable;
assign send_nack_enable = current_send_nack_enable;


/* Next State Logic */
always @(*) begin
	case(state)
	IDLE: begin
		if(enable)
			next_state = SEND_START_1;
		else
			next_state = IDLE;
	end
	SEND_START_1: begin
		if(send_start_done)
			next_state = SEND_ADDRESS_1;
		else
			next_state = SEND_START_1;
	end
	SEND_ADDRESS_1: begin
		if(s8b_error)
			next_state = TRANSACTION_ERROR;
		else if (s8b_done)
			next_state = RECEIVE_ACK_1;
		else
			next_state = SEND_ADDRESS_1;
	end
	RECEIVE_ACK_1: begin
		if(ra_nack_received)
			next_state = TRANSACTION_ERROR;
		else if (ra_ack_received)
			next_state = SEND_REG_ADDRESS;
		else
			next_state = RECEIVE_ACK_1;
	end
	SEND_REG_ADDRESS: begin
		if(s8b_error)
			next_state = TRANSACTION_ERROR;
		else if(s8b_done)
			next_state = RECEIVE_ACK_2;
		else
			next_state = SEND_REG_ADDRESS;
	end
	RECEIVE_ACK_2: begin
		if(ra_nack_received)
			next_state = TRANSACTION_ERROR;
		else if(ra_ack_received)
			next_state = SEND_START_2;
		else
			next_state = RECEIVE_ACK_2;
	end
	
	SEND_START_2: begin
		if(send_start_done)
			next_state = SEND_ADDRESS_2;
		else
			next_state = SEND_START_2;
	end
	SEND_ADDRESS_2: begin
		if(s8b_error)
			next_state = TRANSACTION_ERROR;
		else if (s8b_done)
			next_state = RECEIVE_ACK_3;
		else
			next_state = SEND_ADDRESS_2;		
	end
	RECEIVE_ACK_3: begin
		if(ra_nack_received)
			next_state = TRANSACTION_ERROR;
		else if(ra_ack_received)
			next_state = RECEIVE_B_COUNT;
		else
			next_state = RECEIVE_ACK_3;	
	end
	RECEIVE_B_COUNT: begin
		if(r8b_error)
			next_state = TRANSACTION_ERROR;
		else if(r8b_done)
			next_state = SEND_ACK_N;
		else
			next_state = RECEIVE_B_COUNT;
			
	end
	SEND_ACK_N: begin
		if(current_byte_count == current_M) begin
			if(send_ack_done)
				next_state = SEND_NACK;
			else
				next_state = SEND_ACK_N;
		end
		else begin
			if(send_ack_done)
				next_state = RECEIVE_BYTE;
			else
				next_state = SEND_ACK_N;
		end
			
	end
	RECEIVE_BYTE: begin
		if(r8b_error)
			next_state = TRANSACTION_ERROR;
		else if (r8b_done)
			next_state = SEND_ACK_N;
		else
			next_state = RECEIVE_BYTE;
	end
	SEND_NACK: begin
		if(send_nack_done)
			next_state = SEND_STOP;
		else
			next_state = SEND_NACK;
	end
	SEND_STOP: begin
		if(send_stop_done)
			next_state = STOP;
		else
			next_state = SEND_STOP;
	end
	TRANSACTION_ERROR: next_state = STOP;
	STOP: next_state = STOP;
	endcase
end

/* Next output Logic */
always @(*) begin
	next_M = current_M;
	next_byte_count = current_byte_count;

	next_error = current_error;
	next_done = current_done;

	next_data_out = current_data_out;
	next_data_ready = current_data_ready;
	next_s8b_enable = current_s8b_enable;
	next_s8b_data_in = current_s8b_data_in;
	next_send_start_enable = current_send_start_enable;
	next_ra_enable = current_ra_enable;
	next_send_stop_enable = current_send_stop_enable;
	next_r8b_enable = current_r8b_enable;
	next_send_ack_enable = current_send_ack_enable;
	next_send_nack_enable = current_send_nack_enable;
	
	next_slave_address = current_slave_address;
	
	case(state)
	IDLE: begin
		if(enable) begin
			next_slave_address = slave_address;
		end
		else begin
			next_s8b_enable = 0;
			next_send_start_enable = 0;
			next_ra_enable = 0;
			next_send_stop_enable = 0;
			next_r8b_enable = 0;
			next_send_ack_enable = 0;
			next_send_nack_enable = 0;
		end
	end	
	SEND_START_1: begin
		next_s8b_enable = 0;
		next_send_start_enable = 1;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_r8b_enable = 0;
		next_send_ack_enable = 0;
		next_send_nack_enable = 0;
	end
	SEND_ADDRESS_1: begin
		next_error = s8b_error;
		next_s8b_data_in [7:1] = current_slave_address;
		next_s8b_data_in [0] = 0; //WRITE	

		next_s8b_enable = 1;
		next_send_start_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_r8b_enable = 0;
		next_send_ack_enable = 0;
		next_send_nack_enable = 0;		

	end
	RECEIVE_ACK_1: begin
		next_error = ra_nack_received;
		
		next_s8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 1;
		next_send_stop_enable = 0;
		next_r8b_enable = 0;
		next_send_ack_enable = 0;
		next_send_nack_enable = 0;	
	end
	SEND_REG_ADDRESS: begin
		next_error = s8b_error;
		next_s8b_data_in = `RECEIVE_BYTE_COUNT;
		
		next_s8b_enable = 1;
		next_send_start_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_r8b_enable = 0;
		next_send_ack_enable = 0;
		next_send_nack_enable = 0;
	end
	RECEIVE_ACK_2: begin
		next_error = ra_nack_received;
		
		next_s8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 1;
		next_send_stop_enable = 0;
		next_r8b_enable = 0;
		next_send_ack_enable = 0;
		next_send_nack_enable = 0;
	end
	SEND_START_2: begin
		next_s8b_enable = 0;
		next_send_start_enable = 1;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_r8b_enable = 0;
		next_send_ack_enable = 0;
		next_send_nack_enable = 0;
	end
	SEND_ADDRESS_2: begin
		next_error = s8b_error;
		next_s8b_data_in [7:1] = current_slave_address;
		next_s8b_data_in [0] = 1; //WRITE	

		next_s8b_enable = 1;
		next_send_start_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_r8b_enable = 0;
		next_send_ack_enable = 0;
		next_send_nack_enable = 0;		
	end
	RECEIVE_ACK_3: begin
		next_error = ra_nack_received;
		
		next_s8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 1;
		next_send_stop_enable = 0;
		next_r8b_enable = 0;
		next_send_ack_enable = 0;
		next_send_nack_enable = 0;
	end
	RECEIVE_B_COUNT: begin
		next_error = r8b_error;
		if (r8b_done) begin
			next_data_out = r8b_data_out;
			next_data_ready = 1;
			next_M = r8b_data_out;
			next_byte_count = 0;
		end
		
		next_s8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_r8b_enable = 1;
		next_send_ack_enable = 0;
		next_send_nack_enable = 0;
			
	end
	SEND_ACK_N: begin
		next_data_ready = 0;
		if(send_ack_done)
			next_byte_count = current_byte_count +1;
	
		next_s8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_r8b_enable = 0;
		next_send_ack_enable = 1;
		next_send_nack_enable = 0;
	
	end
	RECEIVE_BYTE: begin
		next_error = r8b_error;
		if(r8b_done) begin
			next_data_out = r8b_data_out;
			next_data_ready = 1;
		end
		
		next_s8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_r8b_enable = 1;
		next_send_ack_enable = 0;
		next_send_nack_enable = 0;
	end

	SEND_NACK: begin
		next_s8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_r8b_enable = 0;
		next_send_ack_enable = 0;
		next_send_nack_enable = 1;
	end
	SEND_STOP: begin
		if(send_stop_done)
			next_done = 1;
		next_s8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 1;
		next_r8b_enable = 0;
		next_send_ack_enable = 0;
		next_send_nack_enable = 0;
	end
	TRANSACTION_ERROR: begin
		next_error = 1;
		
		next_s8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_r8b_enable = 0;
		next_send_ack_enable = 0;
		next_send_nack_enable = 0;
	end
	
	STOP: begin
		
		next_s8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_r8b_enable = 0;
		next_send_ack_enable = 0;
		next_send_nack_enable = 0;
	end
	endcase
	
end
/* Sincronic Logic */
always @(posedge clk) begin
	if(~enable)
		state <= IDLE;
	else
		state <= next_state;

	current_M <= next_M;
	current_byte_count <= next_byte_count;

	current_error <= next_error;
	current_done <= next_done;

	current_data_out <= next_data_out;
	current_data_ready <= next_data_ready ;
	current_s8b_enable <= next_s8b_enable ;
	current_s8b_data_in <= next_s8b_data_in ;
	current_send_start_enable <= next_send_start_enable ;
	current_ra_enable <= next_ra_enable ;
	current_send_stop_enable <= next_send_stop_enable ;
	current_r8b_enable <= next_r8b_enable ;
	current_send_nack_enable <= next_send_nack_enable ;
	current_send_ack_enable <= next_send_ack_enable ;

	current_slave_address <= next_slave_address ;
end
initial begin
	$dumpfile("rmbr_sim.vcd"); //non synth code, simulation only
	$dumpvars;
end
endmodule
















