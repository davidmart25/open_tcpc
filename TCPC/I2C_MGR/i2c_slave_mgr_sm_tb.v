`include "i2c_slave_mgr_sm.v"
`include "SEND_START_SM/i2c_send_start_sm.v"
`include "SEND_STOP_SM/i2c_send_stop_sm.v"
module testbench;
//Input Regs//
	reg clk;
	reg enable;
	reg [6:0] slave_address_in;
	
	reg scl_in;
	reg sda_in;
	
	// Data from reg file //

	reg [7:0] read_data_in;
	reg read_data_done;
	
	
	
	assign sda = sda_in & sda_out;
	assign scl = scl_out &scl_in;
	
	reg [7:0] s8b_data_in;
	reg ra_enable;
	reg ra_done;
	reg [5:0] state ;
	reg r8b_done;
	reg r8b_enable;
	reg send_stop_enable; 
	reg s8b_enable;
	reg send_ack_enable;
	reg send_nack_enable;
	reg send_start_enable;
	reg scl_enable;
	
	
	
	wire [7:0] reg_address;
	wire [7:0] write_data_out;
	wire [7:0] read_data_out;
	
i2c_slave_mgr_sm slave(
	.clk(clk),
	.enable(enable),
	.slave_address_in(slave_address_in),
	
	.scl_in(scl_in),
	.scl_out(scl_out),
	.sda_in(sda_in),
	.sda_out(sda_out),
	
	// Data from reg file //
	.reg_address(reg_address),
	.write_data_out(write_data_out),
	.read_data_in(read_data_in),
	.read_data_done(read_data_done),
	.save_data(save_data),
	.load_data(load_data)
);
s8b_sm s8b(
	.clk(clk),
	
	.enable(s8b_enable),
	.done(s8b_done),
	.scl(scl_in),
	.sda(s8b_sda),
	.data_in(s8b_data_in)
);
send_ack_sm send_ack(
	.clk(clk),
	.enable(send_ack_enable),
	
	.sda(send_ack_sda),
	.scl(scl_in),
	
	.done(send_ack_done)
);
send_nack_sm send_nack(
	.clk(clk),
	.enable(send_nack_enable),
	
	.sda(send_nack_sda),
	.scl(scl_in),
	
	.done(send_nack_done)
);
send_start_sm send_start (
	.clk(clk),
	.enable(send_start_enable),
	
	.done(send_start_done),
	.sda(send_start_sda),
	.scl(scl_in)
);
send_stop_sm send_stop(
	.clk(clk),
	.enable(send_stop_enable),
	.done(send_stop_done),
	.sda(send_stop_sda),
	.scl(scl_in)
);

always @(*) begin
	sda_in = 1;

	if(s8b_enable)
		sda_in = s8b_sda;
	else if(send_ack_enable)
		sda_in = s8b_sda;
	else if(send_nack_enable)
		sda_in = send_nack_sda;
	else if(send_start_enable)
		sda_in = send_start_sda;
	else if(send_stop_enable)
		sda_in = send_stop_sda;
	else
		sda_in = 1;	

end
initial begin
	r8b_count = 0;
	read_data_done = 1;
	read_data_in = 133;
	slave_address_in = 127;
	state = 0;
	clk = 0;
	scl_in = 1;
	enable = 0;
	s8b_enable = 0;
	send_ack_enable = 0;
	send_nack_enable =0;
	send_start_enable =0 ;
	send_stop_enable = 0 ;
	#200 enable = 1;
	#20 send_start_enable = 1;
	#20 scl_enable = 1;
end

initial begin
	forever #1 clk = ~clk;
end

initial begin
	forever begin
		if(scl_enable)
			#200 scl_in =~scl_in;
		else 
			#200 scl_in = scl;
	end
end


always @(*) begin
	if(send_start_done && state == 0) begin
		send_start_enable = 0;
		s8b_data_in = 8'hFE;
		s8b_enable = 1;
	end
	if(s8b_done) begin
		s8b_enable = 0;
		ra_enable = 1;
		ra_done = 0;
	end
	if(ra_done && state == 0) begin
		ra_enable = 0;
		ra_done  = 0;
		s8b_data_in = 8'h1E;
		s8b_enable = 1;
		state = 1;
	end
	if(ra_done && state == 1) begin
		ra_enable = 0;
		ra_done = 0;
		send_start_enable = 1;
	end
	if(send_start_done && state == 1) begin
		send_start_enable = 0;
		s8b_data_in = 8'hFF;
		s8b_enable = 1;
		state = 2;
	end
	if(s8b_done && state == 2) begin
		ra_enable = 1;
		ra_done = 0;
		s8b_enable = 0;
	end
	if(ra_done && state ==2) begin
		ra_enable =0;
		ra_done = 0;
		r8b_enable = 1;
		r8b_done = 0;
	end
	if(r8b_done) begin
		r8b_done = 0;
		ra_enable = 1;
		ra_done = 0;
		state =3;
	end
//Sending Wanting to read 3 messages
	if(ra_done && state ==3 ) begin
		ra_done = 0;
		ra_enable = 0;
		send_ack_enable = 1;
	end
	if(send_ack_done && state == 3) begin
		r8b_enable = 1;
		r8b_done = 0 ;
		send_ack_enable = 0;
	end
	if(r8b_done && state == 3) begin
		r8b_enable = 0;
		r8b_done =0;
		send_ack_enable =1;
		state = 4;
	end
	if(send_ack_done && state == 4) begin
		r8b_enable =1;
		r8b_done = 0;
		send_ack_enable = 0;
	end
	if(r8b_done && state == 4) begin
		r8b_enable = 0;
		r8b_done = 0;
		send_nack_enable = 1;
	end
	if(send_nack_done && state == 4) begin
		send_nack_enable = 0;
		send_stop_enable = 1;
	end
end
always @(negedge scl_in) begin
	if(ra_enable) begin
		ra_done <= 1;
		ra_enable <= 0;
	end
	if(r8b_enable) begin
		r8b_count = r8b_count +1;
		if(r8b_count == 7) begin
			r8b_done <= 1;
			r8b_enable <= 0;
		end

	end
end
reg [2:0] r8b_count;
endmodule
