`include "i2c_wmbr_sm.v"
`include "../RA_SM/i2c_ra_sm.v"
`include "../S8B_SM/i2c_s8b_sm.v"
`include "../SEND_STOP_SM/i2c_send_stop_sm.v"
`include "../SEND_START_SM/i2c_send_start_sm.v"
module testbench;
	reg start_scl;
	
	reg clk;
	reg wmbr_enable;
	wire error;
	wire done;
	
	reg [6:0] slave_address;
	reg [7:0] reg_address;
	reg [7:0] write_data_low;
	reg [7:0] write_data_high;

	wire s8b_enable;	
	wire [7:0] s8b_data_in;
	wire s8b_done;
	wire s8b_error;
	
	wire send_start_enable;
	wire send_start_done;
	
	wire ra_enable;
	wire ra_nack_received;
	wire ra_ack_received;
	
	wire send_stop_enable;
	wire send_stop_done;
	
	reg sda;
	reg scl;
	
assign ra_sda = 0;

wmbr_sm wmbr(
	.clk(clk),
	.enable(wmbr_enable),
	.error(wmbr_error),
	.done(wmbr_done),
	
	.slave_address(slave_address),
	.reg_address(reg_address),
	.write_data_low(write_data_low),
	.write_data_high(write_data_high),

	.s8b_enable(s8b_enable),	
	.s8b_data_in(s8b_data_in),
	.s8b_done(s8b_done),
	.s8b_error(s8b_error),
	
	.send_start_enable(send_start_enable),
	.send_start_done(send_start_done),
	
	.ra_enable(ra_enable),
	.ra_nack_received(ra_nack_received),
	.ra_ack_received(ra_ack_received),
	
	.send_stop_enable(send_stop_enable),
	.send_stop_done(send_stop_done)
);

ra_sm ra(
	.clk(clk),
	.enable(ra_enable),

	.sda(ra_sda),
	.scl(scl),

	.nack_received(ra_nack_received),
	.ack_received(ra_ack_received)
);

s8b_sm s8b(
	.clk(clk),

	.enable(s8b_enable),
	.error(s8b_error),
	.done(s8b_done),

	.scl(scl),
	.sda(s8b_sda),

	.data_in(s8b_data_in)
	
);

send_stop_sm send_stop(
	.clk(clk),
	
	.enable(send_stop_enable),
	.done(send_stop_done),
	
	.sda(send_stop_sda),
	.scl(scl)
);

send_start_sm send_start(
	.clk(clk),

	.enable(send_start_enable),
	.done(send_start_done),

	.sda(send_start_sda),
	.scl(scl)
	
);
always @(*) begin
	//if(ra_enable)
		//sda = ra_sda;
	if(s8b_enable)
		sda = s8b_sda;
	else if(send_stop_enable)
		sda = send_stop_sda;
	else if(send_start_enable)
		sda = send_start_sda;
	else
		sda = 1;
end

initial begin
	clk = 0;
	scl = 1;
	wmbr_enable = 0;
	start_scl = 0;
	slave_address = 15;
	reg_address = 8'hCA;
	write_data_low = 8'hFE;
	write_data_high = 8'hBA;
	#20 wmbr_enable = 1;
end

initial begin
	forever #1 clk = ~clk;
end

initial begin
	forever begin
		if(start_scl)
			#200 scl = ~scl;
		else
			#200 scl = scl;
	end
end

always @(*) begin
	if(send_start_enable)
		#20 start_scl = 1;
	
end

endmodule



