module wmbr_sm (
	input clk,
	input enable,
	output error,
	output done,
	
	input [6:0] slave_address,
	input [7:0] reg_address,
	input [7:0] write_data_low,
	input [7:0] write_data_high,

	output s8b_enable,	
	output [7:0] s8b_data_in,
	input s8b_done,
	input s8b_error,
	
	output send_start_enable,
	input send_start_done,
	
	output ra_enable,
	input ra_nack_received,
	input ra_ack_received,
	
	output send_stop_enable,
	input send_stop_done
);

/* State REGS */
reg [12:0] state;
reg [12:0] next_state;

/* One Hot Encoding */ 

parameter IDLE =                 13'b0000000000001;
parameter SEND_START =           13'b0000000000010;
parameter SEND_ADDRESS =         13'b0000000000100; 
parameter RECEIVE_ACK_1 =        13'b0000000001000;
parameter SEND_REG_ADDRESS =     13'b0000000010000;
parameter RECEIVE_ACK_2 =        13'b0000000100000;
parameter SEND_WRITE_DATA_LOW =  13'b0000001000000;
parameter RECEIVE_ACK_3 =        13'b0000010000000;
parameter SEND_WRITE_DATA_HIGH = 13'b0000100000000;
parameter RECEIVE_ACK_4 =        13'b0001000000000;
parameter SEND_STOP =            13'b0010000000000;
parameter STOP =                 13'b0100000000000;
parameter TRANSACTION_ERROR =    13'b1000000000000;

/* Next Values */

reg [6:0] next_slave_address;
reg [7:0] next_reg_address;
reg [7:0] next_write_data_low;
reg [7:0] next_write_data_high;

reg next_send_start_enable;
reg next_s8b_enable;
reg next_ra_enable;
reg next_send_stop_enable;

reg [7:0] next_s8b_data_in;

reg next_error;
reg next_done;

/* Current Values */

reg [6:0] current_slave_address;
reg [7:0] current_reg_address;
reg [7:0] current_write_data_low;
reg [7:0] current_write_data_high;

reg current_send_start_enable;
reg current_s8b_enable;
reg current_ra_enable;
reg current_send_stop_enable;

reg [7:0] current_s8b_data_in;

reg current_error;
reg current_done;

/*Wires for interconection modules */ 
wire send_start_done;

wire s8b_error;
wire s8b_done;

wire ra_nack_received;
wire ra_ack_received;

wire send_stop_done;
/* Output assigns */
assign error = current_error;
assign done = current_done;

assign s8b_enable = current_s8b_enable;
assign s8b_data_in = current_s8b_data_in;

assign send_start_enable = current_send_start_enable;

assign ra_enable = current_ra_enable;

assign send_stop_enable = current_send_stop_enable;
/* Next State Logic */ 

always @(*) begin
	case(state)
	IDLE: begin
		if(enable)
			next_state = SEND_START;
		else
			next_state = IDLE;
	end
	SEND_START: begin
		if(send_start_done) 
			next_state = SEND_ADDRESS;
		else
			next_state = SEND_START;	
	end

	SEND_ADDRESS: begin
		if(s8b_error)
			next_state = TRANSACTION_ERROR;
		else if(s8b_done)
			next_state = RECEIVE_ACK_1;
		else
			next_state = SEND_ADDRESS;
	end
	RECEIVE_ACK_1: begin
		if(ra_nack_received)
			next_state = TRANSACTION_ERROR;
		else if(ra_ack_received)
			next_state = SEND_REG_ADDRESS;	
		else
			next_state = RECEIVE_ACK_1;
		
	end
	SEND_REG_ADDRESS: begin
		if(s8b_error)
			next_state = TRANSACTION_ERROR;
		else if(s8b_done)
			next_state = RECEIVE_ACK_2;
		else
			next_state = SEND_REG_ADDRESS;
	end
	RECEIVE_ACK_2: begin
		if(ra_nack_received)
			next_state = TRANSACTION_ERROR;
		else if(ra_ack_received)
			next_state = SEND_WRITE_DATA_LOW;
		else 
			next_state = RECEIVE_ACK_2;

	end
	
	SEND_WRITE_DATA_LOW: begin
		if(s8b_error)
			next_state = TRANSACTION_ERROR;
		else if (s8b_done)
			next_state = RECEIVE_ACK_3;
		else 
			next_state = SEND_WRITE_DATA_LOW; 
	end

	RECEIVE_ACK_3: begin
		if(ra_nack_received)
			next_state = TRANSACTION_ERROR;
		else if( ra_ack_received)
			next_state = SEND_WRITE_DATA_HIGH;
		else
			next_state = RECEIVE_ACK_3;
	end
	
	SEND_WRITE_DATA_HIGH: begin
		if(s8b_error)
			next_state = TRANSACTION_ERROR;
		else if (s8b_done)
			next_state = RECEIVE_ACK_4;
		else
			next_state = SEND_WRITE_DATA_HIGH;
	end
	RECEIVE_ACK_4: begin
		if(ra_nack_received)
			next_state = TRANSACTION_ERROR;
		else if (ra_ack_received)
			next_state = SEND_STOP;
		else
			next_state = RECEIVE_ACK_4;
	end

	SEND_STOP: begin
		if(send_stop_done)
			next_state = STOP;
		else
			next_state = SEND_STOP;
	end
	STOP: next_state = STOP;
	
	TRANSACTION_ERROR: next_state = STOP;
	
	default: next_state = TRANSACTION_ERROR;
	endcase	

end

/* Next Output Logic */

always @(*) begin
	/* Preventing Infered Latches */ 

	next_slave_address = current_slave_address;
	next_reg_address = current_reg_address;
	next_write_data_low = current_write_data_low;
	next_write_data_high = current_write_data_high;

	next_send_start_enable = current_send_start_enable;
	next_s8b_enable = current_s8b_enable;
	next_ra_enable = current_ra_enable;
	next_send_stop_enable = current_send_stop_enable;

	next_s8b_data_in = current_send_stop_enable;
	
	next_error = current_error;
	next_done = current_done;

	case(state)
	IDLE: begin
		if(enable) begin
			next_slave_address = slave_address;
			next_reg_address = reg_address;
			next_write_data_low = write_data_low;
			next_write_data_high = write_data_high;
		end
		else begin //reset all variables
			next_send_start_enable = 0;
			next_s8b_enable = 0;
			next_ra_enable = 0;
			next_send_stop_enable =0;
			next_error = 0;
		end	
	end
	SEND_START: begin
		next_send_start_enable = 1;
		next_s8b_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable =0;
	end
	SEND_ADDRESS: begin
		next_error = s8b_error;
		next_s8b_data_in [7:1] = current_slave_address;
		next_s8b_data_in [0] = 0; //WRITE


		next_send_start_enable = 0;
		next_s8b_enable = 1;
		next_ra_enable = 0;
		next_send_stop_enable =0;
	end
	RECEIVE_ACK_1: begin
		next_error = ra_nack_received;

		next_send_start_enable = 0;
		next_s8b_enable = 0;	
		next_ra_enable = 1;
		next_send_stop_enable = 0;
	end
	
	SEND_REG_ADDRESS: begin
		next_error = s8b_error;
		next_s8b_data_in = current_reg_address;
		
		next_send_start_enable =0;
		next_s8b_enable = 1;
		next_ra_enable =0;
		next_send_stop_enable = 0;
	end
	RECEIVE_ACK_2: begin
		next_error = ra_nack_received;
		
		next_send_start_enable =0;
		next_s8b_enable =0;
		next_ra_enable =1;
		next_send_stop_enable = 0;
	end

	SEND_WRITE_DATA_LOW: begin

		next_error = s8b_error;
		next_s8b_data_in = current_write_data_low;
	
		next_send_start_enable = 0;
		next_s8b_enable = 1;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		

	end
	RECEIVE_ACK_3: begin

		next_error = ra_nack_received;
		
		next_send_start_enable = 0;
		next_s8b_enable = 0;
		next_ra_enable =1;
		next_send_stop_enable = 0;
	
	end
	SEND_WRITE_DATA_HIGH: begin
		
		next_error = s8b_error;
		next_s8b_data_in = current_write_data_high;
	
		next_send_start_enable = 0;
		next_s8b_enable = 1;
		next_ra_enable = 0;
		next_send_stop_enable = 0; 
		
	end

	RECEIVE_ACK_4:begin
		next_error = ra_nack_received;
		
		next_send_start_enable = 0;
		next_s8b_enable = 0;
		next_ra_enable = 1; 
		next_send_stop_enable = 0;
	end
	

	SEND_STOP: begin
		if(send_stop_done)
			next_done = 1;
		else 
			next_done = 0;
	
		next_send_start_enable = 0;
		next_s8b_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 1;
	end

	TRANSACTION_ERROR:begin //resetear todas las variables
		next_send_start_enable = 0;
		next_s8b_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable =0;
		next_error = 1;	
		next_done = 0;			
	end

	STOP: begin
		next_send_start_enable = 0;
		next_s8b_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable =0;
		next_error = 0;	
		next_done = 0;	
	end

	default: begin //resetear todas las variables
		next_send_start_enable = 0;
		next_s8b_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable =0;
		next_error = 1;	
		next_done = 0;		
	end
		
	endcase
end

/* Sincronic Logic */


always @(posedge clk) begin
	if(~enable)
		state <= IDLE;
	else
		state <= next_state;

	current_slave_address <= next_slave_address;
	current_reg_address <= next_reg_address;
	current_write_data_low <= next_write_data_low;
	current_write_data_high <= next_write_data_high;

	current_send_start_enable <= next_send_start_enable;
	current_s8b_enable <= next_s8b_enable;
	current_ra_enable <= next_ra_enable;
	current_send_stop_enable <= next_send_stop_enable;

	current_s8b_data_in <= next_s8b_data_in;
	
	current_error <= next_error;
	current_done <= next_done;

end 

initial begin
	$dumpfile("wmbr_sim.vcd"); //non synth code, simulation only
	$dumpvars;
end
endmodule
