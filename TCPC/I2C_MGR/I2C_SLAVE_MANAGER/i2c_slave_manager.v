//`include "../DETECT_START_SM/detect_start_sm.v"
//`include "../DETECT_STOP_SM/detect_stop_sm.v"
//`include "../R8B_SM/r8b_sm.v"
//`include "../S8B_SM/i2c_s8b_sm.v"
//`include "../SEND_ACK_SM/i2c_send_ack_sm.v"
//`include "../SEND_NACK_SM/i2c_send_nack_sm.v"
//`include "../RA_SM/i2c_ra_sm.v"

module i2c_slave_mgr_sm(
	input clk,
	input enable,
	input [6:0] slave_address_in,
	
	input scl_in,
	output scl_out,
	input sda_in,
	output sda_out,
	
	// Data from reg file //
	output [7:0] reg_address,
	output [7:0] write_data_out,
	input [7:0] read_data_in,
	input read_data_done,
	output save_data,
	output load_data
);
/*State REGs*/
reg [10:0] state;
reg [10:0] next_state;

/* One Hot Encoding */
parameter IDLE =                                       11'b00000000001;
parameter WAIT_START =                           11'b00000000010;
parameter RECEIVE_SLAVE_ADDRESS =  11'b00000000100;
parameter SEND_ACK_1 =                         11'b00000001000;
parameter RECEIVE_REG_ADDRESS =      11'b00000010000;
parameter SEND_ACK_2 =                         11'b00000100000;
parameter SEND_REGISTER =                   11'b00001000000;
parameter WAIT_ACK =                              11'b00010000000;
parameter READ_DATA =                            11'b00100000000;
parameter SEND_ACK_N =                          11'b01000000000;
parameter WAIT_STOP =                             11'b10000000000;

/* Next Values */
reg next_action;
reg next_load_data;
reg next_r8b_enable;
reg next_ra_enable;
reg [7:0] next_reg_address;
reg [7:0] next_s8b_data_in;
reg next_s8b_enable;
reg next_save_data;
reg next_scl_out;
reg next_sda_out;
reg next_send_ack_enable;
reg [7:0] next_write_data_out;

/* Current Values */
reg current_action;
reg current_load_data;
reg current_r8b_enable;
reg current_ra_enable;
reg [7:0] current_reg_address;
reg [7:0] current_s8b_data_in;
reg current_s8b_enable;
reg current_save_data;
reg current_scl_out;
reg current_sda_out;
reg current_send_ack_enable;
reg [7:0] current_write_data_out;

/* Wires for interconecting modules */
wire start_detected;
wire r8b_enable;
wire send_ack_done;
wire r8b_error;
wire stop_detected;
wire s8b_done;
wire ra_nack_received;
wire send_nack_done;
wire r8b_done;
wire ra_ack_received;

wire [7:0] s8b_data_in;
wire [7:0] r8b_data_out;
wire [7:0] read_data_out;
/* assigns */ 
assign detect_start_enable = 1;
assign detect_stop_enable = 1;

/* Output Assigns */
assign save_data = current_save_data;
assign load_data = current_load_data;
assign reg_address = current_reg_address;
assign write_data_out = current_write_data_out;
assign action = current_action;
assign sda_out = current_sda_out;
assign scl_out = current_scl_out;
assign r8b_enable =  current_r8b_enable;
assign send_ack_enable = current_send_ack_enable;
assign ra_enable = current_ra_enable;
assign s8b_enable = current_s8b_enable;
assign s8b_data_in = current_s8b_data_in;



/* Modules declaration*/
detect_start_sm detect_start(
	.clk(clk),
	.enable(detect_start_enable),
	
	.sda(sda_in),
	.scl(scl_in),
	.start_detected(start_detected)
);
detect_stop_sm detect_stop(
	.clk(clk),
	.enable(detect_stop_enable),
	
	.sda(sda_in),
	.scl(scl_in),
	.stop_detected(stop_detected)
);
r8b_sm r8b(
	.clk(clk),
	.enable(r8b_enable),
	
	.scl(scl_in),
	.sda(sda_in),
	
	.done(r8b_done),
	.error(r8b_error),
	
	.data_out(r8b_data_out)
);
s8b_sm s8b(
	.clk(clk),
	
	.enable(s8b_enable),
	.error(s8b_error),
	.done(s8b_done),
	.scl(scl_in),
	.sda(s8b_sda),
	.data_in(s8b_data_in)
);
send_ack_sm send_ack(
	.clk(clk),
	.enable(send_ack_enable),
	
	.sda(send_ack_sda),
	.scl(scl_in),
	
	.done(send_ack_done)
);
send_nack_sm send_nack(
	.clk(clk),
	.enable(send_nack_enable),
	
	.sda(send_nack_sda),
	.scl(scl_in),
	
	.done(send_nack_done)
);
ra_sm ra(
	.clk(clk),
	.enable(ra_enable),
	
	.sda(sda_in),
	.scl(scl_in),
	
	.nack_received(ra_nack_received),
	.ack_received(ra_ack_received)
);
/*Next state logic */
always @(*) begin
	next_state = IDLE;
	case(state)
	IDLE: begin
		if(enable)
			next_state = WAIT_START;
		else
			next_state = IDLE;
	end
	WAIT_START: begin
		if(start_detected)
			next_state = RECEIVE_SLAVE_ADDRESS;
		else
			next_state = WAIT_START;
	end
	RECEIVE_SLAVE_ADDRESS: begin
		if(r8b_error)
			next_state = WAIT_START;
		else if(r8b_done) begin// is it my address?
			if(r8b_data_out[7:1] == slave_address_in) 
				next_state = SEND_ACK_1;
			else
				next_state = WAIT_START;
		end
		else
			next_state = RECEIVE_SLAVE_ADDRESS;
	end
	SEND_ACK_1: begin
		if(send_ack_done) begin
			if(action == 0)
				next_state = RECEIVE_REG_ADDRESS;
			else if(action == 1)
				next_state = SEND_REGISTER;
		end
		else
			next_state = SEND_ACK_1;
	end
	RECEIVE_REG_ADDRESS: begin
		if(r8b_error)
			next_state = WAIT_START;
		else if (r8b_done)
			next_state = SEND_ACK_2;
		else
			next_state = RECEIVE_REG_ADDRESS;
	end
	SEND_ACK_2: begin
		if(send_ack_done)  begin
			if(action == 1 ) //READ
				next_state = SEND_REGISTER;
			else if(action == 0) //WRITE
				next_state = READ_DATA;
		end
		else	
			next_state = SEND_ACK_2;
	end
	SEND_REGISTER: begin
		if(s8b_done)
			next_state = WAIT_ACK;
		else
			next_state = SEND_REGISTER;
	end
	WAIT_ACK: begin
		if(ra_ack_received)
			next_state = SEND_REGISTER;
		else if(ra_nack_received)
			next_state  = WAIT_STOP;
		else
			next_state =  WAIT_ACK;
	end
	READ_DATA: begin
		if(r8b_error) 
			next_state = WAIT_START;
		if(r8b_done)
			next_state  = SEND_ACK_N;
		else
			next_state = READ_DATA;
	end
	SEND_ACK_N: begin
		if(send_ack_done) 
			next_state = READ_DATA;
		else
			next_state = SEND_ACK_N;
	end
	WAIT_STOP: next_state =  WAIT_STOP;
	endcase
	if(start_detected)
		next_state = RECEIVE_SLAVE_ADDRESS;
	if(stop_detected)
		next_state = WAIT_START;
end
/* Next Output Logic */ 
always @(*) begin
	next_scl_out = 1;
	next_sda_out = 1;
	next_reg_address = current_reg_address;
	next_write_data_out = r8b_data_out;
	next_save_data = 0;
	next_load_data = 0;
	//Next Enables // 
	next_r8b_enable = 0;
	next_send_ack_enable = 0;
	next_s8b_enable = 0;
	next_ra_enable = 0;
	next_action = current_action;
	next_s8b_data_in = 7'dx;
	case(state) 
	RECEIVE_SLAVE_ADDRESS: begin
		next_r8b_enable = 1;
		if(r8b_done)  begin
			if(r8b_data_out[7:1] == slave_address_in)
				next_action = r8b_data_out[0];
		end
	end
	SEND_ACK_1: begin
		next_send_ack_enable = 1;
		next_sda_out = send_ack_sda;
	end
	RECEIVE_REG_ADDRESS: begin
		next_r8b_enable = 1;
		if(r8b_done)
			next_reg_address = r8b_data_out;
	end
	SEND_ACK_2: begin
		next_send_ack_enable = 1;
		next_sda_out = send_ack_sda;
	end
	SEND_REGISTER: begin
		next_load_data = 1;
		next_scl_out = 0;
		if(read_data_done || s8b_enable) begin
			next_s8b_data_in = read_data_in;
			next_s8b_enable = 1;
			next_scl_out = 1;
			next_sda_out = s8b_sda;
			if(s8b_done)
				next_reg_address = current_reg_address +1;
		end
	end
	WAIT_ACK: next_ra_enable = 1;
	
	READ_DATA: begin
		next_r8b_enable =1 ;
		if(r8b_error)
			next_r8b_enable = 0;
		if(r8b_done) begin
			next_save_data = 1;
		end			
	end
	SEND_ACK_N:begin
		next_send_ack_enable =1; 
		next_sda_out = send_ack_sda;
		if(send_ack_done)
			next_reg_address = current_reg_address +1;
	end
	endcase
end
/* Clk logic */
always @(posedge clk) begin
	if(~enable)
		state <= IDLE;
	else
		state <= next_state;
	
	current_action<= next_action ;
	current_load_data<= next_load_data;
	current_r8b_enable<= next_r8b_enable ;
	current_ra_enable<= next_ra_enable ;
	current_reg_address<= next_reg_address ;
	current_s8b_data_in<= next_s8b_data_in ;
	current_s8b_enable<= next_s8b_enable ;
	current_save_data<= next_save_data ;
	current_scl_out<= next_scl_out ;
	current_sda_out<= next_sda_out ;
	current_send_ack_enable<= next_send_ack_enable ;
	current_write_data_out<= next_write_data_out ;
	
end
initial begin
	$dumpfile("slave_mgr.vcd"); //non synth code, simulation only
	$dumpvars;
end
endmodule 
