`include "DETECT_START_SM/detect_start_sm.v"
`include "DETECT_STOP_SM/detect_stop_sm.v"
`include "R8B_SM/r8b_sm.v"
`include "S8B_SM/i2c_s8b_sm.v"
`include "SEND_ACK_SM/i2c_send_ack_sm.v"
`include "SEND_NACK_SM/i2c_send_nack_sm.v"
`include "RA_SM/i2c_ra_sm.v"

module i2c_slave_mgr_sm(
	input clk,
	input enable,
	input [6:0] slave_address_in,
	
	input scl_in,
	output scl_out,
	input sda_in,
	output sda_out,
	
	// Data from reg file //
	output [7:0] reg_address,
	output [7:0] write_data_out,
	input [7:0] read_data_in,
	input read_data_done,
	output save_data,
	output load_data
);
/*State REGs*/
reg [16:0] state;
reg [16:0] next_state;

/* One Hot Encoding*/
parameter IDLE =                    17'b00000000000000001;
parameter WAIT_START_1 =            17'b00000000000000010;
parameter RECEIVE_SLAVE_ADDRESS_1 = 17'b00000000000000100;
parameter DECODE_SLAVE_ADDRESS_1 =  17'b00000000000001000; 
parameter SEND_ACK_1 =              17'b00000000000010000;
parameter RECEIVE_REG_ADDRESS =     17'b00000000000100000;
parameter SEND_ACK_2 =              17'b00000000001000000;
parameter RECEIVE_WRITE_DATA =      17'b00000000010000000;
parameter SEND_ACK_N =              17'b00000000100000000;
parameter WAIT_STOP =               17'b00000001000000000;
parameter WAIT_START_2 =            17'b00000010000000000;
parameter RECEIVE_SLAVE_ADDRESS_2 = 17'b00000100000000000;
parameter DECODE_SLAVE_ADDRESS_2 =  17'b00001000000000000;
parameter SEND_ACK_3 =              17'b00010000000000000;
parameter SEND_READ_DATA =          17'b00100000000000000;
parameter RECEIVE_ACK_N =           17'b01000000000000000;
parameter SEND_NACK =               17'b10000000000000000;


/* Next Values */
reg [7:0] next_slave_address;
reg [7:0] next_write_data_out;
reg next_detect_start_enable;
reg next_r8b_enable;
reg next_send_ack_enable;
reg next_send_nack_enable;
reg next_save_data;
reg next_load_data;
reg next_scl_out;
reg next_detect_stop_enable;
reg [7:0] next_reg_address;
reg [7:0] next_read_data_out;
reg next_s8b_enable;
reg next_ra_enable;
reg next_sda_out;

/* Current Values */
reg [7:0] current_slave_address;
reg [7:0] current_write_data_out;
reg current_detect_start_enable;
reg current_r8b_enable;
reg current_send_ack_enable;
reg current_send_nack_enable;
reg current_save_data;
reg current_load_data;
reg current_scl_out;
reg current_detect_stop_enable;
reg [7:0] current_reg_address;
reg [7:0] current_read_data_out;
reg current_s8b_enable;
reg current_ra_enable;
reg current_sda_out;

/* Assigns */
/* Output Assigns*/
assign scl_out = current_scl_out;
assign sda_out = current_sda_out;

// Data from reg file //
assign reg_address = current_reg_address;
assign write_data_out = current_write_data_out;
assign save_data = current_save_data;
assign load_data = current_load_data;
assign read_data_out = current_read_data_out;

assign detect_start_enable = current_detect_start_enable;
assign detect_stop_enable = current_detect_stop_enable;
assign r8b_enable =  current_r8b_enable;
assign s8b_enable = current_s8b_enable;
assign send_nack_enable = current_send_nack_enable;
assign ra_enable = current_ra_enable;
assign send_ack_enable = current_send_ack_enable;




/* Wires for interconecting modules */
wire start_detected;
wire r8b_enable;
wire send_ack_done;
wire r8b_error;
wire stop_detected;
wire s8b_done;
wire ra_nack_received;
wire send_nack_done;
wire r8b_done;
wire ra_ack_received;

wire [7:0] r8b_data_out;
wire [7:0] read_data_out;
/* Modules declaration*/
detect_start_sm detect_start(
	.clk(clk),
	.enable(detect_start_enable),
	
	.sda(sda_in),
	.scl(scl_in),
	.start_detected(start_detected)
);
detect_stop_sm detect_stop(
	.clk(clk),
	.enable(detect_stop_enable),
	
	.sda(sda_in),
	.scl(scl_in),
	.stop_detected(stop_detected)
);
r8b_sm r8b(
	.clk(clk),
	.enable(r8b_enable),
	
	.scl(scl_in),
	.sda(sda_in),
	
	.done(r8b_done),
	.error(r8b_error),
	
	.data_out(r8b_data_out)
);
s8b_sm s8b(
	.clk(clk),
	
	.enable(s8b_enable),
	.error(s8b_error),
	.done(s8b_done),
	.scl(scl_in),
	.sda(s8b_sda),
	.data_in(read_data_out)
);
send_ack_sm send_ack(
	.clk(clk),
	.enable(send_ack_enable),
	
	.sda(send_ack_sda),
	.scl(scl_in),
	
	.done(send_ack_done)
);
send_nack_sm send_nack(
	.clk(clk),
	.enable(send_nack_enable),
	
	.sda(send_nack_sda),
	.scl(scl_in),
	
	.done(send_nack_done)
);
ra_sm ra(
	.clk(clk),
	.enable(ra_enable),
	
	.sda(sda_in),
	.scl(scl_in),
	
	.nack_received(ra_nack_received),
	.ack_received(ra_ack_received)
);
/* SDA LOGIC */

/*Next State Logic */ 
always @(*) begin
	next_state = IDLE;
	case(state)
	IDLE: begin
		if(enable)
			next_state = WAIT_START_1;
		else
			next_state = IDLE;
	end
	WAIT_START_1: begin
		if(start_detected)
			next_state = RECEIVE_SLAVE_ADDRESS_1;
		else
			next_state = WAIT_START_1;
	end
	RECEIVE_SLAVE_ADDRESS_1: begin
		if(r8b_error)
			next_state = SEND_NACK;
		else if(r8b_done)
			next_state = DECODE_SLAVE_ADDRESS_1;
		else
			next_state = RECEIVE_SLAVE_ADDRESS_1;
	end
	DECODE_SLAVE_ADDRESS_1: begin
		if(current_slave_address[7:1] == slave_address_in)
			next_state = SEND_ACK_1;
		else
			next_state = WAIT_START_1;	
	end
	SEND_ACK_1: begin
		if(send_ack_done)
			next_state = RECEIVE_REG_ADDRESS;
		else
			next_state = SEND_ACK_1;
	end
	RECEIVE_REG_ADDRESS: begin
		if(r8b_error)
			next_state = SEND_NACK;
		else if (r8b_done)
			next_state = SEND_ACK_2;
		else
			next_state = RECEIVE_REG_ADDRESS;
	end
	SEND_ACK_2: begin
		if(send_ack_done) begin
			if(current_slave_address[0] == 1)
				next_state = RECEIVE_WRITE_DATA;
			else
				next_state = WAIT_START_2;
		end
		else
			next_state = SEND_ACK_2;
	end
	RECEIVE_WRITE_DATA: begin
		if(start_detected)
			next_state = RECEIVE_SLAVE_ADDRESS_2;
		else if(stop_detected)
			next_state = WAIT_START_1;
		else if(r8b_error)
			next_state = SEND_NACK;
		else if(r8b_done)
			next_state = SEND_ACK_N;
		else
			next_state = RECEIVE_WRITE_DATA;
	end
	SEND_ACK_N: begin
		if(send_ack_done) begin
				next_state = RECEIVE_WRITE_DATA;
		end
		else
			next_state = SEND_ACK_N;
	end
	WAIT_STOP: begin
		if(stop_detected)
			next_state = WAIT_START_1;
		else
			next_state = WAIT_STOP;
	end
	WAIT_START_2: begin
		if(start_detected)
			next_state = RECEIVE_SLAVE_ADDRESS_2;
		else
			next_state = WAIT_START_2;
	end
	RECEIVE_SLAVE_ADDRESS_2: begin
		if(r8b_error)
			next_state = SEND_NACK;
		else if(r8b_done)
			next_state = DECODE_SLAVE_ADDRESS_2;
		else
			next_state = RECEIVE_SLAVE_ADDRESS_2;	
	end
	DECODE_SLAVE_ADDRESS_2: begin
		if(current_slave_address[7:1] == slave_address_in) begin
			if(current_slave_address[0] == 0)
				next_state = SEND_ACK_1;
			else
				next_state = SEND_ACK_3;
		end
		else
			next_state = WAIT_START_2; //Tal vez otro estado	
	end
	SEND_ACK_3: begin
		if(send_ack_done)
			next_state = SEND_READ_DATA;
		else
			next_state = SEND_ACK_3;
	end
	SEND_READ_DATA: begin //que pasa con el S8b_error 
		if(s8b_done)
			next_state = RECEIVE_ACK_N;
		else
			next_state = SEND_READ_DATA;
	end
	RECEIVE_ACK_N: begin
		if(ra_nack_received)
			next_state = WAIT_STOP;
		else if(ra_ack_received)
			next_state = SEND_READ_DATA;
		else
			next_state = RECEIVE_ACK_N;
	end
	SEND_NACK: begin
		if(send_nack_done)
			next_state = WAIT_START_1;
		else
			next_state = SEND_NACK;
	end
	endcase
end
/* Next Output Logic*/
always@(*) begin
	next_detect_start_enable = 1;
	next_detect_stop_enable = 1;
	next_r8b_enable = 0;
	next_send_ack_enable =  0;
	next_save_data = 0;
	next_send_ack_enable = 0;
	next_send_nack_enable = 0;
	next_load_data = 0;
	next_send_ack_enable = 0;
	next_scl_out = 1;
	next_sda_out = 1;
	next_ra_enable = 0;
	
	next_write_data_out = current_write_data_out;
	next_reg_address = current_reg_address;
	next_slave_address = current_slave_address;
	next_read_data_out = current_read_data_out;
	
	case(state)
	//IDLE:
	//WAIT_START_1:
	RECEIVE_SLAVE_ADDRESS_1: begin
		next_r8b_enable = 1;
		if(r8b_done)
			next_slave_address = r8b_data_out;	
	end
	//DECODE_SLAVE_ADDRESS_1:
	SEND_ACK_1: begin
		next_send_ack_enable = 1;
		next_sda_out = send_ack_sda;
	end
	
	RECEIVE_REG_ADDRESS: begin
		next_r8b_enable = 1;
		next_sda_out = 1;
		if(r8b_done)
			next_reg_address = r8b_data_out;
	end
	SEND_ACK_2: begin
		next_send_ack_enable = 1;
		next_sda_out = send_ack_sda;
	end
	RECEIVE_WRITE_DATA: begin
		next_r8b_enable = 1;
		next_sda_out = 1;
		if(r8b_done) begin
			next_write_data_out = r8b_data_out;
			next_save_data = 1;
		end
	end
	SEND_ACK_N: begin
		next_send_ack_enable = 1;
		next_sda_out = send_ack_sda;
		if(send_ack_done)
			next_reg_address = current_reg_address + 1;
	end
	//WAIT_STOP:
	//WAIT_START_2:
	RECEIVE_SLAVE_ADDRESS_2: begin
		next_r8b_enable = 1;
		next_sda_out = 1;
		if(r8b_done)
			next_slave_address = r8b_data_out;
	end
	DECODE_SLAVE_ADDRESS_2: next_load_data = 1;
	SEND_ACK_3: begin
		next_load_data = 1;
		next_send_ack_enable = 1;
		next_sda_out = send_ack_sda;
		if(read_data_done)
			next_read_data_out = read_data_in;
		else if(send_ack_done && ~read_data_done)
			next_scl_out = 0;
	end
	SEND_READ_DATA: begin
	next_s8b_enable = 1;
	next_sda_out = s8b_sda;
	end
	RECEIVE_ACK_N: begin
		next_ra_enable = 1;
		next_sda_out = 1;
		if(ra_nack_received)
			next_reg_address = current_reg_address +1;
	end
	SEND_NACK: begin
	next_send_nack_enable = 1;
	next_sda_out = send_nack_sda;
	end
	endcase
end
/* Sinc logic */
always @(posedge clk) begin
	if(~enable)
		state <= IDLE;
	else
		state <= next_state;
		
	current_detect_start_enable <= next_detect_start_enable;
	current_detect_stop_enable <= next_detect_stop_enable;
	current_r8b_enable <= next_r8b_enable;
	current_send_ack_enable <=  next_send_ack_enable;
	current_save_data <= next_save_data;
	current_send_ack_enable <= next_send_ack_enable;
	current_send_nack_enable <= next_send_nack_enable;
	current_load_data <= next_load_data;
	current_scl_out <= next_scl_out;
	
	current_write_data_out <= next_write_data_out;
	current_reg_address <= next_reg_address;
	current_slave_address <= next_slave_address;
	current_read_data_out <= next_read_data_out;
	current_sda_out <= next_sda_out;
	current_s8b_enable <= next_s8b_enable;
	current_ra_enable <= next_ra_enable;
end
//Simulation Code, no synth code
initial begin
	$dumpfile("slave.vcd"); //non synth code, simulation only
	$dumpvars;
end

endmodule
