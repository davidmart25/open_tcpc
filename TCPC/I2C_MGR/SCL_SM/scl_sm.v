module scl_sm (
	input clk,
	input enable,
	input send_start_enable,
	input sda,
	input send_stop_done,
	input scl_clk,
	output scl
);

/* State REGs */
reg [1:0] state;
reg [1:0] next_state;

/* One Hot Encoding */
parameter WAIT_SSE_POSEDGE = 2'b01;
parameter WAIT_SSD_POSEDGE = 2'b10;

/* Next Values */ 
reg next_scl;

/* Current Values */
reg current_scl;

/* Delayed Values*/
reg send_stop_done_d;

/*Assigns */
assign send_start_edge = send_start_enable & ~sda;
assign posedge_send_stop_done = ~send_stop_done_d & send_stop_done;

/*Output Assigns */
assign scl = current_scl;

/* Next State Logic */
always@(*) begin
	case(state)
	WAIT_SSE_POSEDGE: begin
		if (send_start_edge)
			next_state = WAIT_SSD_POSEDGE;
		else
			next_state = WAIT_SSE_POSEDGE;
	end
	WAIT_SSD_POSEDGE: begin
		if(posedge_send_stop_done)
			next_state = WAIT_SSE_POSEDGE;
		else
			next_state = WAIT_SSD_POSEDGE;
	end
	
	default: next_state = WAIT_SSE_POSEDGE;
	endcase
end
/* Next Output Logic */
always@(*) begin
	case(state)
	WAIT_SSE_POSEDGE: next_scl = 1;
	WAIT_SSD_POSEDGE: next_scl = scl_clk;
	default: next_scl = 1;
	endcase
end

/* Sincroinic Logic */
always @(posedge clk) begin
	send_stop_done_d <= send_stop_done;
	if(~enable)
		state <= WAIT_SSE_POSEDGE;
	else
		state <= next_state;
		
	current_scl <= next_scl;
end

initial begin
	$dumpfile("scl_sim.vcd"); //non synth code, simulation only
	$dumpvars;
end

endmodule
