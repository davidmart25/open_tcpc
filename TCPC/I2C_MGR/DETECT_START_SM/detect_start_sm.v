module detect_start_sm(
	input clk,
	input enable,
	
	input sda,
	input scl,
	output start_detected
);

/*State REGs*/
reg [3:0] state;
reg [3:0] next_state;

/* One Hot Encoding*/
parameter IDLE =              4'b0001;
parameter ANY_SCL_SDA_COMB =  4'b0010;
parameter SCL_HIGH_SDA_HIGH = 4'b0100;
parameter SCL_HIGH_SDA_LOW =  4'b1000;

/* Next Values */
reg next_start_detected;

/* Current Values */
reg current_start_detected;

/* Assigns */
/* Output Assigns*/
assign start_detected =current_start_detected;

/* Next State Logic */
always @(*) begin
	case(state)
	IDLE: begin
		if(enable) begin
			if(sda && scl)
				next_state = SCL_HIGH_SDA_HIGH;
			else
				next_state = ANY_SCL_SDA_COMB;
		end
		else
			next_state = IDLE;
	end
	ANY_SCL_SDA_COMB: begin
		if(sda && scl)
			next_state = SCL_HIGH_SDA_HIGH;
		else
			next_state = ANY_SCL_SDA_COMB;
	end
	SCL_HIGH_SDA_HIGH: begin
		if(sda && scl)
			next_state = SCL_HIGH_SDA_HIGH;
		else if (~sda && scl)
			next_state = SCL_HIGH_SDA_LOW;
		else
			next_state = ANY_SCL_SDA_COMB;
	end
	SCL_HIGH_SDA_LOW: begin
		next_state = ANY_SCL_SDA_COMB;
	end
	default: next_state = IDLE;
	endcase
end
/* Next Ouput Logic*/
always@(*) begin
	next_start_detected = current_start_detected;
	case(state)
		IDLE: next_start_detected = 0;
		ANY_SCL_SDA_COMB: next_start_detected = 0;
		SCL_HIGH_SDA_HIGH: next_start_detected = 0;
		SCL_HIGH_SDA_LOW: next_start_detected = 1;
	endcase
end
/* Sincronic Logic */
always @(posedge clk) begin
	if(~enable)
		state <= IDLE;
	else
		state <= next_state;
		
	current_start_detected <= next_start_detected;
end
endmodule
