
`include "../include/registers.vh"
`include "../include/K_code_defines.vh"
`include "../include/Vendor_Defines.vh"
`include "../include/SOP_defines.vh"
`include "register_file/register_file.v"
`include "HR/hr_sm.v"
`include "RX/rx_sm.v"
`include "TX/tx_sm.v"
`include "Physical_Layer/Receiver/FINAL_RECEIVER/PHY_receiver.v"
`include "Physical_Layer/Receiver/4b5b_decoder/4b5b_decoder.v"
`include "Physical_Layer/Receiver/BMC_Decoder/PLL/BMC_to_bin.v"
`include "Physical_Layer/Receiver/SOP_Detect/sop_detect.v"
`include "Physical_Layer/Receiver/CRC/CRC_32_outputlogic_4b.v"
`include "Physical_Layer/Transmiter/PHY_transmiter.v"
`include "Physical_Layer/Transmiter/4b5b_Encoder/4b5b_encoder.v"
`include "Physical_Layer/Transmiter/BMC/bmc_encoder.v"
`include "Physical_Layer/Transmiter/CRC/CRC_32_8b_outputlogic.v"
`include "I2C_MGR/DETECT_START_SM/detect_start_sm.v"
`include "I2C_MGR/DETECT_STOP_SM/detect_stop_sm.v"
`include "I2C_MGR/R8B_SM/r8b_sm.v"
`include "I2C_MGR/S8B_SM/i2c_s8b_sm.v"
`include "I2C_MGR/SEND_ACK_SM/i2c_send_ack_sm.v"
`include "I2C_MGR/SEND_NACK_SM/i2c_send_nack_sm.v"
`include "I2C_MGR/RA_SM/i2c_ra_sm.v"
`include "I2C_MGR/I2C_SLAVE_MANAGER/i2c_slave_manager.v"

module TCPC (
	input pll_bmc_clk, //from PLL
	input bmc_clk, //Internal Independed from pll
	input clk,	
	
	input reset,

	input cc_in,
	input sda_in,
	input scl_in,

	output cc_out_hiz,
	output cc_out,
	output sda_out,
	output scl_out,
	
	input preamble_done,
	input [6:0] slave_address,
	output reg Alert
);
wire [7:0] TRANSMIT;
wire [7:0] TX_BUF_HEADER_BYTE_1;
wire [7:0] RX_BUF_HEADER_BYTE_1;
wire [7:0] RX_BUF_HEADER_BYTE_0;
wire [7:0] RX_BUF_FRAME_TYPE;
wire [15:0] RX_BUF_HEADER;
wire [7:0] RECEIVE_DETECT;
wire [7:0] RX_BUF_OBJ1_BYTE_0;
wire [7:0] RX_BUF_OBJ1_BYTE_1;
wire [7:0] MESSAGE_HEADER_INFO;
wire [15:0] ALERT;

reg I2C_enable;
reg PHY_rec_enable;
reg Rx_start;
always@(posedge clk) begin
	if(reset) begin
		I2C_enable <= 0;
		PHY_rec_enable <= 0;
		Rx_start <= 1;
	end
	else begin
		I2C_enable <= 1;
		PHY_rec_enable <= 1;
		Rx_start <= 0;
	end
end
wire [7:0] I2C_reg_address;
wire [7:0] I2C_write_data;
wire [7:0] I2C_read_data;
i2c_slave_mgr_sm I2C(
        .clk(clk),
        .enable(I2C_enable),
        .slave_address_in(slave_address),
        
        .scl_in(scl_in),
        .scl_out(scl_out),
        .sda_in(sda_in),
        .sda_out(sda_out),
        
        // Data from reg file //
        .reg_address(I2C_reg_address),
        .write_data_out(I2C_write_data),
        .read_data_in(I2C_read_data),
        .read_data_done(I2C_read_data_done),
        .save_data(I2C_save_data),
        .load_data(I2c_load_data)
);
wire [7:0] PHY_trans_read_data;
wire [7:0] PHY_trans_reg_address;
phy_transmiter PHY_trans(
	.clk(clk),
	.bmc_clk(bmc_clk),
	.data_in_from_reg(PHY_trans_read_data),
	.load_done(PHY_trans_load_done),// tbd
	.enable(PHY_trans_enable || Rx_send_goodcrc_message_to_PHY ||req_PHY_send_hr|| req_PHY_send_cr),
	.send_goodCRC(Rx_send_goodcrc_message_to_PHY),
	.send_hard_reset(req_PHY_send_hr),
	.send_cable_reset(req_PHY_send_cr),	
	.RX_BUF_FRAME_TYPE(RX_BUF_FRAME_TYPE),
	.RX_BUF_HEADER(RX_BUF_HEADER),
	.MESSAGE_HEADER_INFO(MESSAGE_HEADER_INFO),
	.TRANSMIT(TRANSMIT),
	.reg_address(PHY_trans_reg_address),
	.load(PHY_trans_load),
	.cc_hiz(cc_out_hiz),
	.cc(cc_out),
	.done(PHY_trans_done)
);
wire [7:0] PHY_rec_reg_address;
wire [7:0] PHY_rec_write_data;
phy_receiver PHY_rec( // Solo falta el problema con el calculo del CRC para producir el goodCRC
	.clk(clk),
	.pll_bmc_clk(pll_bmc_clk),
	.enable(PHY_rec_enable),
	.preamble_done(preamble_done),
	.RECEIVE_DETECT(RECEIVE_DETECT),
	.cc(cc_in),
	.rx_buffer_full(ALERT[2]),
	.expect_GoodCRC(Tx_expect_GoodCRC),
	
	.reg_address(PHY_rec_reg_address),
	.write_data(PHY_rec_write_data),
	.write(PHY_rec_write),
	
	.RX_BUF_HEADER(RX_BUF_HEADER),
	.RX_BUF_EXT_HEADER({RX_BUF_OBJ1_BYTE_1, RX_BUF_OBJ1_BYTE_1}),
	.message_received_from_phy(message_received_from_phy),
	
	.hard_reset(PHY_rec_hard_reset),
	.cable_reset(PHY_rec_cable_reset),
	.GoodCRC(PHY_rec_GoodCRC)
);
wire [7:0] Rx_write_data;
wire [7:0] Rx_reg_address;
tcpc_rx Rx(
	.clk(clk),
	.start(Rx_start),
	.hard_reset(PHY_rec_hard_reset || reset),
	.cable_reset(PHY_rec_cable_reset),
	.rx_buffer_full(ALERT[2]),
	.message_received_from_phy(message_received_from_phy),
	.goodcrc_message_discarted_bus_idle(),
	.goodcrc_transmission_complete(PHY_trans_done),
	.tx_state_machine_active(tx_state_machine_active),
	
	.discard_transmition(Rx_Message_Discard_discard_transmition),
	.send_goodcrc_message_to_phy(Rx_send_goodcrc_message_to_PHY),
	
	.write_data(Rx_write_data),
	.reg_address(Rx_reg_address),
	.write(Rx_write)
	
);
 wire [7:0] Tx_write_data;
 wire [7:0] Tx_reg_address;
tcpc_tx Tx(
	.clk(clk),
	.PRL_Rx_Message_Discard_discard_transmition(Rx_Message_Discard_discard_transmition),
	.hard_reset(PHY_rec_hard_reset || reset),
	.cable_reset(PHY_rec_cable_reset),
	.TRANSMIT(TRANSMIT),
	.TRANSMIT_written(TRANSMIT_written),
	.message_sent_to_PHY(PHY_trans_done),
	.initialize_and_run_CRCReceive_Timer(initialize_and_run_CRCReceiveTimer),
	.CRCReceiveTimerTimeout(CRCReceiveTimerTimeout),
	.message_discarded_bus_idle(1'b0),
	.goodcrc_response_from_phy_layer(PHY_rec_GoodCRC),
	.TX_BUF_HEADER_BYTE_1(TX_BUF_HEADER_BYTE_1),
	.RX_BUF_HEADER_BYTE_1(RX_BUF_HEADER_BYTE_1),
	.RX_BUF_FRAME_TYPE(RX_BUF_FRAME_TYPE),
	.DFP(1'b1),
	.UFP(1'b1),
	
	.tx_state_machine_active(tx_machine_active),
	.write_data(Tx_write_data),
	.reg_address(Tx_reg_address),
	.write(Tx_write),
	.PHY_trans_enable(PHY_trans_enable),
	.initialize_and_run_CRCReceiveTimer(initialize_and_run_CRCReceiveTimer),
	.expect_goodCRC(Tx_expect_GoodCRC)
); 
wire [7:0] Hr_reg_address;
wire [7:0] Hr_write_data;
tcpc_hr Hr(
	.clk(clk),
	.power_on_reset(reset),
	
	.TRANSMIT_written(TRANSMIT_written),
	.TRANSMIT(TRANSMIT),
	
	.start_tHardResetComplete_timer(start_tHardResetComplete_timer),
	.req_PHY_send_hr(req_PHY_send_hr),
	.req_PHY_send_cr(req_PHY_send_cr),
	.hr_cr_sent(hr_cr_sent),
	.tHardResetComplete_expires(tHardResetComplete_expires),
	.stop_tHardResetComplete_timer(stop_tHardResetComplete_timer),
	.stop_attempting_to_send_hr_cr(stop_attempting_to_send_hr_cr),
	.reg_address(Hr_reg_address),
	.write_data(Hr_write_data),
	.write(Hr_write)
);

reg_file_8 register_file(
	.clk(clk),
	.reset(reset),
	
	.write_address_1(Rx_reg_address), // For RX SM
	.write_data_1(Rx_write_data),
	.write_1(Rx_write),
	
	.read_address_1(PHY_trans_reg_address), // For PHY_trans
	.read_data_1(PHY_trans_read_data),
	.read_1(PHY_trans_load),
	.read_1_done(PHY_trans_load_done),
	
	.write_address_2(I2C_reg_address), // For TCPM
	.write_data_2(I2C_write_data),
	.write_2(I2C_save_data),
	
	.read_address_2(I2C_reg_address), // For TCPM
	.read_data_2(I2C_read_data),
	.read_2(I2c_load_data),
	.read_2_done(I2C_read_data_done),

	.write_address_3(PHY_rec_reg_address), // For PHY_rec
	.write_data_3(PHY_rec_write_data),
	.write_3(PHY_rec_write),
/*	
	.[7:0] read_address_3(), // For TCPC
	output reg [7:0] read_data_3,
	.read_3,

	output reg read_3_done,
	*/
	.TRANSMIT_written(TRANSMIT_written),		
	.write_address_4(Tx_reg_address), // For TX
	.write_data_4(Tx_write_data),
	.write_4(Tx_write),
	
	.write_address_5(Hr_reg_address), // For HR
	.write_data_5(Hr_write_data),
	.write_5(Hr_write),	
	
	.TRANSMIT(TRANSMIT),
	.TX_BUF_HEADER_BYTE_1(TX_BUF_HEADER_BYTE_1),
	.RX_BUF_HEADER_BYTE_1(RX_BUF_HEADER_BYTE_1),
	.RX_BUF_HEADER_BYTE_0(RX_BUF_HEADER_BYTE_0),
	.RX_BUF_FRAME_TYPE(RX_BUF_FRAME_TYPE),
	.RECEIVE_DETECT(RECEIVE_DETECT),
	.RX_BUF_OBJ1_BYTE_0(RX_BUF_OBJ1_BYTE_0),
	.RX_BUF_OBJ1_BYTE_1(RX_BUF_OBJ1_BYTE_1),
	.MESSAGE_HEADER_INFO(MESSAGE_HEADER_INFO),
	.ALERT(ALERT)



	
);

assign RX_BUF_HEADER = {RX_BUF_HEADER_BYTE_1, RX_BUF_HEADER_BYTE_0};
assign Alert = (ALERT == 0) ? 1:0;
initial $dumpvars;
endmodule

