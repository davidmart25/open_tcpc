module tcpc_rx (
		input rx_buff_full,
		input msg_r_phy,
		input unexpected_goodcrc_r,
		input goodcrc_trans_complete,
		input goodcrc_message_disc_bus_idle,
		input tx_state_machine_active,
		input reset,
		input clk,

		output reg tx_msg_disc,
		output reg send_goodcrc_msg_phy,
		output reg update_receive_buffer
	);
	//One Hot encoding
	parameter PRL_Rx_Wait_for_PHY_message = 4'b0001;
	parameter PRL_Rx_Message_Discard = 4'b0010;
	parameter PRL_Rx_Report_SOP = 4'b0100;
	parameter PRL_Rx_Send_GoodCRC = 4'b1000;

	reg [3:0] state;	
	reg [3:0] next_state;
	reg next_tmd;
	reg next_sgmp;
	reg next_urb;

	initial begin
		$dumpfile("rx.vcd");
		$dumpvars;
	end
	always @(*) begin
		if(reset) begin
			next_state <= PRL_Rx_Wait_for_PHY_message;
			next_tmd <= 0;
			next_sgmp <= 0;
			next_urb <= 0;
		end

		else begin

		case(state)


			PRL_Rx_Wait_for_PHY_message:

			if (rx_buff_full) begin
				next_state <= PRL_Rx_Wait_for_PHY_message;
				next_tmd <=0;
				next_sgmp <=0;
				next_urb <=0;			
			end 
	
			else if (~rx_buff_full && msg_r_phy) begin
				next_state <= PRL_Rx_Message_Discard;
				next_sgmp <= 0;
				next_urb <= 0;
				
				if (tx_state_machine_active) begin
					next_tmd <= 1;								
				end
				
				else begin
					next_tmd <= 0;					
				end
			end
			
			else begin

				next_state <= PRL_Rx_Wait_for_PHY_message;
				next_tmd <= 0; 				
				next_sgmp <= 0;
				next_urb <= 0;
				
			end

			PRL_Rx_Message_Discard:
			
			if(unexpected_goodcrc_r) begin
				next_state <= PRL_Rx_Report_SOP;
				next_tmd <= 0;
				next_sgmp <= 0;
				next_urb <= 1;		
			end
			
			else begin
				next_state <= PRL_Rx_Send_GoodCRC;
				next_tmd <= 0;
				next_sgmp <= 1;
				next_urb <= 0;
			end


			PRL_Rx_Report_SOP: begin
				next_state <= PRL_Rx_Wait_for_PHY_message;
				next_tmd <= 0;
				next_sgmp <= 0;
				next_urb <= 0;
			end
			PRL_Rx_Send_GoodCRC:
			if( goodcrc_trans_complete || goodcrc_message_disc_bus_idle ) begin 
				next_state <= PRL_Rx_Report_SOP;
				next_tmd <= 0;
				next_sgmp <= 0;
				next_urb <= 1;
			end
			
			else begin
				next_state <=PRL_Rx_Send_GoodCRC;
				next_tmd <= 0;
				next_sgmp <=1;
				next_urb <=1;
			end

			default: begin
			next_state <= PRL_Rx_Wait_for_PHY_message;
			next_tmd <= 0;
			next_sgmp <= 0;
			next_urb <= 0;	
			end  				
		endcase		
		end				
	end

	always @(posedge clk) begin
		
		if(reset) begin
			state <= PRL_Rx_Wait_for_PHY_message;
			tx_msg_disc <= 0;
			send_goodcrc_msg_phy <= 0;
			update_receive_buffer <= 0;	
		end
		
		else begin
			state <= next_state;
			tx_msg_disc <= next_tmd;
			send_goodcrc_msg_phy <= next_sgmp;
			update_receive_buffer <= next_urb;	
		end 
	end
endmodule

		

