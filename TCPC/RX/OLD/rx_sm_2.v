module tcpc_rx(
	input rx_buff_full,
	input msg_r_phy,
	input unexpected_goodcrc_r,
	input goodcrc_trans_complete,
	input goodcrc_message_disc_bus_idle,
	input tx_state_machine_active,
	input reset,
	input clk,

	output tx_msg_disc,
	output send_goodcrc_msg_phy,
	output update_receive_buffer

	);
	//One Hot encoding
	parameter PRL_Rx_Wait_for_PHY_message = 4'b0001;
	parameter PRL_Rx_Message_Discard = 4'b0010;
	parameter PRL_Rx_Report_SOP = 4'b0100;
	parameter PRL_Rx_Send_GoodCRC = 4'b1000;

	reg [3:0] state;	
	reg [3:0] next_state;

	initial begin
		$dumpfile("rx.vcd"); //non synth code, simulation only
		$dumpvars;
	end

	//1-bit outputs logic
	
	assign tx_msg_disc = (state == PRL_Rx_Message_Discard) & (tx_state_machine_active);
	assign send_goodcrc_msg_phy = (state == PRL_Rx_Send_GoodCRC);
	assign update_receive_buffer = (state == PRL_Rx_Report_SOP);	

	always @(*) begin
		case (state) //Logica de proximo estado
		
		PRL_Rx_Wait_for_PHY_message: begin
		
			if(rx_buff_full)
				next_state <= PRL_Rx_Wait_for_PHY_message;
			else if (~rx_buff_full && msg_r_phy)
				next_state <= PRL_Rx_Message_Discard;
			else
				next_state <= PRL_Rx_Wait_for_PHY_message;
		
		end
		
		PRL_Rx_Message_Discard: begin
		
			if(unexpected_goodcrc_r)
				next_state <= PRL_Rx_Report_SOP;
			else
				next_state <= PRL_Rx_Send_GoodCRC;
			
		end

		PRL_Rx_Report_SOP: begin
			
			next_state <= PRL_Rx_Wait_for_PHY_message;
		

		end
		
		PRL_Rx_Send_GoodCRC: begin
		
			if (goodcrc_message_disc_bus_idle )
				next_state <= PRL_Rx_Wait_for_PHY_message;
			else if (goodcrc_trans_complete || goodcrc_message_disc_bus_idle)
				next_state <= PRL_Rx_Report_SOP;
			else
				next_state <= PRL_Rx_Send_GoodCRC;	
		
		end
		
		default: begin
			next_state <=PRL_Rx_Wait_for_PHY_message;
		end
	
		endcase
		/*
		case (state) //Acciones al salir.
		
		PRL_Rx_Wait_for_PHY_message: begin
		
		end
		
		PRL_Rx_Message_Discard: begin

		end

		PRL_Rx_Report_SOP: begin
			
		end
		
		PRL_Rx_Send_GoodCRC: begin
				

		end

		default: begin

		end

		endcase
		*/
		/*
		case(next_state) //Acciones al entrar.
		
		PRL_Rx_Wait_for_PHY_message: begin
			next_tmd <= 0;
			next_sgmp <= 0;
			next_urb <= 0;
		end
		
		PRL_Rx_Message_Discard: begin
		
			next_sgmp <= 0;
			next_urb <= 0;
	
			if(tx_state_machine_active)
				next_tmd <=1;
			else
				next_tmd <=0;

		end

		PRL_Rx_Report_SOP: begin
			next_tmd <= 0;
			next_sgmp <= 1;
			next_urb <= 0;		
		end
		
		PRL_Rx_Send_GoodCRC: begin
			next_tmd <= 0;
			next_sgmp <= 1;
			next_urb <= 0;		
		end

		default: begin
			next_tmd <= 0;
			next_sgmp <= 0;
			next_urb <= 0;
		end

		endcase
		*/

	end
	
	always @(posedge clk) begin
		if(reset) begin

			state <= PRL_Rx_Wait_for_PHY_message;	

		end

		else begin

			state <= next_state;
	
		end		

	end

	
endmodule
		
		
		
