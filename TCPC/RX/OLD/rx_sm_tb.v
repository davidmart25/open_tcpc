`include "rx_sm.v"

module testbench;
	wire rx_buff_full;
	wire msg_r_phy;
	wire unexpected_goodcrc_r;
	wire goodcrc_trans_complete;
	wire goodcrc_message_disc_bus_idle;
	wire tx_state_machine_active;
	wire reset;
	wire clk;

	wire tx_msg_disc;
	wire send_goodcrc_msg_phy;
	wire update_receive_buffer;

	tcpc_rx rx_sm (.rx_buff_full(rx_buff_full), .msg_r_phy(msg_r_phy), .unexpected_goodcrc_r(unexpected_goodcrc_r), .goodcrc_trans_complete(goodcrc_trans_complete), .goodcrc_message_disc_bus_idle(goodcrc_message_disc_bus_idle), .tx_state_machine_active(tx_state_machine_active), .reset(reset), .clk(clk), .tx_msg_disc(tx_msg_disc), .send_goodcrc_msg_phy(send_goodcrc_msg_phy), .update_receive_buffer(update_receive_buffer));

	tcpc_rx_test test (.rx_buff_full(rx_buff_full), .msg_r_phy(msg_r_phy), .unexpected_goodcrc_r(unexpected_goodcrc_r), .goodcrc_trans_complete(goodcrc_trans_complete), .goodcrc_message_disc_bus_idle(goodcrc_message_disc_bus_idle), .tx_state_machine_active(tx_state_machine_active), .reset(reset), .clk(clk), .tx_msg_disc(tx_msg_disc), .send_goodcrc_msg_phy(send_goodcrc_msg_phy), .update_receive_buffer(update_receive_buffer));

endmodule

module tcpc_rx_test(
	output reg rx_buff_full,
	output reg msg_r_phy,
	output reg unexpected_goodcrc_r,
	output reg goodcrc_trans_complete,
	output reg goodcrc_message_disc_bus_idle,
	output reg tx_state_machine_active,
	output reg reset,
	output reg clk,

	input tx_msg_disc,
	input send_goodcrc_msg_phy,
	input update_receive_buffer	
);


integer seed=1;

initial begin
	//$dumpfile("register_file.vcd");
	//$dumpvars;
	//
	#1
	rx_buff_full = 1;
	msg_r_phy = 0;
	unexpected_goodcrc_r = 0;
	goodcrc_trans_complete = 0;
	goodcrc_message_disc_bus_idle = 0;
	tx_state_machine_active = 0;
	reset = 1;
	clk = 0;
	
	#1 clk =1;
	reset = 0;
	#1 clk =0;	
	forever begin
		#1 clk = 1;
		rx_buff_full = $random(seed);
		msg_r_phy = $random(seed);
		unexpected_goodcrc_r = $random(seed);
		goodcrc_trans_complete = $random(seed);
		goodcrc_message_disc_bus_idle = $random(seed);
		tx_state_machine_active = $random(seed);	
		#1 clk = 0;	
	end	



end
endmodule
			



