module tcpc_rx(
	input clk,
	input start,
	input hard_reset,
	input cable_reset,
	input rx_buffer_full,
	input message_received_from_phy,
	input [15:0] RX_BUF_HEADER,
	//input unexpected_goodcrc_received,
	input goodcrc_message_discarted_bus_idle,
	input goodcrc_transmission_complete,
	input tx_state_machine_active,
	
	output discard_transmition,
	output send_goodcrc_message_to_phy,
	
	output reg [7:0] write_data,
	output reg [7:0] reg_address,
	output reg write
	
);
/* State REGs */
reg [3:0] state;	
reg [3:0] next_state;

/* One Hot Encoding */
parameter PRL_Rx_Wait_for_PHY_message = 4'b0001;
parameter PRL_Rx_Message_Discard =      4'b0010;
parameter PRL_Rx_Report_SOP =           4'b0100;
parameter PRL_Rx_Send_GoodCRC =         4'b1000;

/* Next Values */
reg next_discard_transmition;
reg next_assert_ALERT_TxMessage_Discarded;
reg next_send_goodcrc_message_to_phy;
reg next_assert_ALERT_ReceiveSOPStatus;

reg [7:0] next_write_data;
reg [7:0] next_reg_address;
reg next_write;
/* Current Values */
reg current_discard_transmition;
reg current_assert_ALERT_TxMessage_Discarded;
reg current_send_goodcrc_message_to_phy;
reg current_assert_ALERT_ReceiveSOPStatus;
/* Delayed Values*/

/*Assigns */
assign unexpected_GoodCRC_received  = ((Number_of_Data_Objects == 0) & (Message_Type == 1));//Message_Type = goodCRC;
assign Number_of_Data_Objects = RX_BUF_HEADER[14:12];
assign Message_Type = RX_BUF_HEADER[4:0];
/*Output Assigns */
assign discard_transmition = current_discard_transmition;
assign assert_ALERT_TxMessage_Discarded = current_assert_ALERT_TxMessage_Discarded;
assign send_goodcrc_message_to_phy = current_send_goodcrc_message_to_phy;
assign assert_ALERT_ReceiveSOPStatus = current_assert_ALERT_ReceiveSOPStatus;

/* Next State Logic */
always @(*) begin
	next_state = PRL_Rx_Wait_for_PHY_message;
	case(state)
	PRL_Rx_Wait_for_PHY_message: begin
		if(rx_buffer_full)
			next_state = PRL_Rx_Wait_for_PHY_message;
		else if( !rx_buffer_full && message_received_from_phy)
			next_state = PRL_Rx_Message_Discard;
		else
			next_state = PRL_Rx_Wait_for_PHY_message;
	end
	PRL_Rx_Message_Discard: begin
		if(unexpected_GoodCRC_received)
			next_state = PRL_Rx_Report_SOP;
		else
			next_state = PRL_Rx_Message_Discard;
	end
	PRL_Rx_Report_SOP: next_state = PRL_Rx_Wait_for_PHY_message;
	PRL_Rx_Send_GoodCRC: begin
		if( goodcrc_transmission_complete || goodcrc_message_discarted_bus_idle )
			next_state = PRL_Rx_Report_SOP;
		else if (goodcrc_message_discarted_bus_idle)
			next_state = PRL_Rx_Wait_for_PHY_message;
		else
			next_state = PRL_Rx_Send_GoodCRC;
	end
	endcase
	if(hard_reset || cable_reset || start)
		next_state = PRL_Rx_Wait_for_PHY_message;
end

/*Next Output Logic */
always @(*) begin
	next_discard_transmition = 0;
	next_send_goodcrc_message_to_phy = 0;
	next_assert_ALERT_ReceiveSOPStatus = 0;
	next_reg_address = reg_address;
	next_write = 0;
	next_write_data = write_data;
	case(next_state)
	//PRL_Rx_Wait_for_PHY_message:
	PRL_Rx_Message_Discard: begin
		if(tx_state_machine_active) begin
			next_discard_transmition = 1;
			next_reg_address = `ALERT;
			next_write_data = 0;
			next_write_data[5] = 1;
			next_write = 1;
		end
	end
	PRL_Rx_Send_GoodCRC: next_send_goodcrc_message_to_phy = 1;
	PRL_Rx_Report_SOP: begin
		next_reg_address = `ALERT;
		next_write_data = 0;
		next_write_data[2] = 1;
		next_write = 1;
	end
	endcase
end
/*CLK Logic */
always @(posedge clk) begin
	if(start || hard_reset || cable_reset)
		state <= PRL_Rx_Wait_for_PHY_message;
	else
		state <= next_state;
		
	current_discard_transmition = next_discard_transmition;
	current_assert_ALERT_TxMessage_Discarded = next_assert_ALERT_TxMessage_Discarded;
	current_send_goodcrc_message_to_phy = next_send_goodcrc_message_to_phy;
	current_assert_ALERT_ReceiveSOPStatus = next_assert_ALERT_ReceiveSOPStatus;
end
endmodule
