module phy_receiver( // Solo falta el problema con el calculo del CRC para producir el goodCRC
	input clk,
	input pll_bmc_clk,
	input enable,
	input preamble_done,
	input [7:0] RECEIVE_DETECT,
	input cc,
	input rx_buffer_full,
	input expect_GoodCRC,
	
	output reg [7:0] reg_address,
	output reg [7:0] write_data,
	output reg write,
	
	input [15:0] RX_BUF_HEADER,
	input [15:0] RX_BUF_EXT_HEADER,
	output reg message_received_from_phy,
	
	output reg hard_reset,
	output reg cable_reset,
	output reg GoodCRC
);
/* State Regs*/
reg [5:0] state;
reg [5:0] next_state;
/* One hot encoding */
parameter RECEIVE_PREAMBLE = 6'b000001;
parameter RECEIVE_SOP = 6'b000010;
parameter WRITE_RX_BUF_FRAME_TYPE =6'b000100;
parameter RECEIVE_BYTE = 6'b001000;
parameter WRITE_BYTE = 6'b010000;
parameter WRITE_BYTE_COUNT = 6'b100000;

/* Internal Values */
reg [9:0] buffer_5b;
wire [7:0] data_4b;
reg [7:0] crc_data_in;
wire [31:0] crc_out;
reg [31:0] crc_buff;
wire [7:0] byte_to_write;
reg crc_enable;
reg [3:0] buffer_5b_counter;
reg sop_detect_enable;
wire [2:0] SOP;
reg [7:0] RECEIVE_BYTE_COUNT;
reg crc_reset;
/* Next Values */
reg next_crc_reset;
reg next_crc_enable;
reg next_sop_detect_enable;
reg [3:0] next_buffer_5b_counter;
reg [7:0] next_reg_address;
reg [7:0] next_write_data;
reg next_write;
reg [7:0] next_RECEIVE_BYTE_COUNT;
reg [9:0] next_buffer_5b;
reg next_message_received_from_phy;
reg [2:0] next_SOP;
reg next_GoodCRC;
reg next_cable_reset;
reg next_hard_reset;
reg [31:0] next_crc_buff;
/* Current Values */
reg [2:0] current_SOP;
/* Delayed Values */
reg pll_bmc_clk_d;
/* Assigns */
assign posedge_pll_bmc_clk = ~pll_bmc_clk_d & pll_bmc_clk;
//assign crc_data_in = data_4b;
assign byte_to_write = data_4b;
assign Number_of_Data_Objects = RX_BUF_HEADER[14:12];
assign Message_Type = RX_BUF_HEADER[4:0];
integer i;
always@(*)begin
	for(i = 0; i<= 7; i = i+1)
		crc_data_in[i] = data_4b[7-i];		
end
//assign EOP_detected = buffer_5b[4:0] & `EOP;
/* Module instantiation*/
bmc_to_binary bmc_to_binary (
	.pll_bmc_clk(pll_bmc_clk),
	.bmc(cc),
	.binary(serial_binary)
);
sop_detect sop_detect(
	.pll_bmc_clk(pll_bmc_clk),
	.data_in(serial_binary),
	.enable(sop_detect_enable),
	.SOP(SOP),
	.done(sop_detect_done)
);
decoder4b5b decoder4b5b_low(
	.data_in_5b(buffer_5b[4:0]),
	.data_out_4b(data_4b[3:0])
);
decoder4b5b decoder4b5b_high(
	.data_in_5b(buffer_5b[9:5]),
	.data_out_4b(data_4b[7:4])
);
crc crc32(
	.data_in(crc_data_in),
	.crc_en(crc_enable),
	.crc_out(crc_out),
	.rst(crc_reset),
	.clk(clk)
);
/* Next State Logic */
always@(*) begin
	next_state = RECEIVE_PREAMBLE;
	case(state)
	RECEIVE_PREAMBLE: next_state = preamble_done ? RECEIVE_SOP : RECEIVE_PREAMBLE;
	RECEIVE_SOP: begin
		if(sop_detect_done && posedge_pll_bmc_clk) begin // Se están contaminando los datos?
			case(SOP)
				`CABLE_RESET: next_state = (RECEIVE_DETECT[6] == 1) ?  WRITE_RX_BUF_FRAME_TYPE : RECEIVE_PREAMBLE;
				`HARD_RESET: next_state = (RECEIVE_DETECT[5] == 1) ?  RECEIVE_BYTE: RECEIVE_PREAMBLE;
				`SOP2_DEBUG: next_state = (RECEIVE_DETECT[4] == 1) ?  WRITE_RX_BUF_FRAME_TYPE : RECEIVE_PREAMBLE;
				`SOP1_DEBUG: next_state = (RECEIVE_DETECT[3] == 1) ?  WRITE_RX_BUF_FRAME_TYPE : RECEIVE_PREAMBLE;
				`SOP2: next_state = (RECEIVE_DETECT[2] == 1) ?  WRITE_RX_BUF_FRAME_TYPE : RECEIVE_PREAMBLE;
				`SOP1: next_state = (RECEIVE_DETECT[1] == 1) ?  WRITE_RX_BUF_FRAME_TYPE : RECEIVE_PREAMBLE;
				`SOP: next_state = (RECEIVE_DETECT[0] == 1) ?  WRITE_RX_BUF_FRAME_TYPE : RECEIVE_PREAMBLE;
				default: next_state = RECEIVE_PREAMBLE;
			endcase
		end
		else
			next_state = RECEIVE_SOP;
	end
	WRITE_RX_BUF_FRAME_TYPE: next_state = RECEIVE_BYTE;
	RECEIVE_BYTE: begin
		if(posedge_pll_bmc_clk && buffer_5b_counter == 4)
			if(buffer_5b[4:0] == `EOP)
				next_state = WRITE_BYTE_COUNT;
			else
				next_state = RECEIVE_BYTE;
		else if(posedge_pll_bmc_clk && buffer_5b_counter == 9)
			next_state = WRITE_BYTE;
		else
			next_state = RECEIVE_BYTE;
	end
	WRITE_BYTE: next_state = RECEIVE_BYTE;
	WRITE_BYTE_COUNT: next_state = RECEIVE_PREAMBLE;
	endcase
	if(~enable)
		next_state = RECEIVE_PREAMBLE;
end
/* Next Oyutput Logic */
always@(*) begin
	next_crc_reset = 0;
	next_crc_enable = 0;
	next_sop_detect_enable =0;
	next_buffer_5b_counter = 0;
	next_reg_address = reg_address;
	next_write_data = write_data;
	next_write = 0;
	next_RECEIVE_BYTE_COUNT = RECEIVE_BYTE_COUNT;
	next_buffer_5b = buffer_5b;
	next_message_received_from_phy = 0;
	next_SOP = current_SOP;
	next_GoodCRC = 0;
	next_cable_reset = 0;
	next_hard_reset = 0;
	next_crc_buff = crc_buff;
	case(state)
	RECEIVE_PREAMBLE: next_RECEIVE_BYTE_COUNT = 0;
	RECEIVE_SOP: begin
		next_crc_reset = 1;
		next_sop_detect_enable = 1;
		if(sop_detect_done && posedge_pll_bmc_clk)
			next_SOP = SOP;
	end
	WRITE_RX_BUF_FRAME_TYPE: begin
		next_sop_detect_enable = 1;
		next_reg_address = `RX_BUF_FRAME_TYPE;
		next_write_data = SOP;
		next_write = 1;
	end
	RECEIVE_BYTE: begin
		next_buffer_5b_counter = buffer_5b_counter;
		next_buffer_5b[buffer_5b_counter] = serial_binary;
		if(posedge_pll_bmc_clk) begin
			next_buffer_5b[buffer_5b_counter] = buffer_5b[buffer_5b_counter];
			next_buffer_5b_counter = buffer_5b_counter +1;
			if(buffer_5b_counter == 9)
				next_buffer_5b_counter = 0;
		end
	end
	WRITE_BYTE: begin
		next_RECEIVE_BYTE_COUNT = RECEIVE_BYTE_COUNT +1;
		next_crc_enable = 1;
		if(reg_address +1 <= `TX_BUF_OBJ7_BYTE_3) begin
			next_reg_address = reg_address +1;
			next_write_data = byte_to_write;
			next_write = 1;	
		end
		else begin
			next_crc_buff = crc_buff >> 8;
			next_crc_buff [31:24] = byte_to_write;
		end
	end
	WRITE_BYTE_COUNT: begin
		if(crc_out == 32'hC704DD7B ) begin // GoodCRC
			next_message_received_from_phy = 1;
			if(current_SOP == `CABLE_RESET) begin
				next_message_received_from_phy = 0;
				next_cable_reset = 1;
			end
			else if (current_SOP == `HARD_RESET) begin
				next_message_received_from_phy = 0;
				next_cable_reset = 1;
			end
			else if (Number_of_Data_Objects == 0 && Message_Type == 5'b00001)
				next_GoodCRC = 1;
				if(expect_GoodCRC)
					next_message_received_from_phy = 0;
				else
					next_message_received_from_phy = 1;
		next_reg_address = `RECEIVE_BYTE_COUNT;
		next_write_data = RECEIVE_BYTE_COUNT - 4;
		next_write = 1;		
		end
	end
	endcase
end

always@(posedge clk) begin
	pll_bmc_clk_d<= pll_bmc_clk;
	if(~enable)
		state <= RECEIVE_PREAMBLE;
	else
		state <= next_state;
		
	crc_enable <= next_crc_enable;	
	crc_reset <= next_crc_reset;
	sop_detect_enable <= next_sop_detect_enable;
	buffer_5b_counter <= next_buffer_5b_counter;
	reg_address <= next_reg_address;
	write_data <= next_write_data;
	write <= next_write;
	RECEIVE_BYTE_COUNT <= next_RECEIVE_BYTE_COUNT;
	buffer_5b <= next_buffer_5b;
	message_received_from_phy <= next_message_received_from_phy;
	GoodCRC <= next_GoodCRC;
	hard_reset <= next_hard_reset;
	cable_reset <= next_cable_reset;
end
endmodule
