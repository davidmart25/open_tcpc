`define LOG2_MAX_BMC_CLK_RATIO 16

module recover_clk (
	input clk,
	input bmc,
	input enable,
	input [`LOG2_MAX_BMC_CLK_RATIO-1:0] clk_window,
	output [`LOG2_MAX_BMC_CLK_RATIO-1:0] clk_mult,
	output done
);
reg [5:0] state;
reg [5:0] next_state;

/* One Hot Encoding */
parameter IDLE = 					6'b000001;
parameter WAIT_POSEDGE_BMC = 		6'b000010;
parameter WAIT_SECOND_POSEDGE_BMC = 6'b000100;
parameter WAIT_NEGEDGE_BMC = 		6'b001000;
parameter WAIT_SECOND_NEGEDGE_BMC = 6'b010000;
parameter DONE = 					6'b100000;

/* Next Values*/
reg [`LOG2_MAX_BMC_CLK_RATIO-1:0] next_bit_time;
reg [`LOG2_MAX_BMC_CLK_RATIO+5:0] next_total_clk;
reg [`LOG2_MAX_BMC_CLK_RATIO-1:0] next_aux_bit_time;
reg [`LOG2_MAX_BMC_CLK_RATIO-1:0] next_bit_time_d;
reg [`LOG2_MAX_BMC_CLK_RATIO-1:0] next_clk_mult; //promedio de todos
reg [5:0] next_correct_transition;
reg next_data;
reg next_data_d;
reg next_done;


/*Current Values */
reg [`LOG2_MAX_BMC_CLK_RATIO-1:0] current_bit_time;
reg [`LOG2_MAX_BMC_CLK_RATIO+5:0] current_total_clk;
reg [`LOG2_MAX_BMC_CLK_RATIO-1:0] current_aux_bit_time;
reg [`LOG2_MAX_BMC_CLK_RATIO-1:0] current_bit_time_d;
reg [`LOG2_MAX_BMC_CLK_RATIO-1:0] current_clk_mult;
reg [5:0] current_correct_transition;
reg current_data;
reg current_data_d;
reg current_done;

/*Delayed Values */
reg bmc_d;
reg data_d;


/*Assigns */
assign posedge_bmc = ~bmc_d & bmc;
assign negedge_bmc = bmc_d & ~bmc;

/*Output Assigns */
assign clk_mult = current_clk_mult;
assign done = current_done;
/*Next State Logic */
always @(*) begin
	next_state = IDLE;
	case(state)
	IDLE: begin
		if(enable)
			next_state = WAIT_POSEDGE_BMC;
		else
			next_state = IDLE;
	end
	WAIT_POSEDGE_BMC: begin
		if(posedge_bmc) begin
			if(current_bit_time_d == 0) // First Entry
				next_state = WAIT_SECOND_POSEDGE_BMC;
			else if ( current_bit_time_d - clk_window <= current_bit_time && current_bit_time <= current_bit_time_d +clk_window) begin
				if( current_data_d == 1) begin
					next_state = WAIT_NEGEDGE_BMC;
				end
				else if (current_data_d == 0 ) begin
					next_state = WAIT_SECOND_POSEDGE_BMC;
				end
			end
			else
				next_state = WAIT_POSEDGE_BMC;
 		end
		else
			next_state = WAIT_POSEDGE_BMC;
	end
	WAIT_SECOND_POSEDGE_BMC: begin
		if(posedge_bmc) begin
			if(current_bit_time_d == 0) begin
				next_state = WAIT_NEGEDGE_BMC;
			end 
			else if ( current_bit_time_d - clk_window <= current_bit_time && current_bit_time <= current_bit_time_d +clk_window)
				next_state = WAIT_NEGEDGE_BMC;
			else
				next_state = WAIT_POSEDGE_BMC;
		end
		else
			next_state = WAIT_SECOND_POSEDGE_BMC;
	end
	WAIT_NEGEDGE_BMC: begin
		if(negedge_bmc) begin
			if(current_bit_time_d - clk_window <= current_bit_time && current_bit_time <= current_bit_time_d +clk_window) begin
				if(current_data_d == 1)
					next_state = WAIT_POSEDGE_BMC;
				else if (current_data_d == 0)
					next_state = WAIT_SECOND_NEGEDGE_BMC;
			end
			else	
				next_state = WAIT_SECOND_POSEDGE_BMC;
		end
		else
			next_state = WAIT_NEGEDGE_BMC;
	end
	WAIT_SECOND_NEGEDGE_BMC: begin
		if(negedge_bmc) begin
			if(current_bit_time_d - clk_window <= current_bit_time && current_bit_time <= current_bit_time_d +clk_window)
				next_state = WAIT_POSEDGE_BMC;
			else	
				next_state = WAIT_SECOND_POSEDGE_BMC;
		end
		else
			next_state = WAIT_SECOND_NEGEDGE_BMC;
	end
	DONE: next_state = DONE;
	endcase
	if(current_correct_transition ==63 && negedge_bmc)
		next_state = DONE;
end
/* Next Output Logic */ 
always @(*) begin
	next_total_clk = current_total_clk;
	next_aux_bit_time = current_aux_bit_time;
	next_bit_time_d = current_bit_time_d;
	next_correct_transition = current_correct_transition;
	next_data = current_data;
	next_data_d =  current_data_d;
	next_done = current_done;
	next_clk_mult = current_clk_mult; //promedio de todos
	next_total_clk = current_total_clk;
	
	next_bit_time = current_bit_time +1;

	case(state)
	IDLE: begin
		next_total_clk = 0;
		next_aux_bit_time = 0;
		next_correct_transition  = 0;
		next_bit_time = 0;
		next_bit_time_d = 0;
		next_data = 0;
		next_data_d = 0;
		next_done = 0;
	end
	WAIT_POSEDGE_BMC: begin
		if(posedge_bmc) begin
			if(current_bit_time_d == 0) begin
				next_total_clk = 0;
				next_correct_transition = current_correct_transition +1 ;
				next_bit_time = 0;
				next_bit_time_d = 0;
				next_data = 0;
			end
			else if ( current_bit_time_d - clk_window <= current_bit_time && current_bit_time <= current_bit_time_d +clk_window) begin
				next_total_clk = current_total_clk + current_bit_time;
				next_correct_transition = current_correct_transition +1 ;
				next_bit_time = 0;
				next_bit_time_d = current_bit_time;
				if(current_data_d == 0) begin
					next_data = 1;
					next_data_d = 1;
				end
				else if(current_data_d == 1) begin
					next_data = 0;
					next_data_d = 0;
				end
			end
			else begin
				next_total_clk = 0;
				next_correct_transition = 0;
				next_bit_time = 0;
				next_bit_time_d = 0;
				next_data = 0;
			end
		end
	end
	WAIT_SECOND_POSEDGE_BMC: begin
		if(posedge_bmc) begin
			if(current_bit_time_d == 0) begin
				next_total_clk = current_bit_time;
				next_correct_transition = current_correct_transition +1;
				next_bit_time = 0;
				next_bit_time_d = current_bit_time;
				next_data = 1;
			end
			else if (current_bit_time_d - clk_window <= current_bit_time && current_bit_time <= current_bit_time_d +clk_window) begin
				next_total_clk = current_total_clk + current_bit_time;
				next_correct_transition = current_correct_transition +1;
				next_bit_time = 0;
				next_bit_time_d = current_bit_time;
				if(current_data_d == 0) begin
					next_data = 1;
					next_data_d = 1;
				end
				else if(current_data_d == 1)begin
					next_data = 0;
					next_data_d = 0;
				end
			end
			else begin
				next_total_clk = 0;
				next_correct_transition = 0;
				next_bit_time = 0;
				next_bit_time_d = 0;
				next_data = 0;
			end
		end
	end
	WAIT_NEGEDGE_BMC: begin
		if(posedge_bmc) begin
			next_aux_bit_time = current_bit_time +1;
		end
		if(negedge_bmc) begin
			if(current_bit_time_d - clk_window <= current_bit_time && current_bit_time <= current_bit_time_d +clk_window) begin
				next_total_clk = current_total_clk + current_bit_time;
				next_correct_transition = current_correct_transition +1;
				next_bit_time = 0;
				next_bit_time_d = current_bit_time;
				if(current_data_d == 1) begin
					next_data = 0;
					next_data_d =0;
				end
				else if (current_data_d == 0)begin
					next_data = 1;
					next_data_d = 1;
				end
			end
			else begin
				next_total_clk =0 ;
				next_correct_transition = 1;
				next_bit_time = current_bit_time +1 - current_aux_bit_time;
				next_bit_time_d = 0;
				next_data = 0;
			end
		end
	end
	WAIT_SECOND_NEGEDGE_BMC: begin
		if(negedge_bmc) begin
			if(current_bit_time_d - clk_window <= current_bit_time && current_bit_time <= current_bit_time_d +clk_window) begin
				next_total_clk = current_total_clk + current_bit_time;
				next_correct_transition = current_correct_transition +1;
				next_bit_time = 0;
				next_bit_time_d = current_bit_time;	
				if(current_data_d == 1)begin
					next_data = 0;
					next_data_d =0;
				end
				else if(current_data_d == 0)begin
					next_data = 1;
					next_data_d = 1;
				end
			end
			else begin
				next_correct_transition =1;
				next_bit_time =current_bit_time +1;
				next_bit_time_d = 0;
				next_data =0;
			end
		end
	end
	DONE: begin
		next_aux_bit_time = 0;
		next_bit_time =0;
		next_correct_transition =0;
		next_data =0;
		next_data_d = 0;
		next_done =1;
		next_clk_mult = (current_total_clk+current_bit_time_d)/64; //promedio de todos
	end
	endcase
	//Me falta un reloj, porque pierdo el primero.
end
/* CLK Logic */
always @(posedge clk) begin // Arreglar esto
	if(~enable)
		state <= IDLE;
	else
		state <= next_state;
	bmc_d <= bmc;
	current_aux_bit_time <= next_aux_bit_time;
	current_bit_time <= next_bit_time;
	current_bit_time_d <= next_bit_time_d;
	current_correct_transition <= next_correct_transition;
	current_data <= next_data;
	current_data_d <= next_data_d;
	current_done <= next_done;
	current_clk_mult <= next_clk_mult; //promedio de todos
	current_total_clk <= next_total_clk;

end

initial begin
	$dumpfile("recover_clk.vcd"); //non synth code, simulation only
	$dumpvars;
end

endmodule