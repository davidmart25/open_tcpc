`include "BMC_to_bin.v" 
`include "../../../Transmiter/BMC/bmc_encoder.v"
module testbench;
	reg pll_bmc_clk;
	reg reset;
	wire bmc;
	reg enable;
	reg data_in;
	reg bmc_clk;

bmc_to_binary receiver(
	.pll_bmc_clk(pll_bmc_clk),
	//.reset(reset),
	.bmc(bmc)
);
bmc_encoder transmiter(
	.enable(enable),
	.data_in(data_in),
	.bmc_clk(bmc_clk),
	.bmc(bmc)
);
initial begin
	enable = 0;
	pll_bmc_clk = 0;
	bmc_clk =0;
	data_in = 1;
	reset = 0;
	#15 reset = 1;
	enable = 1;

end

initial begin
	#20
	forever #20 pll_bmc_clk = ~pll_bmc_clk;
end

initial begin
	#10
	forever #10 bmc_clk = ~bmc_clk;
end
always@(posedge pll_bmc_clk) begin
	data_in <= $random%2;
	reset <= 0;
end
endmodule

