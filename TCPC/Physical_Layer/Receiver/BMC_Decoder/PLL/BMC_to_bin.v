module bmc_to_binary (
	input pll_bmc_clk,
	//input reset,
	input bmc,
	output reg binary
);
/*Internal Values */
reg last_logic_lvl;
/*Next Values */
reg next_last_logic_lvl;
reg next_binary;
/*Next Output Logic */ 
always@(*) begin
	next_last_logic_lvl = bmc; // check this
	next_binary = 0;
	if(last_logic_lvl == 0)begin
		if(bmc == 0)
			next_binary = 1;
		else if(bmc == 1)
			next_binary = 0;	
	end 
	else if(last_logic_lvl == 1) begin
		if(bmc == 0)
			next_binary = 0;
		else if(bmc == 1)
			next_binary = 1;		
	end
/*
	if(reset)
		next_last_logic_lvl = 0; */
end
/* PLL CLK Logic */
always@(posedge pll_bmc_clk) begin
	last_logic_lvl <= next_last_logic_lvl;
	binary <= next_binary;
end

initial begin
	$dumpfile("BMC_to_bin.vcd"); //non synth code, simulation only
	$dumpvars;
end

endmodule
