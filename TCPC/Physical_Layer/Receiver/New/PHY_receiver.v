/*
`include "../../../../include/K_code_defines.vh"
`include "../../../../include/registers.vh"
`include "../../../../include/SOP_defines.vh"
`include "../4b5b_decoder/4b5b_decoder.v"
`include "../BMC_Decoder/PLL/BMC_to_bin.v"
`include "../SOP_Detect/sop_detect.v"
`include "../CRC/CRC_32_outputlogic_4b.v"
*/
module phy_receiver( // Solo falta el problema con el calculo del CRC para producir el goodCRC
	input clk,
	input pll_bmc_clk,
	input enable,
	input preamble_done,
	input [7:0] RECEIVE_DETECT,
	input cc,
	input rx_buffer_full,
	input expect_GoodCRC,
	
	output reg [7:0] reg_address,
	output reg [7:0] write_data,
	output reg write,
	
	input [15:0] RX_BUF_HEADER,
	input [15:0] RX_BUF_EXT_HEADER,
	output reg message_received_from_phy,
	
	output reg hard_reset,
	output reg cable_reset,
	output reg GoodCRC
);
/* State Registers */

reg [16:0] state;
reg [16:0] next_state;

/*One Hot Encoding*/
parameter RECEIVE_PREAMBLE =                17'b00000000000000001;
parameter RECEIVE_SOP =                     17'b00000000000000010;
parameter WRITE_RX_BUF_FRAME_TYPE =         17'b00000000000000100;
parameter RECEIVE_MESSAGE_HEADER_LOW =      17'b00000000000001000;
parameter WRITE_RX_BUF_HEADER_BYTE_0 =      17'b00000000000010000;
parameter RECEIVE_MESSAGE_HEADER_HIGH =     17'b00000000000100000;
parameter WRITE_RX_BUF_HEADER_BYTE_1 =      17'b00000000001000000;
parameter PARSE_MESSAGE_HEADER =            17'b00000000010000000;
parameter RECEIVE_EXT_MESSAGE_HEADER_LOW =  17'b00000000100000000;
parameter WRITE_EXT_MESSAGE_HEADER_LOW =    17'b00000001000000000;
parameter RECEIVE_EXT_MESSAGE_HEADER_HIGH = 17'b00000010000000000;
parameter WRITE_EXT_MESSAGE_HEADER_HIGH =   17'b00000100000000000;
parameter PARSE_EXT_MESSAGE_HEADER =        17'b00001000000000000;
parameter RECEIVE_BYTE =                    17'b00010000000000000;
parameter WRITE_BYTE =                      17'b00100000000000000;
parameter WRITE_BYTE_COUNT =                17'b01000000000000000;
parameter RECEIVE_CRC =                     17'b10000000000000000;

/* Internal Values */
reg [39:0] buffer_5b;
wire [31:0] data_4b;
reg [3:0] buffer_5b_counter;
reg [5:0] crc_5b_counter;

wire [2:0] Number_of_Data_Objects;
wire [4:0] Number_of_Byte_Objects;
reg [7:0] RECEIVE_BYTE_COUNT;
wire [31:0] Calculated_CRC;
wire [31:0] Received_CRC;
wire [2:0] SOP;
wire [5:0] Message_Type;

reg crc_reset ;
reg sop_detect_enable ;
wire [7:0] byte_to_write;

/* Next Values */
reg [39:0] next_buffer_5b;
reg [3:0] next_buffer_5b_counter;
reg [5:0] next_crc_5b_counter;
reg [7:0] next_RECEIVE_BYTE_COUNT;
reg next_message_received_from_phy;

reg next_crc_reset ;
reg next_sop_detect_enable ;
reg [7:0] next_reg_address ;
reg [7:0] next_write_data ;
reg next_write ;
reg [2:0] next_SOP;
reg next_GoodCRC;
reg next_cable_reset;
reg next_hard_reset;
/* Current Values */
reg [2:0] current_SOP;
/* Delayed Values */
reg pll_bmc_clk_d;
/* Assigns */
assign posedge_pll_bmc_clk = ~pll_bmc_clk_d & pll_bmc_clk;
assign byte_to_write = data_4b[7:0];
assign Extended = RX_BUF_HEADER[15];
assign Number_of_Data_Objects = RX_BUF_HEADER[14:12];
assign Number_of_Byte_Objects = Number_of_Data_Objects <<2;
assign Chunked = RX_BUF_EXT_HEADER[15];
assign Data_Size = RX_BUF_EXT_HEADER[8:0];
assign Received_CRC = data_4b;
assign Message_Type = RX_BUF_EXT_HEADER[4:0];


/* Wires/Regs  for modules */

/*Module Instantiation*/
bmc_to_binary bmc_to_binary (
	.pll_bmc_clk(pll_bmc_clk),
	.bmc(cc),
	.binary(serial_binary)
);
sop_detect sop_detect(
	.pll_bmc_clk(pll_bmc_clk),
	.data_in(serial_binary),
	.enable(sop_detect_enable),
	.SOP(SOP),
	.done(sop_detect_done)
);
decoder4b5b decoder4b5b_low(
	.data_in_5b(buffer_5b[4:0]),
	.data_out_4b(data_4b[3:0])
);
decoder4b5b decoder4b5b_high(
	.data_in_5b(buffer_5b[9:5]),
	.data_out_4b(data_4b[7:4])
);
decoder4b5b decoder4b5b_CRC_3(
	.data_in_5b(buffer_5b[14:10]),
	.data_out_4b(data_4b[11:8])
);
decoder4b5b decoder4b5b_CRC_4(
	.data_in_5b(buffer_5b[19:15]),
	.data_out_4b(data_4b[15:12])
);
decoder4b5b decoder4b5b_CRC_5(
	.data_in_5b(buffer_5b[24:20]),
	.data_out_4b(data_4b[19:16])
);
decoder4b5b decoder4b5b_CRC_6(
	.data_in_5b(buffer_5b[29:25]),
	.data_out_4b(data_4b[23:20])
);
decoder4b5b decoder4b5b_crc_7(
	.data_in_5b(buffer_5b[34:30]),
	.data_out_4b(data_4b[27:24])
);
decoder4b5b decoder4b5b_crc_8(
	.data_in_5b(buffer_5b[39:35]),
	.data_out_4b(data_4b[31:28])
);
wire [3:0] crc_data_in;
wire [31:0] crc_out;
crc_32_4b crc32(
	.data_in(crc_data_in),
	.crc_en(crc_enable),
	.crc_out(crc_out),
	.rst(crc_reset),
	.clk(pll_bmc_clk)
);
/* Next State Logic */
always@(*) begin
	next_state = RECEIVE_PREAMBLE;
	case(state)
	RECEIVE_PREAMBLE: begin
		if(preamble_done )
			next_state = RECEIVE_SOP;
		else
			next_state = RECEIVE_PREAMBLE;
	end
	RECEIVE_SOP: begin
		if(sop_detect_done && posedge_pll_bmc_clk) // Se están contaminando los datos?
			case(SOP)
				`CABLE_RESET: next_state = (RECEIVE_DETECT[6] == 1) ?  WRITE_RX_BUF_FRAME_TYPE : RECEIVE_PREAMBLE;
				`HARD_RESET: next_state = (RECEIVE_DETECT[5] == 1) ?  RECEIVE_MESSAGE_HEADER_LOW: RECEIVE_PREAMBLE;
				`SOP2_DEBUG: next_state = (RECEIVE_DETECT[4] == 1) ?  WRITE_RX_BUF_FRAME_TYPE : RECEIVE_PREAMBLE;
				`SOP1_DEBUG: next_state = (RECEIVE_DETECT[3] == 1) ?  WRITE_RX_BUF_FRAME_TYPE : RECEIVE_PREAMBLE;
				`SOP2: next_state = (RECEIVE_DETECT[2] == 1) ?  WRITE_RX_BUF_FRAME_TYPE : RECEIVE_PREAMBLE;
				`SOP1: next_state = (RECEIVE_DETECT[1] == 1) ?  WRITE_RX_BUF_FRAME_TYPE : RECEIVE_PREAMBLE;
				`SOP: next_state = (RECEIVE_DETECT[0] == 1) ?  WRITE_RX_BUF_FRAME_TYPE : RECEIVE_PREAMBLE;
				default: next_state = RECEIVE_PREAMBLE;
			endcase
		else
			next_state = RECEIVE_SOP;
	end
	WRITE_RX_BUF_FRAME_TYPE: next_state = RECEIVE_MESSAGE_HEADER_LOW;
	RECEIVE_MESSAGE_HEADER_LOW: begin
		if(posedge_pll_bmc_clk && buffer_5b_counter == 9)
			next_state = WRITE_RX_BUF_HEADER_BYTE_0;
		else
			next_state = RECEIVE_MESSAGE_HEADER_LOW;
	end
	WRITE_RX_BUF_HEADER_BYTE_0: next_state = RECEIVE_MESSAGE_HEADER_HIGH;
	RECEIVE_MESSAGE_HEADER_HIGH: begin
		if(posedge_pll_bmc_clk && buffer_5b_counter == 9)
			next_state = WRITE_RX_BUF_HEADER_BYTE_1;
		else
			next_state = RECEIVE_MESSAGE_HEADER_HIGH;
	end	
	WRITE_RX_BUF_HEADER_BYTE_1: next_state =PARSE_MESSAGE_HEADER;
	PARSE_MESSAGE_HEADER: begin
		if(Extended)
			next_state = RECEIVE_EXT_MESSAGE_HEADER_LOW;
		else
			next_state = RECEIVE_BYTE;
	end
	RECEIVE_EXT_MESSAGE_HEADER_LOW: begin
		if(posedge_pll_bmc_clk && buffer_5b_counter == 9)
			next_state = WRITE_EXT_MESSAGE_HEADER_LOW;
		else
			next_state = RECEIVE_EXT_MESSAGE_HEADER_LOW;
	end
	WRITE_EXT_MESSAGE_HEADER_LOW: next_state = RECEIVE_EXT_MESSAGE_HEADER_HIGH;
	RECEIVE_EXT_MESSAGE_HEADER_HIGH: begin
		if(posedge_pll_bmc_clk && buffer_5b_counter == 9)
			next_state = WRITE_EXT_MESSAGE_HEADER_HIGH;
		else
			next_state = RECEIVE_EXT_MESSAGE_HEADER_HIGH;
	end	
	WRITE_EXT_MESSAGE_HEADER_HIGH: next_state = PARSE_EXT_MESSAGE_HEADER;
	PARSE_EXT_MESSAGE_HEADER: begin
		if(~Chunked)
			next_state = RECEIVE_PREAMBLE;
		else
			next_state = RECEIVE_BYTE;
	end
	RECEIVE_BYTE: begin
		if(posedge_pll_bmc_clk && buffer_5b_counter ==9)
			next_state = WRITE_BYTE;
		else if(RECEIVE_BYTE_COUNT == Number_of_Byte_Objects)
			next_state = WRITE_BYTE_COUNT;
		else
			next_state = RECEIVE_BYTE;
	end
	WRITE_BYTE: next_state = RECEIVE_BYTE;
	WRITE_BYTE_COUNT: next_state = RECEIVE_CRC;
	RECEIVE_CRC: begin
		if(posedge_pll_bmc_clk && crc_5b_counter == 39)
			next_state = RECEIVE_PREAMBLE;
		else
			next_state = RECEIVE_CRC;
	end
	endcase
	if(rx_buffer_full)
		next_state = RECEIVE_PREAMBLE;
end

/*Next Output logic*/
always@(*) begin
	next_crc_reset = 0;
	next_sop_detect_enable =0;
	next_buffer_5b_counter = 0;
	next_reg_address = reg_address;
	next_write_data = write_data;
	next_write = 0;
	next_RECEIVE_BYTE_COUNT = RECEIVE_BYTE_COUNT;
	next_buffer_5b = buffer_5b;
	next_crc_5b_counter = 0;
	next_message_received_from_phy = 0;
	next_SOP = current_SOP;
	next_GoodCRC = 0;
	next_cable_reset = 0;
	next_hard_reset = 0;

	case(state)
	RECEIVE_PREAMBLE: next_RECEIVE_BYTE_COUNT = 0;
	RECEIVE_SOP: begin
		next_crc_reset = 1;
		next_sop_detect_enable = 1;
		if(sop_detect_done && posedge_pll_bmc_clk)
			next_SOP = SOP;
	end
	WRITE_RX_BUF_FRAME_TYPE: begin
		next_sop_detect_enable = 1;
		next_reg_address = `RX_BUF_FRAME_TYPE;
		next_write_data = SOP;
		next_write = 1;
	end
	RECEIVE_MESSAGE_HEADER_LOW: begin
		next_buffer_5b_counter = buffer_5b_counter;
		next_buffer_5b[buffer_5b_counter] = serial_binary;
		if(posedge_pll_bmc_clk) begin
			next_buffer_5b[buffer_5b_counter] = buffer_5b[buffer_5b_counter];
			next_buffer_5b_counter = buffer_5b_counter +1;
			if(buffer_5b_counter == 9)
				next_buffer_5b_counter = 0;
		end
	end
	WRITE_RX_BUF_HEADER_BYTE_0: begin
		next_reg_address = `RX_BUF_HEADER_BYTE_0;
		next_write_data = byte_to_write;
		next_write = 1;
	end
	RECEIVE_MESSAGE_HEADER_HIGH: begin
		next_buffer_5b_counter = buffer_5b_counter;
		next_buffer_5b[buffer_5b_counter] = serial_binary;
		if(posedge_pll_bmc_clk) begin
			next_buffer_5b[buffer_5b_counter] = buffer_5b[buffer_5b_counter];
			next_buffer_5b_counter = buffer_5b_counter +1;
			if(buffer_5b_counter == 9)
				next_buffer_5b_counter = 0;
		end
	end
	WRITE_RX_BUF_HEADER_BYTE_1: begin
		next_reg_address = `RX_BUF_HEADER_BYTE_1;
		next_write_data = byte_to_write;
		next_write = 1;
	end
	//PARSE_MESSAGE_HEADER:
	RECEIVE_EXT_MESSAGE_HEADER_LOW: begin
		next_buffer_5b_counter = buffer_5b_counter;
		next_buffer_5b[buffer_5b_counter] = serial_binary;
		if(posedge_pll_bmc_clk) begin
			next_buffer_5b[buffer_5b_counter] = buffer_5b[buffer_5b_counter];
			next_buffer_5b_counter = buffer_5b_counter +1;
			if(buffer_5b_counter == 9)
				next_buffer_5b_counter = 0;
		end
	end
	WRITE_EXT_MESSAGE_HEADER_LOW: begin
		next_RECEIVE_BYTE_COUNT = RECEIVE_BYTE_COUNT +1;
		next_reg_address = reg_address +1;
		next_write_data = byte_to_write;
		next_write = 1;
	end
	RECEIVE_EXT_MESSAGE_HEADER_HIGH: begin 
		next_buffer_5b_counter = buffer_5b_counter;
		next_buffer_5b[buffer_5b_counter] = serial_binary;
		if(posedge_pll_bmc_clk) begin
			next_buffer_5b[buffer_5b_counter] = buffer_5b[buffer_5b_counter];
			next_buffer_5b_counter = buffer_5b_counter +1;
			if(buffer_5b_counter == 9)
				next_buffer_5b_counter = 0;
		end
	end
	WRITE_EXT_MESSAGE_HEADER_HIGH: begin
		next_RECEIVE_BYTE_COUNT = RECEIVE_BYTE_COUNT +1;
		next_reg_address = reg_address +1;
		next_write_data = byte_to_write;
		next_write = 1;
	end
	//PARSE_EXT_MESSAGE_HEADER:
	RECEIVE_BYTE: begin
		next_buffer_5b_counter = buffer_5b_counter;
		next_buffer_5b[buffer_5b_counter] = serial_binary;
		if(posedge_pll_bmc_clk) begin
			next_buffer_5b[buffer_5b_counter] = buffer_5b[buffer_5b_counter];
			next_buffer_5b_counter = buffer_5b_counter +1;
			if(buffer_5b_counter == 9)
				next_buffer_5b_counter = 0;
		end
	end
	WRITE_BYTE: begin
		next_RECEIVE_BYTE_COUNT = RECEIVE_BYTE_COUNT +1;
		next_reg_address = reg_address +1;
		next_write_data = byte_to_write;
		next_write = 1;	
	end
	WRITE_BYTE_COUNT: begin
		next_reg_address = `RECEIVE_BYTE_COUNT;
		next_write_data = RECEIVE_BYTE_COUNT + 3;
		next_write = 1;		
	end
	RECEIVE_CRC: begin
		next_crc_5b_counter = crc_5b_counter;
		next_buffer_5b[crc_5b_counter] = serial_binary;
		if(posedge_pll_bmc_clk) begin
			next_buffer_5b[crc_5b_counter] = buffer_5b[crc_5b_counter];
			next_crc_5b_counter = crc_5b_counter +1;
			if(crc_5b_counter == 39) begin
				next_crc_5b_counter = 0;
				if(Calculated_CRC == Received_CRC) begin
					next_message_received_from_phy = 1;
					if(current_SOP == `CABLE_RESET) begin
						next_message_received_from_phy = 0;
						next_cable_reset = 1;
					end
					else if(current_SOP == `HARD_RESET) begin
						next_message_received_from_phy = 0;
						next_hard_reset = 1;
					end
					else if (Number_of_Data_Objects == 0 && Message_Type == 5'b00001) //Message_Type = GoodCRC
						next_GoodCRC = 1;
						if(expect_GoodCRC)
							next_message_received_from_phy = 0;
						else if (~expect_GoodCRC)
							next_message_received_from_phy = 1;
						
				end
			end
		end
	end
	endcase
end
always@(posedge clk) begin
	pll_bmc_clk_d<= pll_bmc_clk;
	if(~enable)
		state <= RECEIVE_PREAMBLE;
	else
		state <= next_state;
		
	crc_reset <= next_crc_reset;
	sop_detect_enable <= next_sop_detect_enable;
	buffer_5b_counter <= next_buffer_5b_counter;
	reg_address <= next_reg_address;
	write_data <= next_write_data;
	write <= next_write;
	RECEIVE_BYTE_COUNT <= next_RECEIVE_BYTE_COUNT;
	buffer_5b <= next_buffer_5b;
	crc_5b_counter <= next_crc_5b_counter;
	message_received_from_phy <= next_message_received_from_phy;
	GoodCRC <= next_GoodCRC;
	hard_reset <= next_hard_reset;
	cable_reset <= next_cable_reset;
end
endmodule
