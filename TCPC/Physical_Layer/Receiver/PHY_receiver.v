`include "../../../include/K_code_defines.vh"
`include "../../../include/SOP_defines.vh"
/*
`include "4b5b_decoder/4b5b_decoder.v"
`include "BMC_Decoder/PLL/BMC_to_bin.v"
`include "SOP_Detect/sop_detect.v"
`include "CRC/CRC_32_outputlogic_4b.v"
*/
module phy_receiver(
	input pll_bmc_clk,
	input bmc,
	input enable,
	input rd_dp,
	input preamble_done,
	input expect_GoodCRC,
	output reg write,
	output reg [7:0] write_address,
	output reg [7:0] write_data,
	output reg message_received_from_phy,	
);
/* State regs */
reg [7:0] state;
reg [7:0] next_state;

/*One Hot Encoding*/
parameter RECEIVE_PREAMBLE =           8'b00000001;
parameter RECEIVE_SOP =                8'b00000010; 
parameter RECEIVE_MESSAGE_HEADER =     8'b00000100;
parameter RECEIVE_EXT_MESSAGE_HEADER = 8'b00001000;
parameter RECEIVE_MESSAGE =            8'b00010000;
parameter RECEIVE_EXT_MESSAGE =        8'b00100000;
parameter RECEIVE_CRC =                8'b01000000;
parameter RECEIVE_EOP =                8'b10000000;

/*Internal Values*/
reg [2:0] SOP;
reg [15:0] message_header;
reg [15:0] extended_message_header;
reg [4:0] message_header_counter;
reg [4:0] buffer_5b;
wire [3:0] data_4b;
reg [2:0] buffer_5b_counter;
reg [5:0] message_nibble_counter;
reg [4:0] crc_counter;
reg [8:0] ext_message_nibble_counter;
reg [223:0] message;
reg [2079+16+16:0] ext_message;
reg [31:0] crc;
reg EOP_done;

/*Next values*/
reg [4:0] next_crc_counter;
reg next_crc_enable;
reg next_crc_reset;
reg next_sop_detect_enable;
reg [4:0] next_message_header_counter;
reg [2:0] next_buffer_5b_counter;
reg [4:0] next_buffer_5b;
reg [7:0] next_write_data;
reg next_write;
reg [5:0] next_message_nibble_counter;
reg [15:0]next_message_header;
reg [223:0] next_message;
reg [2079:0] next_ext_message ;
reg [31:0] next_crc;
reg next_done;
reg [8:0] next_ext_message_nibble_counter;
reg next_message_received_from_phy;
reg next_EOP_done;
/*Current Values */

/*Assigns*/
wire [2:0] Number_of_Data_Objects;
wire [5:0] Number_of_Nibbles;
wire [8:0] DATA_SIZE;
wire [9:0] DATA_SIZE_nibbles;

assign Extended = message_header[15];
assign Number_of_Data_Objects = message_header[14:12];
assign Number_of_Nibbles = Number_of_Data_Objects << 3; 
assign Chunked = extended_message_header[15];
assign DATA_SIZE = extended_message_header[8:0];
assign DATA_SIZE_nibbles = DATA_SIZE << 2;
assign crc_data_in = data_4b;
/*Output Assigns */
/* Wires for module interconection */
wire serial_binary;

reg sop_detect_enable;


wire [2:0] sop_detect_SOP;
wire sop_detect_done;

reg crc_enable;
wire [31:0] crc_out;
reg crc_reset;

wire [3:0] crc_data_in;

wire eop_detect_done;
/* Module instantiation */
bmc_to_binary bmc_to_binary (
	.pll_bmc_clk(pll_bmc_clk),
	.bmc(bmc),
	.binary(serial_binary)
);
sop_detect sop_detect(
	.pll_bmc_clk(pll_bmc_clk),
	.data_in(serial_binary),
	.enable(sop_detect_enable),
	.SOP(sop_detect_SOP),
	.done(sop_detect_done)
);
decoder4b5b decoder4b5b(
	.data_in_5b(buffer_5b),
	.data_out_4b(data_4b),
	.error(invalid_5b)
);
crc_32_4b crc32(
	.data_in(crc_data_in),
	.crc_en(crc_enable),
	.crc_out(crc_out),
	.rst(crc_reset),
	.clk(pll_bmc_clk)
);
/*Next State Logic */
always@(*) begin
	next_state = RECEIVE_PREAMBLE;
	case(state)
	RECEIVE_PREAMBLE: begin
		if(preamble_done)
			next_state = RECEIVE_SOP;
		else
			next_state = RECEIVE_PREAMBLE;
	end
	RECEIVE_SOP: begin
		if(sop_detect_done) 
			next_state = RECEIVE_MESSAGE_HEADER; 
		else
			next_state = RECEIVE_SOP; 
	end
	RECEIVE_MESSAGE_HEADER: begin
		if(message_header_counter == 4) begin
			if( Extended == 1) //Extended == 1;
				next_state = RECEIVE_EXT_MESSAGE_HEADER;
			else if(Extended == 0)  //Extended == 0; //Different path if 0
				next_state = RECEIVE_MESSAGE;				
		end
			
		else
			next_state = RECEIVE_MESSAGE_HEADER;
	end
	RECEIVE_EXT_MESSAGE_HEADER: begin
		if(message_header_counter ==3) begin
			if(Chunked)
				next_state = RECEIVE_MESSAGE;
			else if(~Chunked)
				next_state = RECEIVE_EXT_MESSAGE;
		end
	end
	RECEIVE_EXT_MESSAGE: begin
		if(ext_message_nibble_counter == DATA_SIZE_nibbles)
			next_state = RECEIVE_CRC;
		else
			next_state = RECEIVE_EXT_MESSAGE;
	end
	RECEIVE_MESSAGE: begin
		if(message_nibble_counter == Number_of_Nibbles)
			next_state = RECEIVE_CRC;
		else
			next_state = RECEIVE_MESSAGE;
	end
	RECEIVE_CRC: begin
		if(crc_counter == 7 && buffer_5b_counter == 4)
			next_state = RECEIVE_EOP;
		else
			next_state = RECEIVE_CRC;
	end
	RECEIVE_EOP: begin
		if(buffer_5b_counter == 4) 
			next_state = RECEIVE_PREAMBLE; 
		else
			next_state = RECEIVE_EOP; 
	end
	endcase
end
/* Next Output Logic */
always@(*) begin
	next_message_received_from_phy = 0;
	next_crc_counter = crc_counter;
	next_crc_enable = 0;
	next_crc_reset = 0;
	next_sop_detect_enable = 0;
	next_message_header_counter = message_header_counter;
	next_buffer_5b_counter = 0;
	next_buffer_5b = 5'dx;
	next_write_data = 8'dx;
	next_write = 0;
	next_message_nibble_counter = message_nibble_counter;
	next_message_header = message_header;
	next_message = message;
	next_ext_message = ext_message;
	next_crc =crc;
	next_done = 0;
	next_EOP_done = 0;
	case(state)
	RECEIVE_PREAMBLE: begin
	end
	RECEIVE_SOP: begin 
		next_crc_reset = 1;
		next_sop_detect_enable = 1;
		if(sop_detect_done) begin
			next_sop_detect_enable = 0;
			//next_buffer_5b_counter = buffer_5b_counter +1;
			next_buffer_5b =  buffer_5b >> 1;
			next_buffer_5b[4] = serial_binary;
		
		end
	end
	RECEIVE_MESSAGE_HEADER: begin
		next_buffer_5b_counter = buffer_5b_counter +1;
		next_buffer_5b =  buffer_5b >> 1;
		next_buffer_5b[4] = serial_binary;
		if(buffer_5b_counter == 4) begin
			next_buffer_5b_counter = 0;
			next_message = message >> 4; //Revisar aca
			next_message[15:12] = data_4b;//Reparar esto
			next_message_header_counter =  message_header_counter + 1;
		end
		if(buffer_5b_counter == 3)
			next_crc_enable =1;
	end
	RECEIVE_MESSAGE: begin
		next_buffer_5b_counter = buffer_5b_counter +1;
		next_buffer_5b = buffer_5b >> 1;
		next_buffer_5b[4] = serial_binary;
		if(buffer_5b_counter == 4) begin
			next_buffer_5b_counter = 0;
			next_message = message >> 4;
			next_message[223:220] = data_4b;
			next_message_nibble_counter = message_nibble_counter +1 ;
		end
		if(buffer_5b_counter == 3)
			next_crc_enable =1;
	end
	RECEIVE_CRC: begin
		next_buffer_5b_counter = buffer_5b_counter +1;
		next_buffer_5b = buffer_5b >> 1;
		next_buffer_5b[4] = serial_binary;		
		if(buffer_5b_counter == 4) begin
			next_buffer_5b_counter = 0;
			next_crc = crc >> 4;
			next_crc[31:28] = data_4b;
			next_crc_counter = crc_counter +1;
			if(crc_counter == 7)
				next_done =1;
		end	
	end
	RECEIVE_EXT_MESSAGE_HEADER: begin
		next_message_nibble_counter = -4;
		next_buffer_5b_counter = buffer_5b_counter +1;
		next_buffer_5b =  buffer_5b >> 1;
		next_buffer_5b[4] = serial_binary;
		if(buffer_5b_counter == 4) begin
			next_buffer_5b_counter = 0;
			next_message = message_header >> 4; //Revisar aca
			next_message[15:12] = data_4b;
			next_crc_enable =1;
			next_message_header_counter =  message_header_counter + 1;
		end		
	end	
	RECEIVE_EXT_MESSAGE: begin
		next_buffer_5b_counter = buffer_5b_counter +1;
		next_buffer_5b = buffer_5b >> 1;
		next_buffer_5b[4] = serial_binary;
		if(buffer_5b_counter == 4) begin
			next_buffer_5b_counter = 0;
			next_ext_message = ext_message >> 4;
			next_ext_message[2079:2076] = data_4b;
			next_ext_message_nibble_counter = ext_message_nibble_counter +1 ;
		end
	end
	RECEIVE_EOP: begin
		next_buffer_5b_counter = buffer_5b_counter+1;
		next_buffer_5b = buffer_5b >> 1;
		next_buffer_5b[4] = serial_binary;
		if(buffer_5b_counter ==3)
			next_EOP_done = 1;
		if(buffer_5b_counter == 4) begin
			next_buffer_5b_counter = 0;
			if(buffer_5b == 5'b01101 && crc == crc_out )
				next_message_received_from_phy = 1;
		end
	end
	endcase
end
/*CLK LOGIC */
always@(posedge pll_bmc_clk) begin
	if(~enable) begin
		state <= RECEIVE_PREAMBLE;
		crc_counter <= 0;
		message_header_counter <= 0;
		message_nibble_counter <= 0;
		ext_message_nibble_counter <= 0;
		
	end
	else begin
		state <= next_state;
		crc_counter <= next_crc_counter;
		message_header_counter <= next_message_header_counter;
		message_nibble_counter <= next_message_nibble_counter;
		ext_message_nibble_counter <= next_ext_message_nibble_counter;
	end
	
	message_header <= next_message_header;
	EOP_done <= next_EOP_done;
	message_received_from_phy <= next_message_received_from_phy;
	ext_message <= next_ext_message;
	crc_enable <= next_crc_enable;
	crc_reset <= next_crc_reset;
	sop_detect_enable <= next_sop_detect_enable;
	buffer_5b_counter <= next_buffer_5b_counter;
	buffer_5b <= next_buffer_5b;
	write_data <= next_write_data;
	write <= next_write;
	message <= next_message;
	crc <= next_crc;
end
initial $dumpvars;
endmodule
