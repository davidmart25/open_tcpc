`include "sop_detect.v"
`include "K_code_defines.vh"
`include "SOP_defines.vh"
module testbench;

reg pll_bmc_clk;
reg enable;
reg [20*7-1:0] SOPS;
reg [19:0] HARD_RESET;
reg [19:0] CABLE_RESET;
reg [19:0] SOP;
reg [19:0] SOP1;
reg [19:0] SOP1_DEBUG;
reg [19:0] SOP2;
reg [19:0] SOP2_DEBUG;

assign data_in  = SOPS[0];
sop_detect test(
	.pll_bmc_clk(pll_bmc_clk),
	.data_in(data_in),
	.enable(enable),
	.done(done)
);
initial begin
	enable = 0;
	pll_bmc_clk = 0;
	
	CABLE_RESET = {`SYNC_3,`RST_1,`SYNC_1,`RST_1};
	HARD_RESET = {`RST_2,`RST_1, `RST_1, `RST_1} ;
	SOP = {`SYNC_2,`SYNC_1,`SYNC_1,`SYNC_1};
	SOP1 = {`SYNC_3,`SYNC_3,`SYNC_1,`SYNC_1};
	SOP1_DEBUG = {`SYNC_3,`RST_2, `RST_2,`SYNC_1};
	SOP2 = {`SYNC_3, `SYNC_1, `SYNC_3,`SYNC_1};
	SOP2_DEBUG = {`SYNC_2,`SYNC_3,`RST_2,`SYNC_1};
	SOPS = {SOP2_DEBUG,SOP2,SOP1_DEBUG,SOP1,SOP,HARD_RESET,CABLE_RESET};
	forever #10 pll_bmc_clk  = ~pll_bmc_clk;
end

initial #11 enable = 1;

always @(posedge pll_bmc_clk) begin
	if(enable)
		SOPS = SOPS >> 1;
end	

endmodule