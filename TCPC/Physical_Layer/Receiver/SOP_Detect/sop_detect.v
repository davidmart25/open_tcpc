//`include "../../../include/K_code_defines.vh"
//`include "../../../include/SOP_defines.vh"

module sop_detect(
	input pll_bmc_clk,
	input data_in,
	input enable,
	output reg [2:0] SOP,
	output reg done
);

/* State Regs */
reg [3:0] state;
reg [3:0] next_state;

/* One Hot Encoding */
parameter K_CODE_1 = 4'b0001;
parameter K_CODE_2 = 4'b0010;
parameter K_CODE_3 = 4'b0100;
parameter K_CODE_4 = 4'b1000;

/* Next Values */ 
reg [2:0] next_bit_count;
reg [4:0] next_k_code_1;
reg [4:0] next_k_code_2;
reg [4:0] next_k_code_3;
reg [4:0] next_k_code_4;
reg [2:0] next_SOP;
reg next_done;

/* Internal Values */
reg [2:0] bit_count;
reg [4:0] k_code_1;
reg [4:0] k_code_2;
reg [4:0] k_code_3;
reg [4:0] k_code_4;

/* Next State Logic */
always @(*) begin
	next_state = K_CODE_1;
	case(state)
	K_CODE_1: begin
		if(bit_count == 4)
			next_state = K_CODE_2;
		else
			next_state = K_CODE_1;
	end
	K_CODE_2: begin
		if(bit_count == 4)
			next_state = K_CODE_3;
		else
			next_state = K_CODE_2;
	end
	K_CODE_3: begin
		if(bit_count == 4)
			next_state = K_CODE_4;
		else
			next_state = K_CODE_3;
	end
	K_CODE_4: begin
		if(bit_count ==4)
			next_state = K_CODE_1;
		else
			next_state = K_CODE_4;
	end
	endcase
	if(~enable)
		next_state = K_CODE_1;
end

/* Next Output Logic */
always @(*) begin
	next_bit_count = bit_count+1;
	next_k_code_1 = k_code_1;
	next_k_code_2 = k_code_2;
	next_k_code_3 = k_code_3;
	next_k_code_4 = k_code_4;
	next_SOP = SOP;
	next_done = 0;
	if(bit_count == 4)
		next_bit_count = 0;
		
	case(state)
	K_CODE_1: begin
		next_k_code_1 = k_code_1 >> 1;
		next_k_code_1[4] = data_in;
	end
	K_CODE_2: begin
		next_k_code_2 = k_code_2 >> 1;
		next_k_code_2[4] = data_in;
	end
	K_CODE_3: begin
		next_k_code_3 = k_code_3 >> 1;
		next_k_code_3[4] = data_in;
	end
	K_CODE_4: begin
		next_k_code_4 = k_code_4 >> 1;
		next_k_code_4[4] = data_in;
		
		if(bit_count == 4) begin
			next_done = 1;
			if ((k_code_2 == `SYNC_1 && k_code_3 == `SYNC_1 && next_k_code_4 == `SYNC_2 ) ||
			(k_code_1 == `SYNC_1 && k_code_3 == `SYNC_1 && next_k_code_4 == `SYNC_2) ||
			(k_code_1 == `SYNC_1 && k_code_2 == `SYNC_1 &&  next_k_code_4 == `SYNC_2) ||
			(k_code_1 == `SYNC_1 && k_code_2 == `SYNC_1 && k_code_3 == `SYNC_1))
				next_SOP = `SOP;
			else if ((k_code_2 == `SYNC_1  && k_code_3 == `SYNC_3  && next_k_code_4 == `SYNC_3  ) ||
			(k_code_1 == `SYNC_1  && k_code_3 == `SYNC_3  && next_k_code_4 == `SYNC_3 ) ||
			(k_code_1 == `SYNC_1  && k_code_2 == `SYNC_1  &&  next_k_code_4 == `SYNC_3 ) ||
			(k_code_1 == `SYNC_1  && k_code_2 == `SYNC_1  && k_code_3 == `SYNC_3 ))
				next_SOP = `SOP1;
			else if ((k_code_2 == `SYNC_3 && k_code_3 == `SYNC_1  && next_k_code_4 == `SYNC_3  ) ||
			(k_code_1 == `SYNC_1 && k_code_3 == `SYNC_1  && next_k_code_4 == `SYNC_3 ) ||
			(k_code_1 == `SYNC_1  && k_code_2 == `SYNC_3  &&  next_k_code_4 == `SYNC_3 ) ||
			(k_code_1 == `SYNC_1 && k_code_2 == `SYNC_3  && k_code_3 == `SYNC_1 ))
				next_SOP = `SOP2;
			else if ((k_code_2 == `RST_2  && k_code_3 == `RST_2  && next_k_code_4 == `SYNC_3  ) ||
			(k_code_1 == `SYNC_1  && k_code_3 == `RST_2  && next_k_code_4 == `SYNC_3 ) ||
			(k_code_1 == `SYNC_1  && k_code_2 == `RST_2  &&  next_k_code_4 == `SYNC_3 ) ||
			(k_code_1 == `SYNC_1  && k_code_2 == `RST_2  && k_code_3 == `RST_2 ))
				next_SOP = `SOP1_DEBUG;
			else if ((k_code_2 == `RST_2  && k_code_3 == `SYNC_3  && next_k_code_4 == `SYNC_2  ) ||
			(k_code_1 == `SYNC_1  && k_code_3 == `SYNC_3  && next_k_code_4 == `SYNC_2 ) ||
			(k_code_1 == `SYNC_1  && k_code_2 == `RST_2  &&  next_k_code_4 == `SYNC_2 ) ||
			(k_code_1 == `SYNC_1  && k_code_2 == `RST_2  && k_code_3 == `SYNC_3 ))
				next_SOP = `SOP2_DEBUG;
			else if ((k_code_2 == `SYNC_1  && k_code_3 == `RST_1  && next_k_code_4 == `SYNC_3  ) ||
			(k_code_1 == `RST_1  && k_code_3 == `RST_1  && next_k_code_4 == `SYNC_3 ) ||
			(k_code_1 == `RST_1  && k_code_2 == `SYNC_1  &&  next_k_code_4 == `SYNC_3 ) ||
			(k_code_1 == `RST_1  && k_code_2 == `SYNC_1  && k_code_3 == `RST_1 ))
				next_SOP = `CABLE_RESET;
			else if ((k_code_2 == `RST_1  && k_code_3 == `RST_1  && next_k_code_4 == `RST_2  ) ||
			(k_code_1 == `RST_1  && k_code_3 == `RST_1  && next_k_code_4 == `RST_2 ) ||
			(k_code_1 == `RST_1  && k_code_2 == `RST_1  &&  next_k_code_4 == `RST_2 ) ||
			(k_code_1 == `RST_1  && k_code_2 == `RST_1  && k_code_3 == `RST_1 ))
				next_SOP = `HARD_RESET;	
			else
				next_SOP = `NO_SOP;
		end
	end
	endcase
	if(~enable) begin
		next_bit_count = 0;
		next_done = 0;
	end
	
end

/* CLK Logic */
always @(posedge pll_bmc_clk) begin
	if(~enable)
		state <= K_CODE_1;
	else
		state <= next_state;
		
	done <= next_done;
	bit_count <= next_bit_count;
	k_code_1 <= next_k_code_1;
	k_code_2 <= next_k_code_2;
	k_code_3 <= next_k_code_3;
	k_code_4 <= next_k_code_4;
	SOP <= next_SOP;
end
initial begin
	$dumpfile("SOPS.vcd"); //non synth code, simulation only
	$dumpvars;
end
endmodule
 
