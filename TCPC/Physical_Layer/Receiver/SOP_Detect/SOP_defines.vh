`ifndef _sopdefines_

`define _sopdefines_

`define CABLE_RESET 3'b000
`define HARD_RESET 3'b001
`define SOP 3'b010
`define SOP1 3'b011
`define SOP1_DEBUG 3'b100
`define SOP2 3'b101
`define SOP2_DEBUG 3'b110
`define NO_SOP 3'b111


`endif //_sopdefines_
