`ifndef _kcodedefines_

`define _kcodedefines_

`define SYNC_1 5'b11000
`define SYNC_2 5'b10001
`define RST_1  5'b00111
`define RST_2  5'b11001
`define EOP    5'b01101
`define SYNC_3 5'b00110

`endif //_kcodedefines_
