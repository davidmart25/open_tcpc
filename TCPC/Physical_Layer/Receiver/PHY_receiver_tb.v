`include "PHY_receiver_tb.v"
`include "../Transmiter/BMC/bmc_encoder.v"
`include "../Transmiter/4b5b_Enconder/4b5b_encoder.v"
module testbench;
	reg bmc_clk;
	reg pll_bmc_clk;
	reg bmc;
	reg phy_rec_enable;
	reg preamble_done;
	reg data_in;
	reg data_in_4b;
	reg [15:0] message_header;
	reg [5:0] 4b_buffer;
	
	wire [7:0] write_address;
	wire [7:0] write_data;
	wire [4:0] data_out_5b;
	
phy_receiver test(
	.pll_bmc_clk(pll_bmc_clk),
	.bmc(bmc),
	.enable(phy_rec_enable),
	.preamble_done(preamble_done),
	.write(write),
	.write_address(write_address),
	.write_data(wirte_data),
	.message_received_from_phy(message_received_from_phy)
);

bmc_encoder bmc_encoder (
	.enable(bmc_encoder_enable),
	.data_in(data_in),
	.bmc_clk(bmc_clk),
	.bmc(bmc),
);

encoder4b5b  encoder_4b_5b (
	.data_in_4b(data_in_4b),
	.data_out_5b(data_out_5b)
);

initial begin
	bmc_clk = 0;
	phy_rec_enable =0;
	data_in = 0;
	#4 phy_rec_enable = 1;
	bmc_encoder_enable = 1;
	repeat (64) begin
		#2 data_in = ~data_in;
	end
	preamble_done =1;
	//Sending the message_header //
	message_header = 0;
	message_header[15] = 0; //Not extended
	message_header[14:12] = 3; //3 32 bit messages will be sent
	
end

initial begin
forever #1 bmc_clk = ~bmc_clk;
end
always@(posedge bmc_clk) begin
	pll_bmc_clk = ~pll_bmc_clk;
end

always@(posedge pll_bmc_clk) begin
	if(preamble_done)
		data_in = 
end

endmodule

