module testbench;
wire [7:0] data_in_8b;
reg crc8_en;
reg crc4_en;
reg clk;
reg rst1;
reg rst2;
wire [3:0] data_in_4b_low;
wire [3:0] data_in_4b_high;
reg [3:0] data_in_4b;
assign data_in_4b_low = 4'b1010;
assign data_in_4b_high = 4'b0101;

assign data_in_8b = {data_in_4b_high, data_in_4b_low};
wire [31:0] crc8_out, crc4_out;
crc crc8(
	.data_in(data_in_8b),
	.crc_en(crc8_en),
	.crc_out(crc8_out),
	.rst(rst1),
	.clk(clk)
	
);
crc_32_4b crc4(
  .data_in(data_in_4b),
  .crc_en(crc4_en),
  .crc_out(crc4_out),
  .rst(rst2),
  .clk(clk)
);
initial begin
	clk = 0;
	crc8_en = 0;
	crc4_en = 0;
	rst1 = 0;
	rst2 = 0;
	#1 rst1= 1;
	rst2 = 1;
	#1 rst1 = 0;
	rst2 = 0;
	crc8_en = 1;
	#1 clk = 1;
	#1 clk = 0;
	crc8_en = 0;
	crc4_en = 1;
	data_in_4b = data_in_4b_high;
	#1 clk = 1;
	#1 clk = 0;
	data_in_4b = data_in_4b_low;
	#1 clk = 1;
	#1 clk = 0;
	crc4_en = 0;
end
initial begin
	$dumpfile("CRC.vcd");
	$dumpvars;
end
endmodule