// Width = 32 //
// Poynomial = 0x04C11DB7 = 0000 0100 1100 0001 0001 1101 1011 0111//
// Initial Values = 0xFFFFFFFF // 

module serial_crc_32 (
	input clk,
	input reset,
	input enable,
	input init,
	input data_in,
	
	output  [31:0] crc_out
);

reg [31:0] lfsr;
assign crc_out = lfsr;

always @(posedge clk) begin
	if (reset)
		lfsr <= 32'hFFFFFFFF;
	else if(enable) begin
		if(init) begin
			lfsr <= 32'hFFFFFFFF;
		end
		else begin
			lfsr[0] <= lfsr[31]^data_in;
			lfsr[1] <= lfsr[30]^data_in^lfsr[31];
			lfsr[2] <= lfsr[29]^data_in^lfsr[31];
			lfsr[3] <= lfsr[28];
			
			lfsr[4] <= lfsr[27]^data_in^lfsr[31];
			lfsr[5] <= lfsr[26]^data_in^lfsr[31];
			lfsr[6] <= lfsr[25];
			lfsr[7] <= lfsr[24]^data_in^lfsr[31];
			
			lfsr[8] <= lfsr[23]^data_in^lfsr[31];
			lfsr[9] <= lfsr[22];
			lfsr[10] <= lfsr[21]^data_in^lfsr[31];
			lfsr[11] <= lfsr[20]^data_in^lfsr[31];
			
			lfsr[12] <= lfsr[19]^data_in^lfsr[31];
			lfsr[13] <= lfsr[18];
			lfsr[14] <= lfsr[17];
			lfsr[15] <= lfsr[16];
			
			lfsr[16] <= lfsr[15]]^data_in^lfsr[31];
			lfsr[17] <= lfsr[14];
			lfsr[18] <= lfsr[13];
			lfsr[19] <= lfsr[12];
			
			lfsr[20] <= lfsr[11];
			lfsr[21] <= lfsr[10];
			lfsr[22] <= lfsr[9]]^data_in^lfsr[31];
			lfsr[23] <= lfsr[8]]^data_in^lfsr[31];
			
			lfsr[24] <= lfsr[7];
			lfsr[25] <= lfsr[6];
			lfsr[26] <= lfsr[5]]^data_in^lfsr[31];
			lfsr[27] <= lfsr[4];
			
			lfsr[28] <= lfsr[3];
			lfsr[29] <= lfsr[2];
			lfsr[30] <= lfsr[1];
			lfsr[31] <= lfsr[0];
		end
	end
end
endmodule

