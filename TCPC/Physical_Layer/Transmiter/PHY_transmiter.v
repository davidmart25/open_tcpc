//`include "4b5b_Encoder/4b5b_encoder.v"
//`include "BMC/bmc_encoder.v"
//`include "CRC/CRC_32_8b_outputlogic.v"
//`include "../../../include/K_code_defines.vh"
//`include "../../../include/registers.vh"
//`include "../../../include/SOP_defines.vh"
module phy_transmiter(
	input clk,
	input bmc_clk,
	input [7:0] data_in_from_reg,
	input load_done,
	input enable,
	input send_goodCRC,
	input send_hard_reset,
	input send_cable_reset,
	input [7:0] RX_BUF_FRAME_TYPE,
	input [15:0] RX_BUF_HEADER,
	input [7:0] TRANSMIT,
	input [7:0] MESSAGE_HEADER_INFO,
	output reg [7:0] reg_address,
	output reg load,
	output cc_hiz,
	output cc,
	output reg done
);
reg pll_bmc_clk;
reg [19:0] SOP;
always@(*) begin
	case(message_type)
	SOP_MSG: begin
		case(TRANSMIT[2:0])
		`CABLE_RESET: SOP = {`SYNC_3,`RST_1,`SYNC_1,`RST_1};
		`HARD_RESET: SOP = {`RST_2,`RST_1,`RST_1,`RST_1};
		`SOP: SOP = {`SYNC_2,`SYNC_1,`SYNC_1,`SYNC_1};
		`SOP1: SOP = {`SYNC_3,`SYNC_3,`SYNC_1,`SYNC_1};
		`SOP1_DEBUG: SOP = {`SYNC_3, `RST_2, `RST_2, `SYNC_1} ;
		`SOP2: SOP = {`SYNC_3,`SYNC_1,`SYNC_3,`SYNC_1};
		`SOP2_DEBUG: SOP = {`SYNC_2,`SYNC_3,`RST_2,`SYNC_2};
		//3'b111: SOP =  // 
		endcase
	end
	HARD_RESET_MSG: SOP = {`RST_2,`RST_1,`RST_1,`RST_1};
	CABLE_RESET_MSG: SOP = {`SYNC_3,`RST_1,`SYNC_1,`RST_1};
	GOODCRC_MSG: begin
		case(RX_BUF_FRAME_TYPE[2:0])
		`CABLE_RESET: SOP = {`SYNC_3,`RST_1,`SYNC_1,`RST_1};
		`HARD_RESET: SOP = {`RST_2,`RST_1,`RST_1,`RST_1};
		`SOP: SOP = {`SYNC_2,`SYNC_1,`SYNC_1,`SYNC_1};
		`SOP1: SOP = {`SYNC_3,`SYNC_3,`SYNC_1,`SYNC_1};
		`SOP1_DEBUG: SOP = {`SYNC_3, `RST_2, `RST_2, `SYNC_1} ;
		`SOP2: SOP = {`SYNC_3,`SYNC_1,`SYNC_3,`SYNC_1};
		`SOP2_DEBUG: SOP = {`SYNC_2,`SYNC_3,`RST_2,`SYNC_2};	
		endcase
	end
	endcase
end

always@(*) begin
	GoodCRC_Message_Header[15:12] = 0;
	GoodCRC_Message_Header[11:9] = MessageID;
	GoodCRC_Message_Header[4:0] = 1; // MessageType == GoodCRC;	
	GoodCRC_Message_Header[7:0]  = MESSAGE_HEADER_INFO[2:0];
	GoodCRC_Message_Header[5] = 0;
	case(RX_BUF_FRAME_TYPE[2:0])
	`SOP: begin 
		GoodCRC_Message_Header[8] = MESSAGE_HEADER_INFO[0];
		GoodCRC_Message_Header[5] = MESSAGE_HEADER_INFO[3];
	end
	`SOP1: GoodCRC_Message_Header[8] = MESSAGE_HEADER_INFO[4];
	`SOP1_DEBUG: GoodCRC_Message_Header[8] = MESSAGE_HEADER_INFO[4];
	`SOP2: GoodCRC_Message_Header[8] = MESSAGE_HEADER_INFO[4];
	`SOP2_DEBUG: GoodCRC_Message_Header[8] = MESSAGE_HEADER_INFO[4];
	endcase
	
end

always@(posedge bmc_clk) begin
	if(~enable)
		pll_bmc_clk <= 0;
	else
		pll_bmc_clk <= ~pll_bmc_clk;
end

/*State Registers */
reg [8:0] next_state;
reg [8:0] state;

/* One Hot encoding */
parameter SEND_PREAMBLE =             9'b000000001;
parameter SEND_SOP =                  9'b000000010;
parameter LOAD_TRANSMIT_BYTE_COUNT =  9'b000000100;
parameter LOAD_BYTE =                 9'b000001000;
parameter CALCULATE_CRC =             9'b000010000;
parameter SEND_MESSAGE =              9'b000100000;
parameter SEND_CRC =                  9'b001000000;
parameter SEND_EOP =                  9'b010000000;
parameter SEND_GOODCRC =              9'b100000000;

/* Other Parameters */
parameter HARD_RESET_MSG = 2'b00;
parameter CABLE_RESET_MSG = 2'b01;
parameter GOODCRC_MSG = 2'b10;
parameter SOP_MSG = 2'b11;	
/* Internal Values */
wire [9:0] encoded5b;
reg [7:0] byte_to_send;
wire [31:0] crc_out;
reg [31:0] crc_rewired;
reg bmc_data_in;
reg [5:0] preamble_counter;
reg [19:0] SOP_transmit;
reg [4:0] SOP_transmit_counter;
reg [8:0] transmit_byte_count;
reg [4:0] transmit_bit_count;
reg crc_enable;
reg crc_reset;
wire [4:0] EOP;
wire [39:0] encoded_crc;
reg [6:0] encoded_crc_counter;
reg [1:0] message_type;
reg [15:0] GoodCRC_Message_Header;
wire [19:0] encoded_GoodCRC_Message;
reg [7:0] crc_data_in;
/* Next Values */
reg [7:0] next_reg_address;
reg next_load;
reg next_bmc_enable;
reg next_bmc_data_in;
reg [5:0] next_preamble_counter;
reg [19:0] next_SOP_transmit;
reg [4:0] next_SOP_transmit_counter;
reg [8:0] next_transmit_byte_count;
reg [4:0] next_transmit_bit_count;
reg next_crc_enable;
reg next_crc_reset;
reg [7:0] next_byte_to_send;
reg [6:0] next_encoded_crc_counter;
reg next_done;
reg [1:0] next_message_type;




/*Current Values */

/*Delayed Values */
reg pll_bmc_clk_d;
reg enable_d;
/* assigns */
wire [1:0] SpecificationRevision;
assign posedge_pll_bmc_clk = ~pll_bmc_clk_d & pll_bmc_clk;
assign posedge_enable = ~enable_d &enable;
assign EOP = `EOP;
assign CablePlug = MESSAGE_HEADER_INFO[4];
assign DataTole = MESSAGE_HEADER_INFO[3];
assign SpecificationRevision = MESSAGE_HEADER_INFO[2:1];
assign PowerRole = MESSAGE_HEADER_INFO[0];
assign MessageID = RX_BUF_HEADER[11:9];
//assign crc_rewired[7:0] = {crc_out[31],crc_out[30],crc_out[29],crc_out[28],crc_out[27],crc_out[26],crc_out[25],crc_out[24]};
integer i;
integer j;
always@(*) begin
	for(i=0; i<=31; i = i+1) begin
		crc_rewired[i] = ~crc_out[31-i];
	end
end
always@(*) begin
	for(j=0; j<=7 ; j = j+1) begin
		crc_data_in[j] = byte_to_send[7-j];
	end
end
/* Wires/Regs for modules */
reg bmc_enable;

/*Module Instantiation*/
encoder4b5b encoder4b5b_high(
	.data_in_4b(byte_to_send[7:4]),
	.data_out_5b(encoded5b[9:5])
);
encoder4b5b encoder4b5b_low(
	.data_in_4b(byte_to_send[3:0]),
	.data_out_5b(encoded5b[4:0])
);
/* GoodCRC_MSG */
encoder4b5b GoodCRC_encoders_0(
	.data_in_4b(GoodCRC_Message_Header[15:12]),
	.data_out_5b(encoded_GoodCRC_Message[19:15])
);
encoder4b5b GoodCRC_encoders_1(
	.data_in_4b(GoodCRC_Message_Header[11:8]),
	.data_out_5b(encoded_GoodCRC_Message[14:10])
);
encoder4b5b GoodCRC_encoders_2(
	.data_in_4b(GoodCRC_Message_Header[7:4]),
	.data_out_5b(encoded_GoodCRC_Message[9:5])
);
encoder4b5b GoodCRC_encoders_3(
	.data_in_4b(GoodCRC_Message_Header[3:0]),
	.data_out_5b(encoded_GoodCRC_Message[4:0])
);
// CRC ENCODERS
encoder4b5b encoded_crc_0_3(
	.data_in_4b(crc_rewired[3:0]),
	.data_out_5b(encoded_crc[4:0])
);
encoder4b5b encoded_crc_4_7(
	.data_in_4b(crc_rewired[7:4]),
	.data_out_5b(encoded_crc[9:5])
);
encoder4b5b encoded_crc_8_11(
	.data_in_4b(crc_rewired[11:8]),
	.data_out_5b(encoded_crc[14:10])
);
encoder4b5b encoded_crc_12_15(
	.data_in_4b(crc_rewired[15:12]),
	.data_out_5b(encoded_crc[19:15])
);
encoder4b5b encoded_crc_16_19(
	.data_in_4b(crc_rewired[19:16]),
	.data_out_5b(encoded_crc[24:20])
);
encoder4b5b encoded_crc_20_23(
	.data_in_4b(crc_rewired[23:20]),
	.data_out_5b(encoded_crc[29:25])
);
encoder4b5b encoded_crc_24_27(
	.data_in_4b(crc_rewired[27:24]),
	.data_out_5b(encoded_crc[34:30])
);
encoder4b5b encoded_crc_28_31(
	.data_in_4b(crc_rewired[31:28]),
	.data_out_5b(encoded_crc[39:35])
);

bmc_encoder bmc_encoder(
	.enable(bmc_enable),
	.data_in(bmc_data_in),
	.bmc_clk(bmc_clk),
	.hiz(cc_hiz),
	.bmc(cc)
);
crc crc32_8b(
	.data_in(crc_data_in),
	.crc_en(crc_enable),
	.crc_out(crc_out),
	.rst(crc_reset),
	.clk(clk)
);

/* Next State Logic */
always@(*) begin
	next_state = SEND_PREAMBLE;
	case(state)
	SEND_PREAMBLE: begin
		if(preamble_counter == 63 && posedge_pll_bmc_clk)
			next_state = SEND_SOP;
		else
			next_state = SEND_PREAMBLE;
	end
	SEND_SOP: begin
		if(SOP_transmit_counter == 19 && posedge_pll_bmc_clk) begin
			next_state = LOAD_TRANSMIT_BYTE_COUNT;
			case(message_type)
			//HARD_RESET_MSG: Not sure what message to send
			//CABLE_RESET_MSG:
			GOODCRC_MSG: next_state = SEND_GOODCRC;
			SOP_MSG: next_state = LOAD_TRANSMIT_BYTE_COUNT;
			endcase 
		end
		else
			next_state = SEND_SOP;
	end
	LOAD_TRANSMIT_BYTE_COUNT: begin
		if(load_done)
			next_state = LOAD_BYTE;
		else
			next_state = LOAD_TRANSMIT_BYTE_COUNT;
	end
	LOAD_BYTE: begin
		if(load_done)
			next_state = CALCULATE_CRC;
		else
			next_state = LOAD_BYTE;
	end
	CALCULATE_CRC: next_state = SEND_MESSAGE;
	SEND_MESSAGE: begin
		if(transmit_byte_count != 1 && transmit_bit_count == 9 && posedge_pll_bmc_clk)
			next_state = LOAD_BYTE;
		else if(transmit_byte_count == 1 && posedge_pll_bmc_clk && transmit_bit_count == 9)
			next_state = SEND_CRC;
		else 
			next_state = SEND_MESSAGE;
	end
	SEND_CRC: begin
		if(encoded_crc_counter == 39 && posedge_pll_bmc_clk)
			next_state = SEND_EOP;
		else
			next_state = SEND_CRC;
	end
	SEND_EOP: begin
		if(SOP_transmit_counter == 4 && posedge_pll_bmc_clk)
			next_state = SEND_PREAMBLE;
		else
			next_state = SEND_EOP;
	end
	SEND_GOODCRC: begin
		if(SOP_transmit_counter == 19 && posedge_pll_bmc_clk)
			next_state = SEND_CRC;
		else
			next_state = SEND_GOODCRC;
	end
	endcase
	if(~enable)
		next_state = SEND_PREAMBLE;
end
/* Next Output Logic */
always@(*) begin
	next_load =0;
	next_done = 0;
	next_crc_enable = 0;
	next_crc_reset = 0;
	next_bmc_enable = 0;
	next_bmc_data_in = bmc_data_in;
	next_preamble_counter = preamble_counter;
	next_SOP_transmit_counter = SOP_transmit_counter;
	next_SOP_transmit = SOP_transmit;
	next_transmit_byte_count = transmit_byte_count;
	next_transmit_bit_count = transmit_bit_count;
	next_encoded_crc_counter = encoded_crc_counter;
	next_reg_address =  reg_address;
	next_message_type = message_type;
	case(state)
	SEND_PREAMBLE: begin
		if(enable) begin
			next_bmc_enable = 1;
			if(posedge_pll_bmc_clk) begin
				next_bmc_data_in = ~bmc_data_in;
				next_preamble_counter = preamble_counter +1;
				next_reg_address = `TRANSMIT_BYTE_COUNT;
			end
		end
		else begin
			next_bmc_enable = 0;
			next_bmc_data_in = 0;
			next_preamble_counter = 0;
			next_SOP_transmit_counter = 0;
			next_transmit_bit_count =0;
			next_transmit_byte_count = 0;	
			next_SOP_transmit = SOP;
			next_encoded_crc_counter = 0;
			next_reg_address = `TRANSMIT_BYTE_COUNT;
		end
	end
	SEND_SOP: begin
		next_bmc_enable = 1;
		next_bmc_data_in = SOP[SOP_transmit_counter];
		if(posedge_pll_bmc_clk) begin
			next_SOP_transmit_counter = SOP_transmit_counter + 1;
			if(SOP_transmit_counter == 19)
				next_SOP_transmit_counter = 0;
		end
	end
	LOAD_TRANSMIT_BYTE_COUNT: begin
		next_bmc_enable = 1;
		next_reg_address = `TRANSMIT_BYTE_COUNT;
		next_load = 1;
		if(load_done) begin
			next_transmit_byte_count = data_in_from_reg;
			//next_load = 0;
			next_crc_reset = 1;
			next_reg_address = `TRANSMIT_BYTE_COUNT +1;
		end
	end
	LOAD_BYTE: begin
		next_bmc_enable = 1;
		if(transmit_byte_count != 0) begin
			next_load = 1;
			if(load_done) begin
				next_byte_to_send = data_in_from_reg;
				//next_transmit_byte_count = transmit_byte_count - 1;
				next_reg_address = reg_address +1;
				next_load = 0;
				next_crc_enable =1;
			end
		end
	end
	CALCULATE_CRC: next_bmc_enable = 1;
	SEND_MESSAGE: begin
		next_bmc_enable = 1;
		next_bmc_data_in = encoded5b[transmit_bit_count];
		if(posedge_pll_bmc_clk) begin
			next_transmit_bit_count = transmit_bit_count +1;
			if(transmit_bit_count == 9) begin
				next_transmit_bit_count = 0;
				next_transmit_byte_count = transmit_byte_count -1;
			end
		end
	end
	SEND_CRC: begin
		next_bmc_enable = 1;
		next_bmc_data_in = encoded_crc[encoded_crc_counter];
		if( posedge_pll_bmc_clk) begin
			next_encoded_crc_counter = encoded_crc_counter + 1;
			if(encoded_crc_counter == 39)
				next_encoded_crc_counter = 0;
		end
	end
	SEND_EOP: begin
		next_bmc_enable = 1;
		next_bmc_data_in = EOP[SOP_transmit_counter];
		if(posedge_pll_bmc_clk) begin
			next_SOP_transmit_counter = SOP_transmit_counter + 1;
			if(SOP_transmit_counter == 4) begin
				next_SOP_transmit_counter  = 0;
				next_done = 1;
			end
		end
	end
	SEND_GOODCRC: begin
		next_bmc_enable = 1;
		next_bmc_data_in = encoded_GoodCRC_Message[SOP_transmit_counter];
		if(posedge_pll_bmc_clk) begin
			next_SOP_transmit_counter = SOP_transmit_counter + 1;
			if(SOP_transmit_counter == 19)
				next_SOP_transmit_counter  = 0;
		end		
	end
	endcase
	if(posedge_enable) begin
		if(send_hard_reset)
			next_message_type = HARD_RESET_MSG;
		else if(send_cable_reset)
			next_message_type = CABLE_RESET_MSG;
		else if(send_goodCRC) 
			next_message_type = GOODCRC_MSG;
		else
			next_message_type = SOP_MSG;	
	end
end
/*Clk Logic */
always @(posedge clk) begin
	pll_bmc_clk_d <= pll_bmc_clk;
	enable_d <= enable;
	if(~enable) begin
		state <= SEND_PREAMBLE;
	end
	else
		state <= next_state;
	reg_address <= next_reg_address;
	load <= next_load;
	done <= next_done;
	crc_enable <= next_crc_enable;	
	encoded_crc_counter <= next_encoded_crc_counter;	
	crc_reset <= next_crc_reset;
	bmc_enable <= next_bmc_enable;
	bmc_data_in <= next_bmc_data_in;
	preamble_counter <= next_preamble_counter;
	SOP_transmit_counter <= next_SOP_transmit_counter;
	SOP_transmit <= next_SOP_transmit;
	transmit_byte_count <= next_transmit_byte_count;
	transmit_bit_count <= next_transmit_bit_count;
	byte_to_send <= next_byte_to_send;
	message_type <= next_message_type;
	
end
initial $dumpvars;
endmodule
