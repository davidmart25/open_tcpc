module bmc_encoder (
	input enable,
	
	input data_in,
	input bmc_clk,
	
	output bmc,
	output hiz
);
/* State REGs */
reg [2:0] state;
reg [2:0] next_state;

/*One Hote Encoding*/
parameter IDLE =       3'b001;
parameter TRANSMIT_1 = 3'b010;
parameter TRANSMIT_2 = 3'b100;

/* Next values */ 
reg next_bmc;
reg next_hiz;
reg next_data_out;

/* Current Values */
reg current_bmc;
reg current_hiz;
reg current_data_out;


/* Output Assigns */
assign bmc = current_bmc;
assign hiz = current_hiz;

/*Next State Logic */
always@(*) begin
	next_state = IDLE;
	case(state)
	IDLE: begin
		if(enable)
			next_state = TRANSMIT_2;
		else
			next_state = IDLE;
	end
	TRANSMIT_1: next_state = TRANSMIT_2;
	TRANSMIT_2: next_state = TRANSMIT_1;
	endcase
	if(~enable)
		next_state = IDLE;
end
/*Next Output Logic */
always@(*) begin
	next_data_out = 1'bx;
	next_bmc = 0; //No se
	next_hiz = 0;
	case(state)
	IDLE: begin
		if(enable) begin
			next_data_out = data_in;
			next_bmc = 0;
			next_hiz = 0;
		end
		else begin
			next_hiz = 1;
			next_bmc = 1;
		end		
	end
	TRANSMIT_1: begin
		next_bmc = ~current_bmc;
		next_data_out = data_in;
	end
	TRANSMIT_2: begin
		if(current_data_out == 1)
			next_bmc = ~current_bmc;
		else if (current_data_out == 0)
			next_bmc = current_bmc;
	end
	endcase
end
/* Clk logic */
always@(posedge bmc_clk) begin
	if(~enable) begin
		state <= IDLE;
		current_hiz = 1;
		current_bmc = 1;
	end
	else begin
		state <= next_state;
		current_hiz = next_hiz;
		current_bmc <= next_bmc;
	end
	current_data_out <= next_data_out;
end
initial $dumpvars;
endmodule
