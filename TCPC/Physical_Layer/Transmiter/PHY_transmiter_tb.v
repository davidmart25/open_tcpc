`include "PHY_transmiter.v"
`include "../Receiver/FINAL_RECEIVER/PHY_receiver.v"
`include "../Receiver/4b5b_decoder/4b5b_decoder.v"
`include "../Receiver/BMC_Decoder/PLL/BMC_to_bin.v"
`include "../Receiver/SOP_Detect/sop_detect.v"
`include "../Receiver/CRC/CRC_32_outputlogic_4b.v"

module testbench;
reg clk;
reg bmc_clk;
reg [7:0] data_in_from_reg;
wire load_done;
reg phy_transmiter_enable;
wire [19:0] SOP;
assign SOP = {`SYNC_2,`SYNC_1,`SYNC_1,`SYNC_1};
wire [7:0] reg_address;
reg preamble_done;
assign Extended = message_header[15];
assign Number_of_Data_Objects = message_header[14:12];
reg pll_bmc_clk;
reg phy_receiver_enable;
wire [15:0] RX_HEADER;
assign load_done = 1;
always@(posedge bmc_clk) begin
	pll_bmc_clk <= ~pll_bmc_clk;
end
phy_transmiter trans_test(
	.TRANSMIT(0),
	.clk(clk),
	.bmc_clk(bmc_clk),
	.data_in_from_reg(data_in_from_reg),
	.load_done(load_done),
	.enable(phy_transmiter_enable),
	.reg_address(reg_address),
	.load(load),
	.cc(cc),
	.done(trans_done)
);
phy_receiver test_receiver(
	.clk(clk),
	.pll_bmc_clk(~pll_bmc_clk),
	.enable(phy_receiver_enable),
	.preamble_done(preamble_done),
	.RECEIVE_DETECT(8'hFF),
	.rx_buffer_full(0),
	.RX_BUF_HEADER(16'b0001000000000000),
	.cc(cc)
);
initial begin
	preamble_done = 0;
	clk =0;
	bmc_clk = 0;
	pll_bmc_clk = 0;
	data_in_from_reg = 6;
	phy_transmiter_enable =0;
	phy_receiver_enable = 0;
	#1000 phy_transmiter_enable =1;
	phy_receiver_enable = 1;
end

initial begin
	forever #1 clk = ~clk;
end
initial begin
	forever #100 bmc_clk = ~bmc_clk;
end
initial begin
	$dumpfile("PHY_Transaction.vcd"); //non synth code, simulation only
	$dumpvars;
end
initial #27117 preamble_done = 1;




endmodule
