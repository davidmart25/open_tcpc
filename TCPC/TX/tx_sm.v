//`include "../../include/registers.vh"
module tcpc_tx(
	input clk,
	input PRL_Rx_Message_Discard_discard_transmition,
	input hard_reset,
	input cable_reset,
	input [7:0] TRANSMIT,
	input TRANSMIT_written,
	input message_sent_to_PHY,
	input initialize_and_run_CRCReceive_Timer,
	input CRCReceiveTimerTimeout,
	input message_discarded_bus_idle,
	input goodcrc_response_from_phy_layer,
	input [7:0] TX_BUF_HEADER_BYTE_1,
	input [7:0] RX_BUF_HEADER_BYTE_1,
	input [7:0] RX_BUF_FRAME_TYPE,
	input DFP,
	input UFP,
	
	output reg tx_state_machine_active,
	output reg [7:0] write_data,
	output reg [7:0] reg_address,
	output reg write,
	output reg PHY_trans_enable,
	output reg initialize_and_run_CRCReceiveTimer,
	output reg expect_goodCRC
);
/* State REGs */
reg [7:0] state;
reg [7:0] next_state;

/* One Hot encoding */
parameter PRL_Tx_Wait_for_Transmit_Request = 8'b00000001;
parameter PRL_Tx_Reset_RetryCounter =        8'b00000010;
parameter PRL_Tx_Construct_Message =         8'b00000100;
parameter PRL_Tx_Wait_for_PHY_response =     8'b00001000;
parameter PRL_Tx_Match_MessageID =           8'b00010000;
parameter PRL_Tx_Report_Success =            8'b00100000;
parameter PRL_Tx_Check_RetryCounter =        8'b01000000;
parameter PRL_Tx_Report_Failure =            8'b10000000;

/* Other Parameters */
parameter nRetryCount = 2'd2;

/* Internal Values */
reg [1:0] RetryCounter;

/* Next Values */
reg [1:0] next_RetryCounter;
reg next_PHY_trans_enable;
reg next_initialize_and_run_CRCReceiveTimer;
reg [7:0] next_reg_address;
reg [7:0] next_write_data;
reg next_write;
reg next_tx_state_machine_active;
reg next_expect_GoodCRC;


/* Next State Logic */
always @(*) begin
	next_state = PRL_Tx_Wait_for_Transmit_Request;
	case(state)
	PRL_Tx_Wait_for_Transmit_Request: begin
		if(TRANSMIT[2:0] < 3'b101 && TRANSMIT_written)
			next_state = PRL_Tx_Reset_RetryCounter;
		else
			next_state = PRL_Tx_Wait_for_Transmit_Request;
	end
	PRL_Tx_Reset_RetryCounter: next_state = PRL_Tx_Construct_Message;
	PRL_Tx_Construct_Message: begin
		if(message_sent_to_PHY)
			next_state = PRL_Tx_Wait_for_PHY_response;
		else
			next_state = PRL_Tx_Construct_Message;
	end
	PRL_Tx_Wait_for_PHY_response: begin
		if(CRCReceiveTimerTimeout | message_discarded_bus_idle)
			next_state = PRL_Tx_Check_RetryCounter;
		else if(goodcrc_response_from_phy_layer)
			next_state = PRL_Tx_Match_MessageID;
		else
			next_state = PRL_Tx_Wait_for_PHY_response;
	end
	PRL_Tx_Match_MessageID: begin
		if(TX_BUF_HEADER_BYTE_1 != RX_BUF_HEADER_BYTE_1 || TRANSMIT[2:0] != RX_BUF_FRAME_TYPE[2:0])
			next_state = PRL_Tx_Check_RetryCounter;
		else
			next_state = PRL_Tx_Report_Success;
	end
	PRL_Tx_Report_Success: next_state = PRL_Tx_Wait_for_Transmit_Request;
	PRL_Tx_Check_RetryCounter: begin
		if(RetryCounter > nRetryCount)
			next_state = PRL_Tx_Report_Failure;
		else if (RetryCounter <= nRetryCount )
			next_state = PRL_Tx_Construct_Message;
		else
			next_state = PRL_Tx_Check_RetryCounter;
	end
	PRL_Tx_Report_Failure: next_state = PRL_Tx_Wait_for_Transmit_Request;
	endcase
	if(PRL_Rx_Message_Discard_discard_transmition || hard_reset || cable_reset)
		next_state = PRL_Tx_Wait_for_Transmit_Request;
end
/*Next Outpút Logic */
//case made with next_state due to actions on entry
always @(*) begin
	next_RetryCounter = RetryCounter;
	next_PHY_trans_enable = 0;
	next_initialize_and_run_CRCReceiveTimer = 0 ;
	next_reg_address = `ALERT;
	next_write_data = write_data;
	next_write = 0;
	next_tx_state_machine_active =0;
	next_expect_GoodCRC = 0;
	case(next_state)
	PRL_Tx_Reset_RetryCounter: next_RetryCounter =0;
	PRL_Tx_Construct_Message: next_PHY_trans_enable = 1;
	PRL_Tx_Wait_for_PHY_response: begin
		next_initialize_and_run_CRCReceiveTimer = 1;
		next_expect_GoodCRC = 1;
	end
//	PRL_Tx_Match_MessageID:
	PRL_Tx_Report_Success: begin
		next_reg_address = `ALERT;
		next_write_data = 0;
		next_write_data[6] = 1;
		next_write = 1;
	end
	PRL_Tx_Check_RetryCounter: begin
		if(DFP || UFP)
			next_RetryCounter = RetryCounter +1;
	end
	PRL_Tx_Report_Failure: begin
		next_reg_address = `ALERT;
		next_write_data = 0;
		next_write_data[4] = 1;
		next_write = 1;
	end
	endcase
	if(next_state != PRL_Tx_Wait_for_Transmit_Request)
		next_tx_state_machine_active = 1;
end
/* CLK Logic */
always @(posedge clk) begin
	if(PRL_Rx_Message_Discard_discard_transmition || hard_reset || cable_reset)
		state <= PRL_Tx_Wait_for_Transmit_Request;
	else
		state <= next_state;
		
	RetryCounter <= next_RetryCounter;
	PHY_trans_enable <= next_PHY_trans_enable;
	initialize_and_run_CRCReceiveTimer <= next_initialize_and_run_CRCReceiveTimer;
	reg_address <= next_reg_address;
	write_data <= next_write_data;
	write <= next_write;
	tx_state_machine_active <= next_tx_state_machine_active;
end

endmodule
