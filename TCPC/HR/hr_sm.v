//`include "../../include/SOP_defines.vh"
//`include "../../include/registers.vh"
module tcpc_hr(
	input clk,
	input power_on_reset,
	
	input TRANSMIT_written,
	input [7:0] TRANSMIT,
	
	output reg start_tHardResetComplete_timer,
	output reg req_PHY_send_hr,
	output reg req_PHY_send_cr,
	input hr_cr_sent,
	input tHardResetComplete_expires,
	output reg stop_tHardResetComplete_timer,
	output reg stop_attempting_to_send_hr_cr ,
	output [7:0] reg_address,
	output reg [7:0] write_data,
	output reg write
);

/* State Regs */
reg [4:0] state;
reg [4:0] next_state;

/* One hot encoding */
parameter PRL_HR_Wait_for_Hard_Reset_Request = 5'b00001;
parameter PRL_HR_Construct_Message = 5'b00010;
parameter PRL_HR_Success = 5'b00100;
parameter PRL_HR_Failure = 5'b01000;
parameter PRL_HR_Report = 5'b10000;

/*Internal Values */

/* Next Values*/
reg next_start_tHardResetComplete_timer;
reg next_req_PHY_send_hr;
reg next_req_PHY_send_cr;
reg next_stop_tHardResetComplete_timer;
reg next_stop_attempting_to_send_hr_cr;
reg next_write;
reg [7:0] next_write_data;
/*Current Values */
/*Assigns*/
assign reg_address = `ALERT;
/*Next State Logic */
always@(*) begin
	next_state = PRL_HR_Wait_for_Hard_Reset_Request;
	case(state)
	PRL_HR_Wait_for_Hard_Reset_Request: begin
		if(TRANSMIT_written && (TRANSMIT[2:0] == 3'b101 || TRANSMIT[2:0] == 3'b110) )
			next_state = PRL_HR_Construct_Message;
		else
			next_state = PRL_HR_Wait_for_Hard_Reset_Request;
	end
	PRL_HR_Construct_Message: begin
		if(hr_cr_sent)
			next_state = PRL_HR_Success;
		else if(tHardResetComplete_expires)
			next_state =  PRL_HR_Failure;
		else
			next_state = PRL_HR_Construct_Message;
	end
	PRL_HR_Success: next_state = PRL_HR_Report;
	PRL_HR_Failure: next_state = PRL_HR_Report;
	PRL_HR_Report: next_state = PRL_HR_Wait_for_Hard_Reset_Request;
	endcase
end
/* Next Output Logic */
always@(*) begin
	next_start_tHardResetComplete_timer = 0;
	next_req_PHY_send_hr = 0;
	next_req_PHY_send_cr = 0;
	next_stop_tHardResetComplete_timer = 0;
	next_stop_attempting_to_send_hr_cr =0;
	next_write = 0;
	next_write_data = 0; // DONT CARE
	case(next_state)
	PRL_HR_Construct_Message: begin
			if(TRANSMIT[2:0] == `HARD_RESET) begin
				next_start_tHardResetComplete_timer = 1;
				next_req_PHY_send_hr = 1;
			end
			else if(TRANSMIT[2:0] == `CABLE_RESET) begin 
				next_start_tHardResetComplete_timer = 1;
				next_req_PHY_send_cr =1;
			end
	end
	PRL_HR_Success: next_stop_tHardResetComplete_timer = 1;
	PRL_HR_Failure: next_stop_attempting_to_send_hr_cr =1;
	PRL_HR_Report: begin
		next_write = 1;
		next_write_data = 0;
		if(state == PRL_HR_Success)
			next_write_data[6] = 1;
		else if (state == PRL_HR_Failure)
			next_write_data[4] = 1;
	end
	endcase
end
/*CLK Logic */
always@(posedge clk) begin
	if(power_on_reset)
		state <= PRL_HR_Wait_for_Hard_Reset_Request;
	else
		state <= next_state;
		
	start_tHardResetComplete_timer<= next_start_tHardResetComplete_timer;
	req_PHY_send_hr<= next_req_PHY_send_hr;
	req_PHY_send_cr<= next_req_PHY_send_cr;
	stop_tHardResetComplete_timer<= next_stop_tHardResetComplete_timer;
	stop_attempting_to_send_hr_cr<= next_stop_attempting_to_send_hr_cr;
	write<= next_write;
	write_data<= next_write_data;
end
endmodule
