//`include "../../include/registers.vh"
//`include "../../include/Vendor_Defines.vh"
module reg_file_8(
	input clk,
	input reset,
	
	input [7:0] write_address_1, // For RX SM
	input [7:0] write_data_1,
	input write_1,
	
	input [7:0] read_address_1, // For Tx
	output reg [7:0] read_data_1,
	input read_1,
	output reg read_1_done,
	
	input [7:0] write_address_2, // For TCPM
	input [7:0] write_data_2,
	input write_2,
	
	input [7:0] read_address_2, // For TCPM
	output reg [7:0] read_data_2,
	input read_2,
	output reg read_2_done,
	
	input [7:0] write_address_3, // For 
	input [7:0] write_data_3,
	input write_3,
	
	input [7:0] read_address_3, // For 
	output reg [7:0] read_data_3,
	input read_3,
	output reg read_3_done,
	output reg TRANSMIT_written,
		
	input [7:0] write_address_4, // For TX
	input [7:0] write_data_4,
	input write_4,
	
	input [7:0] write_address_5, // For HR
	input [7:0] write_data_5,
	input write_5,	
	
	
	//All regs
	output [15:0] VENDOR_ID,
	output [15:0] PRODUCT_ID,
	output [15:0] DEVICE_ID,
	output [15:0] USBTYPEC_REV,
	output [15:0] USBPD_REV_VER,
	output [15:0] PD_INTERFACE_REV,
	// 0C-0F RESERVED
	output [15:0] ALERT,
	output [15:0] ALERT_MASK,
	output [7:0] POWER_STATUS_MASK,
	output [7:0] FAULT_STATUS_MASK,
	// 16-17 RESERVED
	output [7:0] CONFIG_STANDARD_OUTPUT,
	output [7:0] TCPC_CONTROL,
	output [7:0] ROLE_CONTROL,
	output [7:0] FAULT_CONTROL,
	output [7:0] POWER_CONTROL,
	output [7:0] CC_STATUS,
	output [7:0] POWER_STATUS,
	output [7:0] FAULT_STATUS,
	// 20-22 RESERVED
	output [7:0] COMMAND,
	output [15:0] DEVICE_CAPABILITIES_1,
	output [15:0] DEVICE_CAPABILITIES_2,
	output [7:0] STANDARD_INPUT_CAPABILITIES,
	output [7:0] STANDARD_OUTPUT_CAPABILITIES,
	output [7:0] MESSAGE_HEADER_INFO,
	output [7:0] RECEIVE_DETECT,

	//RECEIVE_BUFFER

	output [7:0] RECEIVE_BYTE_COUNT,
	output [7:0] RX_BUF_FRAME_TYPE,
	output [7:0] RX_BUF_HEADER_BYTE_0,
	output [7:0] RX_BUF_HEADER_BYTE_1,
	output [7:0] RX_BUF_OBJ1_BYTE_0,
	output [7:0] RX_BUF_OBJ1_BYTE_1,
	output [7:0] RX_BUF_OBJ1_BYTE_2,
	output [7:0] RX_BUF_OBJ1_BYTE_3,
	output [7:0] RX_BUF_OBJ2_BYTE_0,
	output [7:0] RX_BUF_OBJ2_BYTE_1,
	output [7:0] RX_BUF_OBJ2_BYTE_2,
	output [7:0] RX_BUF_OBJ2_BYTE_3,
	output [7:0] RX_BUF_OBJ3_BYTE_0,
	output [7:0] RX_BUF_OBJ3_BYTE_1,
	output [7:0] RX_BUF_OBJ3_BYTE_2,
	output [7:0] RX_BUF_OBJ3_BYTE_3,
	output [7:0] RX_BUF_OBJ4_BYTE_0,
	output [7:0] RX_BUF_OBJ4_BYTE_1,
	output [7:0] RX_BUF_OBJ4_BYTE_2,
	output [7:0] RX_BUF_OBJ4_BYTE_3,
	output [7:0] RX_BUF_OBJ5_BYTE_0,
	output [7:0] RX_BUF_OBJ5_BYTE_1,
	output [7:0] RX_BUF_OBJ5_BYTE_2,
	output [7:0] RX_BUF_OBJ5_BYTE_3,
	output [7:0] RX_BUF_OBJ6_BYTE_0,
	output [7:0] RX_BUF_OBJ6_BYTE_1,
	output [7:0] RX_BUF_OBJ6_BYTE_2,
	output [7:0] RX_BUF_OBJ6_BYTE_3,
	output [7:0] RX_BUF_OBJ7_BYTE_0,
	output [7:0] RX_BUF_OBJ7_BYTE_1,
	output [7:0] RX_BUF_OBJ7_BYTE_2,
	output [7:0] RX_BUF_OBJ7_BYTE_3,
	output [7:0] TRANSMIT,

	//TRANSMIT_BUFFER

	output [7:0] TRANSMIT_BYTE_COUNT,
	output [7:0] TX_BUF_HEADER_BYTE_0,
	output [7:0] TX_BUF_HEADER_BYTE_1,
	output [7:0] TX_BUF_OBJ1_BYTE_0,
	output [7:0] TX_BUF_OBJ1_BYTE_1,
	output [7:0] TX_BUF_OBJ1_BYTE_2,
	output [7:0] TX_BUF_OBJ1_BYTE_3,
	output [7:0] TX_BUF_OBJ2_BYTE_0,
	output [7:0] TX_BUF_OBJ2_BYTE_1,
	output [7:0] TX_BUF_OBJ2_BYTE_2,
	output [7:0] TX_BUF_OBJ2_BYTE_3,
	output [7:0] TX_BUF_OBJ3_BYTE_0,
	output [7:0] TX_BUF_OBJ3_BYTE_1,
	output [7:0] TX_BUF_OBJ3_BYTE_2,
	output [7:0] TX_BUF_OBJ3_BYTE_3,
	output [7:0] TX_BUF_OBJ4_BYTE_0,
	output [7:0] TX_BUF_OBJ4_BYTE_1,
	output [7:0] TX_BUF_OBJ4_BYTE_2,
	output [7:0] TX_BUF_OBJ4_BYTE_3,
	output [7:0] TX_BUF_OBJ5_BYTE_0,
	output [7:0] TX_BUF_OBJ5_BYTE_1,
	output [7:0] TX_BUF_OBJ5_BYTE_2,
	output [7:0] TX_BUF_OBJ5_BYTE_3,
	output [7:0] TX_BUF_OBJ6_BYTE_0,
	output [7:0] TX_BUF_OBJ6_BYTE_1,
	output [7:0] TX_BUF_OBJ6_BYTE_2,
	output [7:0] TX_BUF_OBJ6_BYTE_3,
	output [7:0] TX_BUF_OBJ7_BYTE_0,
	output [7:0] TX_BUF_OBJ7_BYTE_1,
	output [7:0] TX_BUF_OBJ7_BYTE_2,
	output [7:0] TX_BUF_OBJ7_BYTE_3,


	output [15:0] VBUS_VOLTAGE,
	output [15:0] VBUS_SINK_DISCONNECT_THRESHOLD,
	output [15:0] VBUS_STOP_DISCHARGE_THRESHOLD,
	output [15:0] VBUS_VOLTAGE_ALATRAM_HI_CFG,
	output [15:0] VBUS_VOLTAGE_ALARM_LO_CFG
		
);

	
reg [7:0] registers [255:0];
reg [7:0] reset_registers[255:0];
reg [7:0] next_write_data_1;
reg [7:0] next_write_data_2;
reg [7:0] next_write_data_4;
reg [7:0] next_write_data_5;
/* Borrar soon
assign TRANSMIT_2_0 = registers[`TRANSMIT][2:0];
assign TX_BUFF_HEADER_BYTE_1 = registers[`TX_BUF_HEADER_BYTE_1];
assign RX_BUFF_HEADER_BYTE_1 = registers [`RX_BUF_HEADER_BYTE_1];
assign RX_BUFF_FRAME_TYPE = registers [`RX_BUF_FRAME_TYPE];
*/
integer i;

always @(posedge clk) begin
	if(reset) begin
		for(i =0; i<256;i=i+1)
			registers[i] <= reset_registers[i];
	end
	else begin
		if(write_1)
			registers[write_address_1] <= next_write_data_1;
		if(write_2)
			registers[write_address_2] <= next_write_data_2;
		if(write_3)
			registers[write_address_3] <= write_data_3;
		if(read_1) begin
			read_data_1 <= registers[read_address_1];
			read_1_done <= 1;
		end
		else 
			read_1_done <= 0;
		if(read_2) begin
			read_data_2 <= registers[read_address_2];
			read_2_done <= 1;
		end 
		else
			read_2_done <= 0;
		if(read_3) begin
			read_data_3 <= registers[read_address_3];
			read_3_done <= 1;
		end
		else
			read_3_done <=0;
	end
	if(write_address_2 == `TRANSMIT && write_2)
		TRANSMIT_written <= 1;
	else
		TRANSMIT_written <= 0;
end

//TCPM Writes 
always@(*) begin
	next_write_data_2 = registers[write_address_2];
	case(write_address_2)
	`ALERT: next_write_data_2 = ~write_data_2 & registers[`ALERT];
	`ALERT +1 : begin
		next_write_data_2 = ~write_data_2 & registers[`ALERT+1];
		
		if(write_data_2[2] == 1)
			if(registers[`ALERT+1][2] == 1)
				if(registers[`ALERT][2] == 0)
					next_write_data_2[2]= 0;
				else
					next_write_data_2[2] = 1;
			else
				next_write_data_2[2] = 0;
		else
			next_write_data_2[2] = registers[`ALERT+1][2];	
	end
	`ALERT_MASK: next_write_data_2 = write_data_2;
	`ALERT_MASK+1: next_write_data_2 = write_data_2;
	`POWER_STATUS_MASK: next_write_data_2 = write_data_2;
	`FAULT_STATUS_MASK: next_write_data_2 = write_data_2;
	`TCPC_CONTROL:next_write_data_2 = write_data_2;
	`ROLE_CONTROL:next_write_data_2 = write_data_2;
	`FAULT_CONTROL:next_write_data_2 = write_data_2;
	`POWER_CONTROL:next_write_data_2 = write_data_2;
	`FAULT_STATUS: next_write_data_2 = ~write_data_2 & registers[`FAULT_STATUS]; // RW
	`COMMAND: next_write_data_2 = write_data_2;
	`MESSAGE_HEADER_INFO: next_write_data_2 = write_data_2;
	`RECEIVE_DETECT: next_write_data_2 = write_data_2;
	`TRANSMIT: next_write_data_2 = write_data_2;
	`TRANSMIT_BYTE_COUNT: next_write_data_2 = write_data_2;
	`TX_BUF_HEADER_BYTE_0: next_write_data_2 = write_data_2;
	`TX_BUF_HEADER_BYTE_1: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ1_BYTE_0: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ1_BYTE_1: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ1_BYTE_2: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ1_BYTE_3: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ2_BYTE_0: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ2_BYTE_1: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ2_BYTE_2: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ2_BYTE_3: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ3_BYTE_0: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ3_BYTE_1: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ3_BYTE_2: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ3_BYTE_3: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ4_BYTE_0: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ4_BYTE_1: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ4_BYTE_2: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ4_BYTE_3: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ5_BYTE_0: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ5_BYTE_1: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ5_BYTE_2: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ5_BYTE_3: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ6_BYTE_0: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ6_BYTE_1: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ6_BYTE_2: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ6_BYTE_3: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ7_BYTE_0: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ7_BYTE_1: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ7_BYTE_2: next_write_data_2 = write_data_2;
	`TX_BUF_OBJ7_BYTE_3: next_write_data_2 = write_data_2;
	`VBUS_SINK_DISCONNECT_THRESHOLD: next_write_data_2 = write_data_2;
	`VBUS_SINK_DISCONNECT_THRESHOLD: next_write_data_2 = write_data_2;
	`VBUS_STOP_DISCHARGE_THRESHOLD: next_write_data_2 = write_data_2;
	endcase
end 
// RX Writes:
always@(*) begin
	next_write_data_1 = write_data_1;
	case(write_address_1)
	`ALERT: next_write_data_1 = write_data_1 | registers[write_address_1];
	endcase
end
// TX Writes:
always@(*) begin
	next_write_data_4 = write_data_4;
	case(write_address_4)
	`ALERT: next_write_data_4 = write_data_4 | registers[write_address_4];
	endcase
end
// HR Writes:
always@(*) begin
	next_write_data_5 = write_data_5;
	case(write_address_5)
	`ALERT: next_write_data_5 = write_data_5 | registers[write_address_5];
	endcase
end
//Reset Values
always@(*) begin
		for(i = 0; i<256 ; i= i+1)
			reset_registers[i] = 0;
		
		reset_registers[`VENDOR_ID] = `VD_VENDOR_ID;
		reset_registers[`PRODUCT_ID] = `VD_PRODUCT_ID;
		reset_registers[`DEVICE_ID] = `VD_DEVICE_ID;
		reset_registers[`USBTYPEC_REV] = `VD_USBTYPEC_REV;
		reset_registers[`USBPD_REV_VER] = `VD_USBPD_REV_VER;
		reset_registers[`PD_INTERFACE_REV] = `VD_PD_INTERFACE_REV;
		reset_registers[`ALERT_MASK+1] = 8'h0F;
		reset_registers[`ALERT_MASK] = 8'hFF;
		reset_registers[`POWER_STATUS_MASK] = 8'hFF;
		reset_registers[`FAULT_STATUS_MASK] = 8'hFF;
		reset_registers[`CONFIG_STANDARD_OUTPUT] = 8'h60;
		reset_registers[`POWER_CONTROL] = 8'h60;
		reset_registers[`FAULT_STATUS] = 8'h80;
		reset_registers[`DEVICE_CAPABILITIES_1] = `VD_DEVICE_CAPABILITIES_1;
		reset_registers[`DEVICE_CAPABILITIES_2] = `VD_DEVICE_CAPABILITIES_2;
		reset_registers[`STANDARD_INPUT_CAPABILITIES] = `VD_STANDARD_INPUT_CAPABILITIES;
		reset_registers[`STANDARD_OUTPUT_CAPABILITIES] = `VD_STANDARD_OUTPUT_CAPABILITIES;
		reset_registers[`VBUS_SINK_DISCONNECT_THRESHOLD+1] = 0;
		reset_registers[`VBUS_SINK_DISCONNECT_THRESHOLD] = 200; // SAFE5V?
		reset_registers[`RECEIVE_DETECT] = 8'hFF; // SAFE5V?
		case(DEVICE_CAPABILITIES_1[7:5])
		3'b000: begin
			reset_registers[`ROLE_CONTROL] = 8'h0A;
			reset_registers[`MESSAGE_HEADER_INFO] = 8'h02;
		end
		3'b001: begin
			reset_registers[`ROLE_CONTROL] = 8'h05;
			reset_registers[`MESSAGE_HEADER_INFO] = 8'h0B;
		end
		3'b010: begin
			reset_registers[`ROLE_CONTROL] = 8'h0A;
			reset_registers[`MESSAGE_HEADER_INFO] = 8'h02;
		end
		3'b011: begin
			reset_registers[`ROLE_CONTROL] = 8'h0A;
			reset_registers[`MESSAGE_HEADER_INFO] = 8'h02;
		end
		3'b100: begin
			reset_registers[`ROLE_CONTROL] = 8'h0A;
			reset_registers[`MESSAGE_HEADER_INFO] = 8'h02;
		end
		3'b101: begin
			reset_registers[`ROLE_CONTROL] = 8'h0A;
			reset_registers[`MESSAGE_HEADER_INFO] = 8'h02;
		end
		3'b110: begin
			reset_registers[`ROLE_CONTROL] = 8'h0A;
			reset_registers[`MESSAGE_HEADER_INFO] = 8'h02;
		end
		default: begin
			reset_registers[`ROLE_CONTROL] = 8'hXX;
			reset_registers[`MESSAGE_HEADER_INFO] = 8'hXX;
		end
		endcase
		for(i = 'h80; i<256; i=i+1)
			reset_registers[i] =`VD_Vendor_defined_bits;	
end

//All regs
assign VENDOR_ID = {registers[`VENDOR_ID+1], registers[`VENDOR_ID]};
assign PRODUCT_ID = {registers[`PRODUCT_ID+1], registers[`PRODUCT_ID]};
assign DEVICE_ID = {registers[`DEVICE_ID+1], registers[`DEVICE_ID]};
assign USBTYPEC_REV = {registers[`USBTYPEC_REV+1], registers[`USBTYPEC_REV]};
assign USBPD_REV_VER = {registers[`USBPD_REV_VER+1],registers[`USBPD_REV_VER]};
assign PD_INTERFACE_REV = {registers[`PD_INTERFACE_REV+1],registers[`PD_INTERFACE_REV]};
// 0C-0F RESERVED
assign ALERT = {registers[`ALERT+1], registers[`ALERT]};
assign ALERT_MASK = {registers[`ALERT_MASK+1], registers[`ALERT_MASK]};
assign POWER_STATUS_MASK = registers[`POWER_STATUS_MASK];
assign FAULT_STATUS_MASK = registers[`FAULT_STATUS_MASK];
// 16-17 RESERVED
assign CONFIG_STANDARD_OUTPUT = registers[`CONFIG_STANDARD_OUTPUT];
assign TCPC_CONTROL = registers[`TCPC_CONTROL];
assign ROLE_CONTROL = registers[`ROLE_CONTROL];
assign FAULT_CONTROL = registers[`FAULT_CONTROL];
assign POWER_CONTROL = registers[`POWER_CONTROL];
assign CC_STATUS = registers[`CC_STATUS];
assign POWER_STATUS = registers[`POWER_STATUS];
assign FAULT_STATUS = registers[`FAULT_STATUS];
// 20-22 RESERVED
assign COMMAND = registers[`COMMAND];
assign DEVICE_CAPABILITIES_1 = {registers[`DEVICE_CAPABILITIES_1+1], registers[`DEVICE_CAPABILITIES_1]};
assign DEVICE_CAPABILITIES_2 = {registers[`DEVICE_CAPABILITIES_2+1], registers[`DEVICE_CAPABILITIES_2]};
assign STANDARD_INPUT_CAPABILITIES = registers[`STANDARD_INPUT_CAPABILITIES];
assign STANDARD_OUTPUT_CAPABILITIES = registers[`STANDARD_OUTPUT_CAPABILITIES];
assign MESSAGE_HEADER_INFO = registers[`MESSAGE_HEADER_INFO];
assign RECEIVE_DETECT = registers[`RECEIVE_DETECT];

//RECEIVE_BUFFER

assign RECEIVE_BYTE_COUNT = registers[`RECEIVE_BYTE_COUNT];
assign RX_BUF_FRAME_TYPE = registers[`RX_BUF_FRAME_TYPE];
assign RX_BUF_HEADER_BYTE_0 = registers[`RX_BUF_HEADER_BYTE_0];
assign RX_BUF_HEADER_BYTE_1 = registers[`RX_BUF_HEADER_BYTE_1];
assign RX_BUF_OBJ1_BYTE_0 = registers[`RX_BUF_OBJ1_BYTE_0];
assign RX_BUF_OBJ1_BYTE_1 = registers[`RX_BUF_OBJ1_BYTE_1];
assign RX_BUF_OBJ1_BYTE_2 = registers[`RX_BUF_OBJ1_BYTE_2];
assign RX_BUF_OBJ1_BYTE_3 = registers[`RX_BUF_OBJ1_BYTE_3];
assign RX_BUF_OBJ2_BYTE_0 = registers[`RX_BUF_OBJ2_BYTE_0];
assign RX_BUF_OBJ2_BYTE_1 = registers[`RX_BUF_OBJ2_BYTE_1];
assign RX_BUF_OBJ2_BYTE_2 = registers[`RX_BUF_OBJ2_BYTE_2];
assign RX_BUF_OBJ2_BYTE_3 = registers[`RX_BUF_OBJ2_BYTE_3];
assign RX_BUF_OBJ3_BYTE_0 = registers[`RX_BUF_OBJ3_BYTE_0];
assign RX_BUF_OBJ3_BYTE_1 = registers[`RX_BUF_OBJ3_BYTE_1];
assign RX_BUF_OBJ3_BYTE_2 = registers[`RX_BUF_OBJ3_BYTE_2];
assign RX_BUF_OBJ3_BYTE_3 = registers[`RX_BUF_OBJ3_BYTE_3];
assign RX_BUF_OBJ4_BYTE_0 = registers[`RX_BUF_OBJ4_BYTE_0];
assign RX_BUF_OBJ4_BYTE_1 = registers[`RX_BUF_OBJ4_BYTE_1];
assign RX_BUF_OBJ4_BYTE_2 = registers[`RX_BUF_OBJ4_BYTE_2];
assign RX_BUF_OBJ4_BYTE_3 = registers[`RX_BUF_OBJ4_BYTE_3];
assign RX_BUF_OBJ5_BYTE_0 = registers[`RX_BUF_OBJ5_BYTE_0];
assign RX_BUF_OBJ5_BYTE_1 = registers[`RX_BUF_OBJ5_BYTE_1];
assign RX_BUF_OBJ5_BYTE_2 = registers[`RX_BUF_OBJ5_BYTE_2];
assign RX_BUF_OBJ5_BYTE_3 = registers[`RX_BUF_OBJ5_BYTE_3];
assign RX_BUF_OBJ6_BYTE_0 = registers[`RX_BUF_OBJ6_BYTE_0];
assign RX_BUF_OBJ6_BYTE_1 = registers[`RX_BUF_OBJ6_BYTE_1];
assign RX_BUF_OBJ6_BYTE_2 = registers[`RX_BUF_OBJ6_BYTE_2];
assign RX_BUF_OBJ6_BYTE_3 = registers[`RX_BUF_OBJ6_BYTE_3];
assign RX_BUF_OBJ7_BYTE_0 = registers[`RX_BUF_OBJ7_BYTE_0];
assign RX_BUF_OBJ7_BYTE_1 = registers[`RX_BUF_OBJ7_BYTE_1];
assign RX_BUF_OBJ7_BYTE_2 = registers[`RX_BUF_OBJ7_BYTE_2];
assign RX_BUF_OBJ7_BYTE_3 = registers[`RX_BUF_OBJ7_BYTE_3];
assign TRANSMIT = registers[`TRANSMIT];

//TRANSMIT_BUFFER

assign TRANSMIT_BYTE_COUNT = registers[`TRANSMIT_BYTE_COUNT];
assign TX_BUF_HEADER_BYTE_0 = registers[`TX_BUF_HEADER_BYTE_0];
assign TX_BUF_HEADER_BYTE_1 = registers[`TX_BUF_HEADER_BYTE_1];
assign TX_BUF_OBJ1_BYTE_0 = registers[`TX_BUF_OBJ1_BYTE_0];
assign TX_BUF_OBJ1_BYTE_1 = registers[`TX_BUF_OBJ1_BYTE_1];
assign TX_BUF_OBJ1_BYTE_2 = registers[`TX_BUF_OBJ1_BYTE_2];
assign TX_BUF_OBJ1_BYTE_3 = registers[`TX_BUF_OBJ1_BYTE_3];
assign TX_BUF_OBJ2_BYTE_0 = registers[`TX_BUF_OBJ2_BYTE_0];
assign TX_BUF_OBJ2_BYTE_1 = registers[`TX_BUF_OBJ2_BYTE_1];
assign TX_BUF_OBJ2_BYTE_2 = registers[`TX_BUF_OBJ2_BYTE_2];
assign TX_BUF_OBJ2_BYTE_3 = registers[`TX_BUF_OBJ2_BYTE_3];
assign TX_BUF_OBJ3_BYTE_0 = registers[`TX_BUF_OBJ3_BYTE_0];
assign TX_BUF_OBJ3_BYTE_1 = registers[`TX_BUF_OBJ3_BYTE_1];
assign TX_BUF_OBJ3_BYTE_2 = registers[`TX_BUF_OBJ3_BYTE_2];
assign TX_BUF_OBJ3_BYTE_3 = registers[`TX_BUF_OBJ3_BYTE_3];
assign TX_BUF_OBJ4_BYTE_0 = registers[`TX_BUF_OBJ4_BYTE_0];
assign TX_BUF_OBJ4_BYTE_1 = registers[`TX_BUF_OBJ4_BYTE_1];
assign TX_BUF_OBJ4_BYTE_2 = registers[`TX_BUF_OBJ4_BYTE_2];
assign TX_BUF_OBJ4_BYTE_3 = registers[`TX_BUF_OBJ4_BYTE_3];
assign TX_BUF_OBJ5_BYTE_0 = registers[`TX_BUF_OBJ5_BYTE_0];
assign TX_BUF_OBJ5_BYTE_1 = registers[`TX_BUF_OBJ5_BYTE_1];
assign TX_BUF_OBJ5_BYTE_2 = registers[`TX_BUF_OBJ5_BYTE_2];
assign TX_BUF_OBJ5_BYTE_3 = registers[`TX_BUF_OBJ5_BYTE_3];
assign TX_BUF_OBJ6_BYTE_0 = registers[`TX_BUF_OBJ6_BYTE_0];
assign TX_BUF_OBJ6_BYTE_1 = registers[`TX_BUF_OBJ6_BYTE_1];
assign TX_BUF_OBJ6_BYTE_2 = registers[`TX_BUF_OBJ6_BYTE_2];
assign TX_BUF_OBJ6_BYTE_3 = registers[`TX_BUF_OBJ6_BYTE_3];
assign TX_BUF_OBJ7_BYTE_0 = registers[`TX_BUF_OBJ7_BYTE_0];
assign TX_BUF_OBJ7_BYTE_1 = registers[`TX_BUF_OBJ7_BYTE_0];
assign TX_BUF_OBJ7_BYTE_2 = registers[`TX_BUF_OBJ7_BYTE_2];
assign TX_BUF_OBJ7_BYTE_3 = registers[`TX_BUF_OBJ7_BYTE_3];


assign VBUS_VOLTAGE = {registers[`VBUS_VOLTAGE+1], registers[`VBUS_VOLTAGE]};
assign VBUS_SINK_DISCONNECT_THRESHOLD = {registers[`VBUS_SINK_DISCONNECT_THRESHOLD+1], registers[`VBUS_SINK_DISCONNECT_THRESHOLD]};
assign VBUS_STOP_DISCHARGE_THRESHOLD = {registers[`VBUS_STOP_DISCHARGE_THRESHOLD+1], registers[`VBUS_STOP_DISCHARGE_THRESHOLD]};
assign VBUS_VOLTAGE_ALATRAM_HI_CFG = {registers[`VBUS_VOLTAGE_ALATRAM_HI_CFG+1], registers[`VBUS_VOLTAGE_ALATRAM_HI_CFG]};
assign VBUS_VOLTAGE_ALARM_LO_CFG = {registers[`VBUS_VOLTAGE_ALARM_LO_CFG+1], registers [`VBUS_VOLTAGE_ALARM_LO_CFG]};

initial $dumpvars;
endmodule
			
