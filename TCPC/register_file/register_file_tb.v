`include "register_file.v"
`include "../include/registers.vh"
module testbench;
	wire clk,rw;
	wire [7:0] addr;
	wire [`REG_FILE_WIDTH-1:0] w_data;
	wire [`REG_FILE_WIDTH-1:0] r_data;
	
	reg_file_8 ram (.clk(clk), .rw(rw), .addr(addr), .w_data(w_data), .r_data(r_data));
	ram_tester ramIO(.clk(clk), .rw(rw), .addr(addr), .w_data(w_data), .r_data(r_data));
endmodule

module  ram_tester(
	output reg clk,
	output reg rw,
	output reg [7:0] addr,
	output reg [`REG_FILE_WIDTH-1:0] w_data,
	input [`REG_FILE_WIDTH-1:0] r_data );
	
	initial begin
		$dumpfile("register_file.vcd");
		$dumpvars;
		$display ("Populating the RAM");
		$monitor ($time,, "clk = %b rw = %b addr = %h w_data = %h r_data = %h", clk,rw,addr,w_data,r_data);
		
		#200 clk = 0;
		rw=1;
		addr = `ROLE_CONTROL;
		w_data = 255;
		#200 clk =1;
		#200 clk =0;
		repeat(255) begin
			
			addr = addr + 1;
			w_data = w_data -1; 
			#200 clk =~clk;
			#200 clk =~clk;

		end

		$display ("Population completed, Reading the RAM");
		rw = 0;
		addr =0;
		
		#200 clk =1;
		#200 clk =0;

		repeat(255) begin
			addr = addr +1;
			#200 clk =~clk;
			#200 clk =~clk;
		end

		$display("Trial Ended");

	end
endmodule

			
						
			
