//This a completely combinational circuit
module encoder4b5b (
	input [3:0] data_in_4b,
	output reg [4:0] data_out_5b,
);
always@(*) begin
	case(data_in_4b)
	4'b0000: data_out_5b = 5'b11110;
	4'b0001: data_out_5b = 5'b01001;
	4'b0010: data_out_5b = 5'b10100;
	4'b0011: data_out_5b = 5'b10101;
	4'b0100: data_out_5b = 5'b01010;
	4'b0101: data_out_5b = 5'b01011;
	4'b0110: data_out_5b = 5'b01110;
	4'b0111: data_out_5b = 5'b01111;
	4'b1000: data_out_5b = 5'b10010;
	4'b1001: data_out_5b = 5'b10011;
	4'b1010: data_out_5b = 5'b10110;
	4'b1011: data_out_5b = 5'b10111;
	4'b1100: data_out_5b = 5'b11010;
	4'b1101: data_out_5b = 5'b11011;
	4'b1110: data_out_5b = 5'b11100;
	4'b1111: data_out_5b = 5'b11101;
	endcase
end
