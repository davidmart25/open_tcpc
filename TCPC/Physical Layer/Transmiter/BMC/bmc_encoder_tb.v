`include "bmc_encoder_no_sm.v"
module testbench;
reg enable;
reg data_in;
reg bmc_clk;

bmc_encoder test(
	.enable(enable),
	.data_in(data_in),
	.bmc_clk(bmc_clk)
);
reg data_clk;
initial begin
	forever #1 bmc_clk = ~bmc_clk;
end

initial begin
	bmc_clk = 0;
	data_in = 0;
	enable = 0;
	data_clk = 1;
	#10 enable = 1;
end

always@(posedge bmc_clk) begin
	if(enable)
		data_clk <= data_clk +1;
	if(data_clk)
		data_in <= ~data_in;
end
endmodule