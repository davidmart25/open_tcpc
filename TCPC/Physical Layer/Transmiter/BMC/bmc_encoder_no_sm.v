module bmc_encoder(
	input enable,
	input data_in,
	input bmc_clk,
	output reg bmc,
	output reg hiz
);
reg data_clk;
reg data_out;

always @(posedge bmc_clk) begin
	if(~enable) begin
		data_clk <= 1;
		bmc <= 1;
		hiz <= 1;
	end
	else begin
		data_clk <= data_clk + 1;
		hiz <= 0;
		
		if(data_clk) begin
			data_out <= data_in;
			bmc <= ~bmc;
		end
		else begin
			data_out <= data_out;
			if(data_out == 1)
				bmc <= ~bmc;
			else if (data_out == 0)
				bmc <= bmc;
		end
	end
		
end

initial begin
	$dumpfile("encoder.vcd"); //non synth code, simulation only
	$dumpvars;
end
endmodule