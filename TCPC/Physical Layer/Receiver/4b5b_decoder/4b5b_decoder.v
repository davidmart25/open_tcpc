//This is a completely combinational circuit
module decoder4b5b (
	input [4:0] data_in_5b,
	output reg [3:0] data_out_4b,
	output reg error
);
always @(*) begin
	error = 0;
	data_out_4b = 4'bxxxx;
	case(data_in_5b)
	5'b11110: data_out_4b = 4'h0;
	5'b01001: data_out_4b = 4'h1;
	5'b10100: data_out_4b = 4'h2; 
	5'b10101: data_out_4b = 4'h3;
	5'b01010: data_out_4b = 4'h4;
	5'b01011: data_out_4b = 4'h5;
	5'b01110: data_out_4b = 4'h6;
	5'b01111: data_out_4b = 4'h7;
	5'b10010: data_out_4b = 4'h8;
	5'b10011: data_out_4b = 4'h9;
	5'b10110: data_out_4b = 4'hA;
	5'b10111: data_out_4b = 4'hB;
	5'b11010: data_out_4b = 4'hC;
	5'b11011: data_out_4b = 4'hD;
	5'b11100: data_out_4b = 4'hE;
	5'b11101: data_out_4b = 4'hF;
	default: error = 1;
	endcase
end

endmodule
