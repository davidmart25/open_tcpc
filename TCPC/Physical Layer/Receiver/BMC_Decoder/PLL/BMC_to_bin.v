module bmc_to_binary (
	input pll_bmc_clk,
	input enable,
	input reset,
	input bmc,
	output reg binary
);
/*Internal Values */
reg last_bmc_logic_lvl;

/*Next Output Logic */ 
always@(*) begin
	next_last_logic_lvl = current_last_logic_lvl; // check this
	next_binary = 0;
	if(last_logic_lvl == 0)begin
		if(bmc == 0)
			next_binary = 1;
		else if(bmc == 1)
			next_binary = 0;	
	end 
	else if(last_logic_lvl == 1) begin
		if(bmc == 0)
			next_binary = 0;
		else if(bmc == 1)
			next_binary = 1;		
	end
	if(reset)
		last_logic_lvl = 0;
end
/* PLL CLK Logic */
always@(posedge pll_bmc_clk)
	last_logic_lvl <= next_last_logic_lvl;
	last_binary <= next_last_binary;
end
endmodule
