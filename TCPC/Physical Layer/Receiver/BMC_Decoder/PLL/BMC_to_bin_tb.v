`include "BMC_to_bin.v" 
`include "../../../Transmiter/BMC/bmc_encoder.v"
module testbench;
	reg pll_bmc_clk;
	reg reset;
	wire bmc;
	reg enable;
	reg data_in;
	reg bmc_clk;

bmc_to_binary (
	.pll_bmc_clk(pll_bmc_clk),
	.reset(reset),
	.bmc(bmc)
);
bmc_encoder (
	.enable(enable),
	.data_in(data_in),
	.bmc_clk(bmc_clk),
	.bmc(bmc)
);
initial begin
	reset = 0;
	enable = 0;
	#50 reset = 1;
	enable = 1;
end
