`include "recover_clk.v"
module testbench;
reg bmc_clk;
reg clk;
reg bmc;
reg enable;
reg data;
reg rng;
reg rand_bit;
recover_clk test(
	.clk(clk),
	.bmc(bmc),
	.enable(enable),
	.clk_window(0)
);
initial begin
	enable = 0;
	bmc = 0;
	data =0;
	#50 enable = 1;
	#50000 rng = 0;
end

initial begin
clk = 0;
forever #1 clk = ~clk;
end


always@(posedge bmc_clk) begin
	bmc <= ~bmc;
	data <= ~data;
end
always@(negedge bmc_clk) begin
	if(data == 1 )
		bmc <= ~bmc;
end

initial begin
bmc_clk = 0;
rng = 1;
forever begin
	if(rng)
		#500 bmc = rand_bit;
	else
		#500 bmc_clk = ~bmc_clk;
end
end

initial begin
	forever begin
		#500 rand_bit = $urandom%2;
	end
end
endmodule