module bmc_2_sb (
	input clk,
	input enable,
	
	input data_clk_low,
	input data_clk_high,
	input bmc,
	output binary,
	output data_clk,
	output send_next_clk //Next data Out
);
/* State regs */
reg [ ] state;
reg [ ] next_state ;

/* One hot encoding */
parameter IDLE =;
parameter WAIT_DATA_CLK_LOW = ;
parameter WAIT_DATA_CLK_HIGH_OR_DATA = ;
parameter DONE = ;

/*Next values */

/*Current Values */

/*Delayed Values */
reg last_bmc_state;
reg bmc_d;
reg data_clk_low_d;
/*Assigns */
assign posedge_data_clk_low = ~data_clk_low_d & data_clk_low;
assign negedge_data_clk_low = data_clk_low_d & ~data_clk_low;

assign posedge_data_clk_high = ~data_clk_high_d & data_clk_high;
assign negedge_data_clk_high = data_clk_high_d & ~data_clk_high;

assign posedge_bmc = ~bmc_d & bmc;
assign negedge_bmc = bmc_d & bmc;
/*Output Assigns */ 

/*Next State Logic */
always@(*) begin
	next_state = IDLE;
	case(state)
	IDLE: begin
		if(enable)
			next_state = WAIT_DATA_CLK_LOW;
		else
			next_state = IDLE;
	end
	WAIT_DATA_CLK_LOW: begin
		if(posedge_data_clk_low)
			next_state = WAIT_DATA_CLK_HIGH_OR_DATA;
		else
			next_state = WAIT_DATA_CLK_LOW;
	end
	WAIT_DATA_CLK_HIGH_OR_DATA: begin
			if(posedge_bmc || negedge_bmc)
				next_state = WAIT_DATA_CLK_LOW;
			else if (posedge_data_clk_high)
				next_state = ERROR;
			else
				next_state = WAIT_DATA_CLK_HIGH_OR_DATA;
	end
	endcase
end

/*Next Output Logic */
always@(*) begin
	next_outputs = ;
	case(state)
	IDLE: next_last_bmc_state = 1;
	WAIT_DATA_CLK_LOW: begin
		if(posedge_data_clk_low) begin
			if(last_bmc_state ==1) begin
				if(bmc == 1) begin
					next_binary = 1;
					next_last_bmc_state = 1;
				end
				else(bmc == 0) begin
					next_binary = 0;
					next_last_bmc_state =  0;
				end
			end
			else if (last_bmc_state == 0) begin
				if(bmc == 1) begin
					next_binary = 0;
					next_last_bmc_state = 1;
				end
				else if (bmc == 0) begin
					next_binary = 1;
					next_last_bmc_state =0;
				end
			end
		end
	end
	WAIT_DATA_CLK_HIGH_OR_DATA: begin
		if(posedge_bmc || negedge_bmc)
			next_reset_clk = 1;
	end
	
	endcase
end

/* CLK Logic */
always@(posedge clk) begin

end
