`include "i2c_m_trans.v"

module testbench;

	reg enable;
	reg scl;
	reg [7:0] data_in;	
		
	wire sda;
	wire scl_trans_start;
	wire valid_ack_t;
	wire done;

i2c_m_trans master (.enable(enable), .scl(scl), .data_in(data_in), .sda(sda), .scl_trans_start(scl_trans_start), .valid_ack_t(valid_ack_t), .done(done));

initial begin
	enable =0;
	scl =1;
	data_in = 8'b10101010;
	#200 enable = 1;
	#200 scl = 0; 
	data_in = 8'b00000000;
	forever
		#200 scl = ~scl;
end

endmodule



