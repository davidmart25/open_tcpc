module i2c_m_trans (
	input enable,
	input scl,
	input [7:0] data_in,

	output reg sda,
	output scl_trans_start,
	output reg valid_ack_t,
	output done,
	output reg request_data
);

reg [2:0] b_count;
reg [7:0] data;
reg last_data;
reg ack_t;
reg reg_done;

	initial begin
		$dumpfile("i2c_master.vcd"); //non synth code, simulation only
		$dumpvars;	
		//sda = 1;
		//scl_trans_start = 0;
		//request_data = 0;		
	end

assign scl_trans_start = enable;
assign done = reg_done & enable; 

always @(posedge enable) begin

	data <= data_in;
	b_count <= 0;
	//scl_trans_start <= 1;
	sda <= 0;
	reg_done <= 0;
	last_data <= 0;
	ack_t <=0;
	valid_ack_t <= 0;
	request_data <= 0;

end

always @(negedge scl) begin

	if(last_data) begin

		sda <= 1;
		reg_done <= 1;
		ack_t <= 1;
		last_data <= 0;
		request_data <= 0;

		data <= data;
		b_count <= b_count;

	end
	
	//else if(ack_t) //ack not valid for the whole tiume
	//	ack_t <= 0;

	else if(enable) begin
		reg_done <= 0;
		ack_t <= 0;
		sda <= data[7];
		data <= data<<1;
		b_count = b_count -1;
	
		if (b_count == 0) begin
			last_data <= 1;
			request_data <= 1;
		end			
		else begin
			last_data <= 0;
			request_data <= 0;
		end

	end

end

always @(posedge done) begin

	data <= data_in;
	b_count <= 0;
	
end

always @(*) begin

	//if(!enable)
	//	reg_done <= 0;
	//else
	//	reg_done <= reg_done;

	if(ack_t && ~scl) 
		valid_ack_t <= 1;
	else
		valid_ack_t <= 0;

end

endmodule



