`include "i2c_m_trans_2.v"

module testbench;

	reg enable;
	reg scl;
	reg [7:0] data_in;
	reg clk;	
		
	wire sda;
	wire scl_trans_start;
	wire valid_ack_t;
	wire done;
	wire request_data;

i2c_m_trans master (.clk(clk), .enable(enable), .scl(scl), .data_in(data_in), .sda(sda), .scl_trans_start(scl_trans_start), .valid_ack_t(valid_ack_t), .done(done), .request_data(request_data));

initial begin
	scl = 1;
	clk = 1;
	enable =0;
	scl =1;
	data_in = 8'b10101010;
	#10
	enable = 1;
	#100 scl = ~scl;
	#10 data_in = 255;

	forever begin 
		#100 scl = ~scl;
	end
end

initial forever #1 clk = ~clk;



endmodule
