module i2c_m_trans (
	input enable,
	input scl,
	input [7:0] data_in,
	input clk,
	//input ack,

	output reg sda,
	output reg scl_trans_start,
	output reg valid_ack_t,
	output reg done,
	output request_data
);

	reg first_enable;
	reg [7:0] data;
	reg await_ack;
	reg ack_t;
	reg request_data2;
	reg [2:0] b_count;
/*
	initial begin
		$dumpfile("i2c_master_trans.vcd"); //non synth code, simulation only
		$dumpvars;	
	end
*/
	assign request_data = await_ack;

always @(posedge clk) begin //reloj interno

	if(enable && ~first_enable) begin

		first_enable <= 1;
		data <= data_in;
		b_count <= 0;
		sda <= 0;
		done <= 0;
		scl_trans_start <= 1;

	end

	else if( enable && done) begin
		request_data2 <= 1;
		data <= data_in;
		b_count <= 0;
	end

	else if (~enable) begin
		first_enable <= 0;
		sda <= 1;
		done <= 1;
		valid_ack_t <= 0;
		scl_trans_start <=0;
		await_ack <= 0;
	end
	else
		request_data2 <= 0;
	

end

always @(negedge scl) begin //reloj scl

	if (await_ack) begin
		
		sda <= 1;
		await_ack <= 0;
		done <= 1;
		ack_t <=1;
		
	end

	else if(enable) begin
		ack_t <= 0;
		done <= 0;
		sda <= data[7];
		data <= data<<1;
		b_count = b_count -1; //blocking assign
		
		if(b_count == 0) begin
			
			await_ack <= 1;
	
		end

		else begin
	
			await_ack <= 0;

		end

	end


	

end

always @(*) begin

	if( ack_t && ~scl)
		valid_ack_t <= 1;
	else
		valid_ack_t <= 0;

end

endmodule
