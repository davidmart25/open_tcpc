`ifndef _mgropmodes_

`define _mgropmodes_

`define WRITE_SINGLE_BYTE_REG 3'd0
`define READ_SINGLE_BYTE_REG 3'd1
`define WRITE_MULTIPLE_BYTE_REG 3'd2
`define READ_MULTIPLE_BYTE_REG 3'd3
`define WRITE_TRANSMIT_BUFFER 3'd4
`define READ_RECEIVE_BUFFER 3'd5

`endif //_mgropmodes_
