`include "i2c_mgr_states.vh"
module i2c_mgr(
	input clk,
	input enable,
	input sda,
	input scl,
	input [2:0] op_mode,
	input [6:0] slave_address,
	input [7:0] reg_address,
	input [7:0] write_data,
	
	output [7:0] mgr_data_out,
	output done,
	output mgr_error

);
//falta instancear el clock manager
i2c_m_trans trans_machine ( 
	.enable(trans_enable),
	.scl(scl),
	.data_in(trans_data),
	.clk(clk),
	.sda(trans_sda),
	.scl_trans_start(scl_trans_start),
	.valid_ack_t(trans_valid_ack_t), //tbd
	.request_data(trans_request_data) //tbd
);
	
i2c_m_receive receive_machine (
	.enable(rec_enable),
	.clk(clk),
	.scl(scl),
	.sda(rec_sda),
	.check_ack(rec_check_ack),
	.data_out(rec_data_out),
	.send_nack(rec_send_nack),
	.ack(rec_ack)
);

i2c_wsbr_sm wsbr_state_machine(
	.enable(wsbr_enable),
	.clk(clk),
	
	.slave_address_ff(slave_address_ff),
	.reg_address_ff(reg_address_ff),
	.write_data_ff(write_data_ff),
	
	.trans_valid_ack_t(trans_valid_act_t),
	.trans_request_data(trans_request_data),
	
	.trans_data(wsbr_trans_data),
	.trans_enable(wsbr_trans_enable),
	.trans_send_stop(wsbr_send_stop),
	
	.rec_ack(rec_ack),
	
	.rec_enable(wsbr_rec_enable),
	.rec_check_ack(wsbr_rec_check_ack),
	
	.wsrb_error(wsrb_error)
);


reg wsrb_enable;
reg rsbr_enable;
reg wmbr_enable;
reg rmbr_enable;
reg wtb_enable;
reg rrb_enable;


reg [2:0] working_mode;
reg working;
reg seq; //to define byte ammount

reg [6:0] slave_address_ff; //slave_address + R/W info
reg [7:0] reg_address_ff;
reg [7:0] write_data_ff;

always @(posedge clk) begin //FlipFlops de clk.
	if(~enable)
		working <= 0;
		seq <= 0;
		wsrb_enable <= 0;
		rsbr_enable <= 0;
		wmbr_enable <= 0;
		rmbr_enable <= 0;
		wtb_enable <= 0;
		rrb_enable <= 0;
	
	else if(enable) begin
		if(~working) begin
			working <= 1;
			working_mode <= op_mode;
			slave_address_ff <= slave_address;
			reg_address_ff <= reg_address;
			write_data_ff <= write_data;
			
			wsrb_enable <= 0;
			rsbr_enable <= 0;
			wmbr_enable <= 0;
			rmbr_enable <= 0;
			wtb_enable <= 0;
			rrb_enable <= 0;
			
			trans_enable <= 0;
			rec_enable <=0;
			
			
		
		end
		
		else 
			case (working_mode)
		
			`WRITE_SINGLE_BYTE_REG: begin
				wsrb_enable <= 1;
				rsbr_enable <= 0;
				wmbr_enable <= 0;
				rmbr_enable <= 0;
				wtb_enable <= 0;
				rrb_enable <= 0;
				
				trans_data <=  wsrb_trans_data;
				trans_enable <= wsrb_trans_enable;
				trans_send_stop <= wsrb_trans_send_stop;
				
				rec_enable <= wsrb_rec_enable;
				rec_check_ack <= wsrb_rec_check_ack;
				
				error <= wsrb_error;
				
				if(wsbr_trans_send_stop || wsrb_error) begin
					working <= 0;
				end
				
			end
		
			`READ_SINGLE_BYTE_REG: begin
				wsrb_enable <= 0;
				rsbr_enable <= 1;
				wmbr_enable <= 0;
				rmbr_enable <= 0;
				wtb_enable <= 0;
				rrb_enable <= 0;
				
				trans_data <= rsbr_trans_data;
				trans_enable <= rsbr_trans_enable;
				trans_send_stop <= rsbr_trans_send_stop;
				trans_send_nack <= rsbr_trans_send_nack;
				
				rec_enable <= rsrb_rec_enable;
				rec_check_ack <= rsrb_check_ack;
				
				
				mgr_data_out <= rsbr_data_out;
				mgr_data_ready <= rsbr_data_ready;
				mgr_error <= rsbr_error;
				
				if(rsbr_trans_send_stop || rsbr_error) begin
					working <=0;
				end
				
				
				
				
			end
		
			`WRITE_MULTIPLE_BYTE_REG: begin
				wsrb_enable <= 0;
				rsbr_enable <= 0;
				wmbr_enable <= 1;
				rmbr_enable <= 0;
				wtb_enable <= 0;
				rrb_enable <= 0;
			end
		
			`READ_MULTIPLE_BYTE_REG: begin
				wsrb_enable <= 0;
				rsbr_enable <= 0;
				wmbr_enable <= 0;
				rmbr_enable <= 1;
				wtb_enable <= 0;
				rrb_enable <= 0;
			end
		
			`WRITE_TRANSMIT_BUFFER: begin
				wsrb_enable <= 0;
				rsbr_enable <= 0;
				wmbr_enable <= 0;
				rmbr_enable <= 0;
				wtb_enable <= 1;
				rrb_enable <= 0;
			end
		
			`READ_RECEIVE_BUFFER: begin 
				wsrb_enable <= 0;
				rsbr_enable <= 0;
				wmbr_enable <= 0;
				rmbr_enable <= 0;
				wtb_enable <= 0;
				rrb_enable <= 1;
			end
			
			default: begin //invalide operation mode
				wsrb_enable <= 0;
				rsbr_enable <= 0;
				wmbr_enable <= 0;
				rmbr_enable <= 0;
				wtb_enable <= 0;
				rrb_enable <= 0;
			end
			
			endcase
			
			
	end


end

always @(*) begin // logica combinacional


end

endmodule


/*
`define WRITE_SINGLE_BYTE_REG 3'd0
`define READ_SINGLE_BYTE_REG 3'd1
`define WRITE_MULTIPLE_BYTE_REG 3'd2
`define READ_MULTIPLE_BYTE_REG 3'd3
`define WRITE_TRANSMIT_BUFFER 3'd4
`define READ_RECEIVE_BUFFER 3'd5

*/