`include "i2c_send_stop.v"
module testbench;
	reg  clk;
	
	reg  enable;
	wire done;
	
	wire sda;
	reg  scl;

send_stop_sm master (
	.clk(clk),
	
	.enable(enable),
	.done(done),

	.sda(sda),
	.scl(scl)
);

initial begin 
	clk = 0;
	scl = 0;
	enable = 0;
	#20 enable = 1;
end


initial begin
	forever #1 clk = ~clk;
end

initial begin
	forever #200 scl = ~scl;
end
endmodule
