module i2c_rsbr_sm (
	input enable,
	input clk,
	
	input [6:0] slave_address_ff,
	input [7:0] reg_address_ff,
	
	//trans inputs
	input trans_valid_ack_t,
	input trans_request_data,
	input trans_nack_sent,
	
	//trans outputs
	output reg trans_data,
	output reg trans_enable,
	output reg trans_send_stop,
	output reg trans_send_nack,
	
	//Rec inputs
	input rec_ack,
	input rec_data_ready,
	input [7:0] rec_data_out,
	input rec_send_nack,
	
	//Rec outputs
	output reg rec_enable,
	output reg rec_check_ack,
	
	output [7:0] rsbr_data_out,
	output rsbr_data_ready,
	output reg rsbr_error,
);

//One hot encoding

parameter RW_ENABLER = 


reg [4:0] state;
reg [4:0] next_state;

assign rsbr_data_out = rec_data_out;
assign rsbr_data_ready = rec_data_ready;

always @(*) begin //logica del proximo estado
	case (state)
	
	RW_ENABLER: begin
		if(enable)
			next_state <= W_ADDR_RW_ACK_SEND;
		else
			next_state <= RW_ENABLER;
	end
	
	W_ADDR_RW_ACK_SEND: begin
		if(rec_enable && ~trans_valid_ack_t) begin
			if(rec_ack)
				next_state <= ACK_R_REG_ADDR_SEND;
			else
				next_state <= ACK_N_REC;
		end
		else
			next_state <= RW_ADDR_RW_ACK_SEND;
	end
	
	ACK_R_REG_ADDR_SEND: begin
		if(rec_enable && ~trans_valid_ack_t) begin
			if(rec_ack)
				next_state <= WAIT_ADDRESS_SENT;
			else
				next_state <= ACK_N_REC;
		end
		else
			next_state <= ACK_R_REG_ADDR_SEND;
	end
	
	WAIT_ADDRESS_SENT: begin
		if (rec_enable && ~trans_valid_ack_t) begin
			if(rec_ack)
				next_state <= RECEIVE_REG;
			else
				next_state <= ACK_N_REC;
		end
		else
			next_state <=  WAIT_ADDRESS_SENT; //Send nack parece ser parte del proceso.
	end
	
	RECEIVE_REG: begin
		if(rec_send_nac || data_ready)
			next_state <= SEND_NACK; //Talvez otro estado, ya que se debe enviar un nack, no recibio un nack
		else 
			next_state <= RECEIVE_REG;
		
	end
	
	SEND_NACK: begin
		if(trans_nack_sent)
			next_state <= SEND_STOP;
		else
			next_state <= SEND_NACK;
	end
	
	SEND_STOP: begin
		next_state <= SEND_STOP;
	end
	
	endcase
end

always @(*) begin
	case(state)
	
	RW_ENABLER: begin
		if(enable) begin
			trans_data [7:1] <= slave_address_ff;
			trans_data[0] <= 0; //write
			trans_enable <= 1;
		end
		else begin //all enables should be here
			trans_enable <= 0;
			trans_send_stop <=0;
			rec_enable <= 0;
			rec_check_ack  <= 0;
		end
	end
	
	W_ADDR_RW_ACK_SEND: begin
	
		if(trans_reqest_data) begin
			trans_data <= reg_address_ff;
		end
		if(trans_valid_ack_t) begin
			rec_check_ack <= 1;
			rec_enable <= 1;
		end
		if(rec_enable && ~trans_valid_ack_t) begin
			rec_enable <= 0;
		end
	end
	
	ACK_R_REG_ADDR_SEND: begin
		if(trans_request_data) begin
			trans_data[7:1] <= slave_address_ff;
			trans_data[0] <= 1; //READ
		end
		if(trans_valid_ack_t) begin
			rec_check_ack <=1;
			rec_enable <=1;
		end
		if(rec_enable && ~trans_valid_ack_t) begin
			rec_enable <= 0;
		end	
		
	end
	WAIT_ADDRESS_SENT: begin
		if(trans_valid_ack_t) begin
			rec_check_ack <= 1;
			rec_enable <=1;
		end

		if(rec_enable && ~trans_valid_ack_t) begin
			trans_enable <=0;
			rec_check_ack <= 0; //Modo de receptor
		end
		
		if(nack_rec)
			rsrb_error <=1;
	end
	
	RECEIVE_REG: begin
		if(rec_send_nac || data_ready)
			rec_enable <=0;
	end
	
	SEND_NACK begin
		trans_send_nack <=1;
	end
	
	SEND_STOP begin
		trans_send_nack <=0; //Deberia esperar al stop send
		trans_send_stop <=1;
	end
	
	
	endcase
	
	
end