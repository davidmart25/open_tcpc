`include "scl_sm.v"
module testbench;
	reg clk;
	reg enable;
	reg send_start_enable;
	reg send_stop_done;
	reg scl_clk;
	wire scl;
	
scl_sm master (
	.clk(clk),
	.enable(enable),
	.send_start_enable(send_start_enable),
	.send_stop_done(send_stop_done),
	.scl_clk(scl_clk),
	.scl(scl)
);

initial begin
	clk = 0;
	scl_clk = 1;
	enable = 0 ;
	send_start_enable = 0;
	send_stop_done = 0;
	#100 enable = 1;
	#1000 send_start_enable = 1;
	#10000 send_stop_done = 1;
end
initial begin
	forever #100 scl_clk =~scl_clk;
end
initial begin	
	forever #1 clk = ~clk;
end

endmodule