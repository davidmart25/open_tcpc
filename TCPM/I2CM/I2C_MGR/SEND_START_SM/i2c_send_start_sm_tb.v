`include "i2c_send_start_sm.v"
module testbench;
	reg clk;
	
	reg enable;
	//wire error;
	wire done;
	
	wire sda;
	reg scl;

send_start_sm master ( 
	.clk(clk),
	.enable(enable),
	.done(done),
	.sda(sda),
	.scl(scl)
);

initial begin
	scl = 1;
	clk = 0;
	enable = 0;
	#20 enable =1 ;
		
end

initial begin
	forever #1 clk = ~clk;
end

always @(negedge sda) begin
	#20 scl = 0;
end

endmodule


