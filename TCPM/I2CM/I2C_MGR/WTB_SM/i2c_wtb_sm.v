module wtb_sm (
	input clk,
	input enable,
	output error,
	output done,
	
	input [6:0] slave_address,
	
	output [7:0] reg_address,
	output request_new_data,
	input new_data_available,
	input [7:0] transmit_data,
	
	output s8b_enable,	
	output [7:0] s8b_data_in,
	input s8b_done,
	input s8b_error,
	
	output send_start_enable,
	input send_start_done,
	
	output ra_enable,
	input ra_nack_received,
	input ra_ack_received,
	
	output send_stop_enable,
	input send_stop_done
);

/* State REGS */
reg [12:0] state;
reg [12:0] next_state;

/* One Hot Encoding */ 
parameter IDLE =                  13'b0000000000001;
parameter SEND_START =            13'b0000000000010;
parameter SEND_ADDRESS =          13'b0000000000100;
parameter RECEIVE_ACK_1 =         13'b0000000001000;
parameter SEND_REG_ADDRESS =      13'b0000000010000;
parameter RECEIVE_ACK_2 =         13'b0000000100000;
parameter REQUEST_FROM_REG_FILE = 13'b0000001000000;
parameter SEND_BYTE_COUNT =       13'b0000010000000;
parameter RECEIVE_ACK_N =         13'b0000100000000;
parameter TRANSMIT_DATA =         13'b0001000000000;
parameter SEND_STOP =             13'b0010000000000;
parameter TRANSACTION_ERROR =     13'b0100000000000;
parameter STOP =                  13'b1000000000000;

/* Next Values */  
reg next_error;
reg next_done;

reg [6:0] next_slave_address;

reg [7:0] next_reg_address;
reg next_request_new_data;

reg next_s8b_enable;
reg [7:0] next_s8b_data_in;

reg next_send_start_enable;

reg next_ra_enable; 

reg next_send_stop_enable;
reg next_last_trans_value;
reg [7:0] next_M;
reg [7:0] next_byte_count;

/* Current Values */ 
reg current_error;
reg current_done;

reg [6:0] current_slave_address;

reg [7:0] current_reg_address;
reg current_request_new_data;

reg current_s8b_enable;
reg [7:0] current_s8b_data_in;

reg current_send_start_enable;

reg current_ra_enable; 

reg current_send_stop_enable;
reg current_last_trans_value;

reg [7:0] current_M;
reg [7:0] current_byte_count;
/* Assigns */ 

/* Output Assigns */
assign error = current_error;
assign done  = current_done;
assign reg_address = current_reg_address;
assign request_new_data = current_request_new_data;
assign s8b_enable = current_s8b_enable;
assign send_start_enable = current_send_start_enable;
assign ra_enable = current_ra_enable;
assign send_stop_enable = current_send_stop_enable;
assign s8b_data_in = current_s8b_data_in;

/* Next State Logic */
always @(*) begin
	case(state)
	IDLE: begin
		if(enable)
			next_state = SEND_START;
		else
			next_state = IDLE;
	end
	SEND_START: begin
		if(send_start_done)
			next_state = SEND_ADDRESS;
		else
			next_state = SEND_START;
	end
	SEND_ADDRESS: begin
		if(s8b_error)
			next_state = TRANSACTION_ERROR;
		else if (s8b_done)
			next_state = RECEIVE_ACK_1;
		else
			next_state = SEND_ADDRESS;
	end
	RECEIVE_ACK_1: begin
		if(ra_nack_received)
			next_state = TRANSACTION_ERROR;
		else if (ra_ack_received)
			next_state = SEND_REG_ADDRESS;
		else
			next_state = RECEIVE_ACK_1;
	end
	SEND_REG_ADDRESS: begin
		if(s8b_error)
			next_state = TRANSACTION_ERROR;
		else if(s8b_done)
			next_state = RECEIVE_ACK_2;
		else
			next_state = SEND_REG_ADDRESS;
	end
	RECEIVE_ACK_2: begin
		if(ra_nack_received)
			next_state = TRANSACTION_ERROR;
		else if(ra_ack_received)
			next_state = REQUEST_FROM_REG_FILE;
		else
			next_state = RECEIVE_ACK_2;
	end
	REQUEST_FROM_REG_FILE: begin
		if(new_data_available && current_reg_address == 8'h51)
			next_state = SEND_BYTE_COUNT;
		else if (new_data_available)
			next_state = TRANSMIT_DATA;
		else
			next_state = REQUEST_FROM_REG_FILE;
	end
	SEND_BYTE_COUNT: begin
		if (s8b_error)
			next_state = TRANSACTION_ERROR;
		else if (s8b_done)
			next_state = RECEIVE_ACK_N;
		else
			next_state = SEND_BYTE_COUNT;
	end
	RECEIVE_ACK_N: begin
		if(ra_nack_received)
			next_state = TRANSACTION_ERROR;
		else if (ra_ack_received && current_M == current_byte_count)
			next_state =  SEND_STOP;
		else if (ra_ack_received)
			next_state = REQUEST_FROM_REG_FILE;
		else
			next_state = RECEIVE_ACK_N;
	end
	TRANSMIT_DATA: begin
		if(s8b_error)
			next_state = TRANSACTION_ERROR;
		else if( s8b_done)
			next_state = RECEIVE_ACK_N;
		else
			next_state = TRANSMIT_DATA;
	end
	
	SEND_STOP: next_state = STOP;
	
	TRANSACTION_ERROR: next_state = STOP;
	
	STOP: next_state = STOP;
	
	default: next_state = TRANSACTION_ERROR;
	
	endcase
end
	
/* Next Output Logic */ 
always @(*) begin
	next_error = current_error;
	next_done = current_done;
	next_slave_address = current_slave_address;
	next_reg_address = current_reg_address;
	next_request_new_data = current_request_new_data;
	next_s8b_enable = current_s8b_enable;
	next_s8b_data_in = current_s8b_data_in;
	next_send_start_enable = current_send_start_enable;
	next_ra_enable = current_ra_enable; 
	next_send_stop_enable = current_send_stop_enable;
	
	case(state)
	IDLE: begin
			if(enable) begin
				next_slave_address = slave_address;
				next_reg_address = 8'h51;
			end
			else begin
				next_s8b_enable = 0;
				next_send_start_enable = 0;
				next_ra_enable = 0;
				next_send_stop_enable = 0;
				next_request_new_data = 0;
			end
	end
	SEND_START: begin
		next_s8b_enable = 0;
		next_send_start_enable = 1;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_request_new_data = 0;
	end
	SEND_ADDRESS: begin
		next_error = s8b_error;
		next_s8b_data_in [7:1] = current_slave_address;
		next_s8b_data_in [0] = 0; //WRITE		

		next_s8b_enable = 1;
		next_send_start_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_request_new_data = 0;
	end
	RECEIVE_ACK_1: begin
		next_error = ra_nack_received;
		next_s8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 1;
		next_send_stop_enable = 0;
		next_request_new_data = 0;

	end
	SEND_REG_ADDRESS: begin
		next_error = s8b_error;
		next_s8b_data_in = current_reg_address;

		next_s8b_enable = 1;
		next_send_start_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_request_new_data = 0;

	end	
	RECEIVE_ACK_2: begin
		next_error = ra_nack_received;
		next_s8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 1;
		next_send_stop_enable = 0;
		next_request_new_data = 0;

		
	end
	REQUEST_FROM_REG_FILE: begin
		next_s8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_request_new_data = 1;
	end
	SEND_BYTE_COUNT: begin
		next_byte_count = 0;
		next_M = transmit_data;
		next_error = s8b_error;
		next_s8b_data_in = transmit_data;

		next_s8b_enable = 1;
		next_send_start_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_request_new_data = 0;		
	end
	RECEIVE_ACK_N: begin
		next_error = ra_nack_received;
		if(ra_ack_received) begin
			next_byte_count = current_byte_count +1;
			next_reg_address = 8'h52 + current_byte_count; // Chequear esta linea
		end

		if(current_M  == current_byte_count  ) begin
			next_last_trans_value = 1;
			next_done = 1;
		end
		else begin
			next_last_trans_value = 0;
			next_done  = 0 ;
		end
		next_s8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 1;
		next_send_stop_enable = 0;
		next_request_new_data = 0;
		
	end
	TRANSMIT_DATA: begin
		next_error = s8b_error;
		next_s8b_data_in = transmit_data;
		
		next_s8b_enable = 1;
		next_send_start_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_request_new_data = 0;
		
	end
	
	SEND_STOP: begin
		next_s8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 1;
		next_request_new_data = 0;
	end
	
	TRANSACTION_ERROR: begin
		next_error = 1;
	
		next_s8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_request_new_data = 0;
	end
	
	STOP: begin
		next_s8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_request_new_data = 0;
	end
	default: begin
		next_error = 1;
		
		next_s8b_enable = 0;
		next_send_start_enable = 0;
		next_ra_enable = 0;
		next_send_stop_enable = 0;
		next_request_new_data = 0;	
	end
	endcase

end
	
/* Sinco Logic */
always @(posedge clk) begin
	if(~enable)
		state <= IDLE;
	else
		state <= next_state;
	
		current_error <= next_error;
		current_done <= next_done;
		current_slave_address <= next_slave_address;
		current_reg_address <= next_reg_address;
		current_request_new_data <= next_request_new_data;
		current_s8b_enable <= next_s8b_enable;
		current_s8b_data_in <= next_s8b_data_in;
		current_send_start_enable <= next_send_start_enable;
		current_ra_enable <= next_ra_enable;
		current_send_stop_enable <= next_send_stop_enable;
		current_M <= next_M;
		current_byte_count <=next_byte_count;
	
	
end
initial begin
	$dumpfile("wtb_sim.vcd"); //non synth code, simulation only
	$dumpvars;
end
endmodule

