module i2c_scl_mgr (
	input scl_in,
	input scl_trans_start,
	input i2c_m_receive_enabled,

	output reg scl_out

);

reg scl_enable;

always @(negedge scl_in) begin //flipflop de clk scl_in
	if (scl_trans_start || i2c_m_receive_enabled)
		scl_enable <= 1;
	else
		scl_enable <= 0;
end

always @(*) begin //logica combinacional

	if(scl_enable)
		scl_out <= scl_in
	else
		scl_out <= 1;

end
