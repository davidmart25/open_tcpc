module i2c_m(	
	input clk,
	input reset,

	input transmit,
	input [7:0] data_in,	
		
	input sda_in,
	input scl_in,
	
	output reg sda_out,
	output reg scl_out,
	output reg ack_not_r
);

reg [2:0] trans_count;
reg [7:0] trans_data;
reg trans_started;

reg [1:0] clk_2;
reg wait_ack;

	initial begin
		$dumpfile("i2c_master.vcd"); //non synth code, simulation only
		$dumpvars;
	end


always @(negedge clk) begin //clk 2 veces mas rapido que clk_2

	if(reset) begin
		sda_out <= 1'bz;
		scl_out <= 1'bz;
		ack_not_r <= 1'b0;
		trans_started <= 0;
		//clk_2 <= 2'b01;
	end

	//else
	//	clk_2 <= clk_2 +1;
end
//lógica del transmit
always @(posedge transmit) begin
	sda_out <= 0;
	trans_started <= 1;
	trans_data <= data_in;
	trans_count <= 0;		

end

always @(clk) begin //clk_2 2 veces mas lento que clk

	if(trans_started) begin
		if(clk)
			scl_out <= 1'bz;
		else
			scl_out <= 1'b0;
	end

end

always @(negedge scl_in) begin
	
	if(trans_started) begin
		trans_count = trans_count - 1;
		
		if(trans_data[7])
			sda_out <= 1'bz;
		else
			sda_out <= 1'b0;

		trans_data = trans_data<<1;

		if(trans_count ==0) begin // se termino de enviar todos los datos
			wait_ack <= 1;
			trans_started <= 0;
		end

	end

	else if (wait_ack) begin
		sda_out <= 1'bz;
		wait_ack <= 1'b0;
		discard_not_ack <= 1;
		
	end

	else if (discard_not_ack) begin 

	ack_not_r <= 1;

	end 

end

//always @(*) begin //logica combinacional
//end

endmodule



	
