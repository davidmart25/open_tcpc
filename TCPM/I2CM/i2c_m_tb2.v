`include "i2c_m.v"

module testbench;
	wire pull_up_sda;
	wire pull_up_scl;

	pullup(pull1) pull_sda (pull_up_sda);
	pullup(pull1) pull_scl (pull_up_scl);

	reg clk;
	reg reset;

	reg transmit;
	reg [7:0] data_in;	
		
	wire ack_not_r;

i2c_m master (.clk(clk), .reset(reset), .transmit(transmit), .data_in(data_in), .sda_in(pull_up_sda), .scl_in(pull_up_scl), .sda_out(pull_up_sda), .scl_out(pull_up_scl));

initial begin
	transmit =0;
	#200 clk =0;
	reset=1;
	#200 clk = 1;
	#200 clk = 0;
	reset = 0;
	#200 data_in = 8'hAA;
	#200 transmit =1;
	forever begin
	#200 clk = ~clk;
	end
end

endmodule



