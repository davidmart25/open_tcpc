module i2c_m_trans (
	input enable,
	input scl,
	input [7:0] data_in,

	output reg sda,
	output reg scl_trans_start,
	output reg valid_ack_t,
	output reg done
);

reg [2:0] b_count;
reg [7:0] data;
reg last_data;

	initial begin
		$dumpfile("i2c_master.vcd"); //non synth code, simulation only
		$dumpvars;	
		sda = 1;
		scl_trans_start = 0;		
	end

always @(posedge enable) begin

	data <= data_in;
	b_count <= 0;
	scl_trans_start <= 1;
	sda <= 0;
	done <= 0;
	wait_ack <= 0;
	last_data <= 0;

end

always @(negedge scl) begin

	if(last_data) begin

		sda <= 1;
		done <= 1;
		valid_ack_t <= 1;
		last_data <= 0;

	end
	
	else if(valid_ack_t)
		valid_ack_t <= 0;

	else if(enable) begin
		
		sda <= data[7];
		data <= data<<1;
		b_count = b_count -1;
	
		if (b_count == 0)
			last_data <= 1;			
		else
			last_data <= 0;

	end

end

always @(negedge valid_ack_t) begin

	data <= data_in;
	b_count <= 0;
	wait_ack <= 0;
	last_data <=0;
	done <= 0;
	
end

always @(*) begin

	if(!enable) begin

		scl_trans_start <= 0;
		done <= 0;
	
	end

end

endmodule



