`include "i2c_m.v"

module testbench;

//wire pulled_up; //probando pullpus
//pullup(pull1) my_pullup (pulled_up);

	wire clk;
	wire reset;

	wire transmit;
	wire [7:0] data_in;	
		
	wire sda_in;
	wire scl_in;
	
	wire sda_out;
	wire scl_out;
	wire ack_not_r;

i2c_m master (.clk(clk), .reset(reset), .transmit(transmit), .data_in(data_in), .sda_in(sda_in), .scl_in(scl_in), .sda_out(sda_out), .scl_out(scl_out));

i2c_m_test test(.clk(clk), .reset(reset), .transmit(transmit), .data_in(data_in), .sda_in(sda_in), .scl_in(scl_in), .sda_out(sda_out), .scl_out(scl_out)); 

endmodule


module i2c_m_test(
	output reg clk,
	output reg reset,

	output reg transmit,
	output reg [7:0] data_in,	
		
	output reg sda_in,
	output scl_in,
	
	input sda_out,
	input scl_out,
	input ack_not_r
);
assign scl_in = clk;
initial begin
	transmit =0;
	#200 reset =1;
	#200 clk = 1;
	#200 clk = 0;
	#200 reset =0;
	data_in = 8'b10101010;
	transmit = 1;
			
	forever begin
		#200 clk = ~clk;
	end
end

endmodule
