`include "i2c_m_receive.v"

module testbench;
	reg clk;
	reg enable;
	reg scl;
	reg sda;
	reg check_ack;

	wire [7:0] data_out;
	wire data_ready;
	wire send_nack;
	wire ack;

i2c_m_receive master ( 
	.clk(clk),
	.enable(enable),
	.scl(scl),
	.sda(sda),
	.check_ack(check_ack),

	.data_out(data_out),
	.data_ready(data_ready),
	.send_nack(send_nack),
	.ack(ack)
);

initial begin //sda always 1
	check_ack <=0;
	scl <=1;
	clk <= 0;
	enable <= 0;
	sda <=1;

	forever	begin
		#100 scl = ~scl; // en la primera iteración scl pasa a 0
		enable = 1;
	end
end

initial forever #1 clk = ~clk;

endmodule
	

