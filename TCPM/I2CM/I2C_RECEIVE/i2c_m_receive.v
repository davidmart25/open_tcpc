module i2c_m_receive ( //Most be enabled when scl is low
	input clk,
	input enable,
	input scl,
	input sda,
	input check_ack,
	
	output reg [7:0] data_out,
	output reg data_ready,
	output reg send_nack,
	output reg ack
);

parameter ACK_MODE = 1'b1;
parameter REC_MODE =1'b0;

reg [7:0] data;
reg [3:0] b_count;
reg first_scl;
reg trans_error;
reg current_sda;
reg ack_to_send;
reg last_state;
/*
initial begin
	$dumpfile("i2c_master_receive.vcd"); //non synth code, simulation only
	$dumpvars;	
end
*/


always @(posedge clk) begin

	if(~enable) begin //Reset
		
		b_count <= 0;
		data_ready <= 0;
		first_scl <= 1;
		current_sda <= sda;
		trans_error <= 0;
		send_nack <= 0;
		ack <= 0;
		ack <= 0;
		last_state = REC_MODE;
	
		
	
	end
	
	else if (enable) begin 
		if(check_ack) begin
			if(last_state == REC_MODE) begin
				b_count <= 0;
				data_ready <= 0;
				first_scl <= 1;
				current_sda <= sda;
				trans_error <= 0;
				send_nack <= 0;
				ack <= 0;
				last_state = ACK_MODE;	
			end

			else begin

				if(scl) begin
					first_scl <= 0;
					if(ack_to_send && ~sda)
						ack_to_send <= 1;
					else
						ack_to_send <= 0;
				end
 
				else if (~scl) begin

					if(first_scl)
						ack_to_send <= ~sda;

					else
						ack <= ack_to_send;
				end
			end
		end
		else begin
			if(last_state == ACK_MODE) begin
				b_count <= 0;
				data_ready <= 0;
				first_scl <= 1;
				current_sda <= sda;
				trans_error <= 0;
				send_nack <= 0;
				ack <= 0;
				last_state = REC_MODE;		
			end
			else begin
				if(scl) begin
					if( first_scl ) begin
						if (b_count == 8) begin
							if (trans_error)
								send_nack <= 1;
							else begin
								data_out <= data;
								data_ready <= 1;
							end			
						end

						data <= data << 1 ;
						first_scl <= 0;
					end
			
				if(sda != current_sda)
					trans_error <= 1;
		
				end
		
				else if (~scl) begin
					data_ready <= 0;
					if( ~first_scl) begin
						if(b_count == 8) begin
							trans_error <= 0;
							b_count <= 0;
						end

						else
							b_count <= b_count +1;
				
						data [0] <= current_sda;
						first_scl <=1;

					end
			
					current_sda <= sda;

				end

			end
		
		end
	end
end

endmodule
